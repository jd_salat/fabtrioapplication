import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';
import * as moment from 'moment';

@Component({
  selector: 'app-user-managment',
  templateUrl: './user-managment.component.html',
  styleUrls: ['./user-managment.component.scss']
})
export class UserManagmentComponent implements OnInit {

  getAdmin: any;
  message: string;

  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) { }

  ngOnInit() {

    this._adminService.getAdminUser().subscribe(
      data => {
       // console.log(data);
        this.getAdmin = data.data;
        const date = moment(data.data.created_on).format('YYYY-DD-MM');
        console.log(date);
      }

    );
  }

}
