import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';


@Component({
  selector: 'app-registered-user',
  templateUrl: './registered-user.component.html',
  styleUrls: ['./registered-user.component.scss']
})
export class RegisteredUserComponent implements OnInit {

  getUser: any;
  message: string;


  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) { }

  ngOnInit() {

    this._adminService.getRegistredUser().subscribe(
      data => {
        this.getUser = data.data;

        console.log(this.getUser);
      }

    );
  }

}
