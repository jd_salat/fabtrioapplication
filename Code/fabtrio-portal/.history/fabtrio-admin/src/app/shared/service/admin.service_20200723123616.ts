import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  ServerUrl = 'https://devapi.javedsalat.co.in';
  errorData: {};


  constructor(private httpclient: HttpClient) { }


  getRegistredUser(): Observable<any> {
    return this.httpclient.get( this.ServerUrl + '/v1/api/get_registered_user');
   }
   getAdminUser(): Observable<any>{
     return this.httpclient.get(this.ServerUrl + '/v1/api/get_admin')
   }
}
