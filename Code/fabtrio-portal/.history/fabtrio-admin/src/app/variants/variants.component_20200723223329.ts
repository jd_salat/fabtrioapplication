import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';
import * as moment from 'moment';

@Component({
  selector: 'app-variants',
  templateUrl: './variants.component.html',
  styleUrls: ['./variants.component.scss']
})
export class VariantsComponent implements OnInit {
  getVariant: any;
  message: string;
  moment: any = moment;


  // tslint:disable-next-line:variable-name
  constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) { }

  ngOnInit() {

    this._adminService.getVariant().subscribe(
      data => {
        this.getVariant = data.data;

        console.log(this.getVariant);
      }

    );
  }

}
