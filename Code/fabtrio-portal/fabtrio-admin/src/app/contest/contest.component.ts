import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';
import * as moment from 'moment';

@Component({
  selector: 'app-contest',
  templateUrl: './contest.component.html',
  styleUrls: ['./contest.component.scss']
})
export class ContestComponent implements OnInit {
  getContest: any;
  message: string;
  moment: any = moment;

  // tslint:disable-next-line:variable-name
 constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) { }

  ngOnInit() {
    this._adminService.getContest().subscribe(
      data => {
        this.getContest = data.data;

        console.log(this.getContest);
      }

    );
  }

}
