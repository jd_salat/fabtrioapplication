import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GamesCenterComponent } from './games-center.component';

describe('GamesCenterComponent', () => {
  let component: GamesCenterComponent;
  let fixture: ComponentFixture<GamesCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GamesCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
