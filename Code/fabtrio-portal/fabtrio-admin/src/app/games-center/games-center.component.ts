import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';

@Component({
  selector: 'app-games-center',
  templateUrl: './games-center.component.html',
  styleUrls: ['./games-center.component.scss']
})
export class GamesCenterComponent implements OnInit {

  getMatch: any;
  getTeama: any;
  getTeamb: any;
  teamaPlayes: any;
  teambPlayes: any;

 // tslint:disable-next-line:variable-name
  constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) { }

  ngOnInit() {

    this._adminService.getmatchteamPlayers().subscribe(
      data => {
        this.getMatch = data.data.response.get_live_match;
        console.log(this.getMatch);
        this.getTeama = data.data.response.team_a.team_details[0].name;
        this.getTeamb = data.data.response.team_b.team_details[0].name;
        this.teamaPlayes = data.data.response.team_a.get_teama_details;
        this.teambPlayes = data.data.response.team_b.get_teamb_details;

      }

    );

  }
  onClickNavigate(player) {
    const data = {
      playerData: player,
      match_id: this.getMatch[0].match_id
    }
    this.router.navigate(['man-of-the-match'], {state: {data}});
  }

}
