import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminService } from '../shared/service/admin.service';

@Component({
  selector: 'app-man-of-the-match',
  templateUrl: './man-of-the-match.component.html',
  styleUrls: ['./man-of-the-match.component.scss']
})
export class ManOfTheMatchComponent implements OnInit {

  setPlayer: any;
  playersData: any;
  message: string;
  // tslint:disable-next-line:variable-name
  player_id: any;
  // tslint:disable-next-line:variable-name
  match_id: any;
   // tslint:disable-next-line:variable-name
  constructor(private router: Router, private route: ActivatedRoute,  private _adminService: AdminService) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.playersData = this.router.getCurrentNavigation().extras.state.data;
      console.log(this.playersData);
      this.player_id = this.playersData.playerData.player_id;
      this.match_id = this.playersData.match_id;
      console.log(this.playersData);
    }
    if (!this.playersData) {
      this.router.navigate(['games-center']);
    }
   }

  ngOnInit() {
  }

  // tslint:disable-next-line:variable-name
  addMom(player_id, match_id ) {
    console.log(player_id, match_id);
    const data = {
      "match_id": match_id,
      "player_id": player_id
      }
    this._adminService.setMom(data).subscribe(
      res => {
         this.setPlayer = res;
         console.log(this.setPlayer);
      }
    );
  }

}
