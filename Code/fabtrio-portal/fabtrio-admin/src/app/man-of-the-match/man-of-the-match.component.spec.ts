import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManOfTheMatchComponent } from './man-of-the-match.component';

describe('ManOfTheMatchComponent', () => {
  let component: ManOfTheMatchComponent;
  let fixture: ComponentFixture<ManOfTheMatchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManOfTheMatchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManOfTheMatchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
