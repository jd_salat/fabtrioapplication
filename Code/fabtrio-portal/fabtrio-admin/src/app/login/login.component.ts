import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router , ActivatedRoute} from '@angular/router';
import { AuthService } from '../shared/auth/auth.service';
import { AdminService } from '../shared/service/admin.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginDetails: any;
  returnUrl: string;
  loginForm: FormGroup;
  phone_number : any;
  password : any;
  otp : any;
  isOTPGenerated = false;
  otpData = [];
   invalidLogin = false;
   message: string;

  constructor(private router: Router, private _adminService: AdminService, private fb: FormBuilder, private route: ActivatedRoute) {

      // redirect to home if already logged in
  //    if (this._adminService.currentUserValue) {
  //     this.router.navigate(['/']);
  //  }
     }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });

   

  }
  

  // checkLogin() {
  //   if (this.loginservice.authenticate(this.username, this.password)
  //   ) {
  //     this.router.navigate(['dashboard']);
  //     this.invalidLogin = false;
  //   } else {
  //     this.invalidLogin = true;
  //   }
  // }

  adminLogin(){
    console.log(this.loginForm.value.username, this.loginForm.value.password);
    const data = {
      "phone_number": this.loginForm.value.username,
      "password": this.loginForm.value.password,
      }
    this._adminService.generateOtp(data).subscribe(
      res => {
         this.otp= res.data.otp;
         this.isOTPGenerated = true;
         this.otpData = Object.assign([], this.otp);
         console.log(Object.assign([], this.otp));
         console.log(this.loginDetails)
      }
    );
  }

  submitOTP() {
    console.log(this.loginForm.value.username, this.loginForm.value.password);
    const data = {
      "phone_number": this.loginForm.value.username,
      "password": this.loginForm.value.password,
      'otp': this.otp
      }
    this._adminService.verifiyOtp(data).subscribe(
      res => {
         this.isOTPGenerated = false;
         console.log(res)
         this.router.navigate(['/dashboard']);
      }
    );
  }


}
