import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Getadmin } from '../class/getadmin';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<Getadmin>;
  public currentUser: Observable<Getadmin>;

constructor(private http: HttpClient) {

  this.currentUserSubject = new BehaviorSubject<Getadmin>(JSON.parse(localStorage.getItem('currentUser')));
      // tslint:disable-next-line:align
      this.currentUser = this.currentUserSubject.asObservable();
}

public get currentUserValue(): Getadmin {
  return this.currentUserSubject.value;
}

authenticate(username, password) {
  if (username === 'admin' && password === 'admin') {
    sessionStorage.setItem('username', username);
    return true;
  } else {
    return false;
  }
}

isUserLoggedIn() {
  const user = sessionStorage.getItem('username');
  console.log(!(user === null));
  return !(user === null);
}

logOut() {
  localStorage.removeItem('currentUser');
  this.currentUserSubject.next(null);
}
}
