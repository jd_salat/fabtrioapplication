import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
ServerUrl = 'http://localhost:3000';
 // ServerUrl = 'https://devapi.javedsalat.co.in';
 // ServerUrl = 'https://prodapi.javedsalat.co.in';
  errorData: {};


  constructor(private httpclient: HttpClient) { }


  getRegistredUser(): Observable<any> {
    return this.httpclient.get(this.ServerUrl + '/v1/api/get_registered_user');
  }
  getAdminUser(): Observable<any> {
    return this.httpclient.get(this.ServerUrl + '/v1/api/get_admin');
  }
  getContest(): Observable<any> {
    return this.httpclient.get(this.ServerUrl + '/v1/api/get_contest');
  }
  getVariant(): Observable<any> {
    return this.httpclient.get(this.ServerUrl + '/v1/api/get_variant');
  }
  getmatchteamPlayers(): Observable<any> {
    return this.httpclient.get(this.ServerUrl + '/v1/api/get_live_match_team_players');
  }
  // tslint:disable-next-line:variable-name
  setMom( data ): Observable<any> {
    return this.httpclient.put(this.ServerUrl + '/v1/api/upadate_mom', data );
  }
  generateOtp ( data) : Observable <any> {
    return this.httpclient.put(this.ServerUrl + '/v1/api/otp/generate_otp_admin' , data)
  }
  verifiyOtp ( data) : Observable <any> {
    return this.httpclient.put(this.ServerUrl + '/v1/api/otp/verify_otp' , data)
  }
}
