export class Getuser {

    id: number;
    username: number;
    password: string;
    // tslint:disable-next-line:variable-name
    first_name: string;
    // tslint:disable-next-line:variable-name
    last_name: string;
    // tslint:disable-next-line:variable-name
    phone_number: string;
    email: number;
    // tslint:disable-next-line:variable-name
    created_on: string;
    // tslint:disable-next-line:variable-name
    is_kyc_done: boolean;
    // tslint:disable-next-line:variable-name
    cash_balance: number;
    bonus: number;

}

