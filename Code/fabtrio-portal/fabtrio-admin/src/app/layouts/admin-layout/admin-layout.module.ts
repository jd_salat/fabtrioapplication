import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { RouterModule } from '@angular/router';
import { FooterComponent } from 'src/app/components/footer/footer.component';
import { SidebarComponent } from 'src/app/components/sidebar/sidebar.component';
import { NavbarComponent } from 'src/app/components/navbar/navbar.component';
import { ContestComponent } from 'src/app/contest/contest.component';
import { RegisteredUserComponent } from 'src/app/registered-user/registered-user.component';
import { UserDetailsComponent } from 'src/app/user-details/user-details.component';
import { UserManagmentComponent } from 'src/app/user-managment/user-managment.component';
import { VariantsComponent } from 'src/app/variants/variants.component';
import { ManOfTheMatchComponent } from 'src/app/man-of-the-match/man-of-the-match.component';
import { GamesCenterComponent } from 'src/app/games-center/games-center.component';





@NgModule({
  declarations: [
                 DashboardComponent,
                 FooterComponent,
                 SidebarComponent,
                 NavbarComponent,
                 ContestComponent,
                 RegisteredUserComponent,
                 UserDetailsComponent,
                 UserManagmentComponent,
                 VariantsComponent,
                 ManOfTheMatchComponent,
                 GamesCenterComponent,


  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
  ]
})
export class AdminLayoutModule { }
