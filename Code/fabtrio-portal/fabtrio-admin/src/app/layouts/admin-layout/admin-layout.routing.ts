import { Routes } from '@angular/router';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { ContestComponent } from 'src/app/contest/contest.component';
import { RegisteredUserComponent } from 'src/app/registered-user/registered-user.component';
import { UserDetailsComponent } from 'src/app/user-details/user-details.component';
import { UserManagmentComponent } from 'src/app/user-managment/user-managment.component';
import { VariantsComponent } from 'src/app/variants/variants.component';
import { ManOfTheMatchComponent } from 'src/app/man-of-the-match/man-of-the-match.component';
import { GamesCenterComponent } from 'src/app/games-center/games-center.component';


export const AdminLayoutRoutes: Routes = [

    { path: 'dashboard',      component: DashboardComponent },
    { path: 'contest',        component: ContestComponent },
    { path: 'registrered-user',      component: RegisteredUserComponent },
    { path: 'user-details',      component: UserDetailsComponent },
    { path: 'user-managment',      component: UserManagmentComponent },
    { path: 'variants',      component: VariantsComponent },
    { path: 'man-of-the-match' , component: ManOfTheMatchComponent },
    { path: 'games-center' , component: GamesCenterComponent },



];
