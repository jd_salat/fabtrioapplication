const knex = require('./knex');

const player_url = async (match_id , player_id) =>{

 // console.log(match_id)
 // console.log(player_id)

    const get_team_squad_details =  await knex('public.team_squad as ts')
    .leftJoin('public.match as m' , 'm.match_id' ,'ts.match_id')
    .select('ts.team_id','ts.player_id','m.format')
    .where('ts.match_id' , match_id)
    .where('ts.player_id' , player_id)
    .where('m.match_id' , match_id)

    //console.log(get_team_squad_details)

    let default_url = '';
    let thumb_url = '';

   
    for (const url of get_team_squad_details) {

         default_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/players_image/default/${url.team_id}/${url.team_id}_${url.format}.png`
        
         thumb_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/players_image/original/${url.team_id}/${url.player_id}_${url.team_id}_${url.format}.png`

       // console.log('default url',default_url)
        // console.log('thumb_url', thumb_url)
    }

    const player_urls = {
        default_url :default_url ,
        thumb_url:thumb_url

    }

  return player_urls


}



module.exports = player_url