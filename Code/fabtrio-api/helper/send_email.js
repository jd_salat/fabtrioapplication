const express = require('express');
const nodemailer = require("nodemailer");



// async..await is not allowed in global scope, must use a wrapper
async function main(content, userEmail) {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.fabtrio.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'info@fabtrio.com', // generated ethereal user
      pass: 'Arsenal@123', // generated ethereal password
    },
    tls: {
      minVersion: 'TLSv1',
      rejectUnauthorized: false,
      ignoreTLS: false,
      requireTLS: true
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: 'info@fabtrio.com', // sender address
    to: userEmail, // list of receivers
    subject: "Fabtrio ✔", // Subject line
    text: "Fabtrio test mail", // plain text body
    html: content, // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

// main().catch(console.error);

module.exports = main