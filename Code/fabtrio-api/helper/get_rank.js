const knex = require('./knex');
//const { use } = require('../routes/Get Data/get_user_rank');

async function getUserRank(users, username ,total_points,user_team_id){

  // console.log('users' , users)

  // console.log('username' ,username)

    let rank = 0;
    let is_array_contains_same_points = false;
    let first_element_index = -1, second_element_index = -1;
    for(let index =0; index<users.length ; index++){
        for(let inner_index =0; inner_index<users.length; inner_index++){
            if(index !== inner_index){
                if(users[index].total_points === users[inner_index].total_points && users[inner_index].is_checked !== true){
                    is_array_contains_same_points = true;
                    second_element_index = inner_index;
                    first_element_index = index;
                    break;
                }
            }
        }
        if(is_array_contains_same_points === true){
            break;
        }
    }

    if(is_array_contains_same_points === true){

        // array contains same points arrange in correct order and repeat untill all the elements come in correct order.
        // take element from user array position =first_element_index;
        // get user_team modified_on time from db for team_id.
        // take element from user array position =first_element_index;
        // get user_team modified_on time from db for team_id.
        // check which user_team is created first (first come first serve)
        // put is_check =true for both element.
        // based on result swap the elements in users array.
        // call getUserRank function again on users array.
        // repeat untill all the users are sorted in correct order.

        const first_element = users[first_element_index];

      //  console.log(first_element)
        const second_element = users[second_element_index];

     //   console.log(second_element)

        const [first_team_modified_on] = await knex('public.user_team').select("created_on").where('user_team_id', first_element.user_team_id).limit(1);
        const [second_team_modified_on] = await knex('public.user_team').select("created_on").where('user_team_id',second_element.user_team_id).limit(1);
        // compare which one is earlier.

        first_element.is_checked = true;
        second_element.is_checked = true;

       // console.log('first_team_modified_on' ,first_team_modified_on)
        //console.log('second_team_modified_on' ,second_team_modified_on)

      //  console.log(new Date(first_team_modified_on.created_on).valueOf() > new Date(second_team_modified_on.created_on).valueOf())

        if(new Date(first_team_modified_on.created_on).valueOf() > new Date(second_team_modified_on.created_on).valueOf()){
            
            // time comparision 
          //  console.log('in if')
            users.splice(first_element_index, 1, second_element)
            users.splice(second_element_index, 1, first_element)
        }else{
           // console.log('in else')
            users.splice(second_element_index, 1, second_element)
            users.splice(first_element_index, 1, first_element)
        }

       // console.log('***secoundtime fun call***')
       return getUserRank(users, username ,total_points,user_team_id);
    }else {
        // count the rank for username.
        var index = users.findIndex(function(item){ return item.username == username && item.total_points === total_points && item.user_team_id === user_team_id})
        if(index === -1){
            //no user found in array

            return 0;
        }else{
            rank = index + 1

            return rank;
        }
    }
//console.log('rank' , rank)
  
}

module.exports  = getUserRank