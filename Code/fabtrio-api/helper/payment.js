const knex = require('./knex');

const final_amount = async (phone_number_by_token, total_amount , res) => {

     // console.log(phone_number_by_token);



    const get_payment_details = await knex('public.user_wallet')
        .where('username', phone_number_by_token)
        .select('bonus_amount',
            'deposited_amount',
            'winning_amount'
        )

    console.log('get_Payment_detils' , get_payment_details)

    const current_bonus_balance = parseFloat(get_payment_details[0].bonus_amount);

    console.log('current_bonus_balance',current_bonus_balance)


    const current_cash_balance = parseFloat(get_payment_details[0].deposited_amount) 


    console.log('current_cash_balance',current_cash_balance)


    const bonus_use_percentage = await knex('public.m_type_values')
        .where('type_id', 12)
        .where('value_id', 1)
        .select('value_name','description')

    const percentage = bonus_use_percentage[0].value_name

    console.log('percentage' , percentage)

    const count_current_bouns_percentage = ((parseFloat(total_amount) * parseFloat(percentage)) / 100).toFixed(2);

    console.log('count_current_bouns_percentage' , count_current_bouns_percentage)

    const max_eligible_amount = parseFloat(bonus_use_percentage.description)

    let eligible_amount = parseFloat(count_current_bouns_percentage)

    console.log('eligible_amount', eligible_amount)

    if(eligible_amount > max_eligible_amount){

        eligible_amount  = max_eligible_amount

    }
    if(current_bonus_balance < eligible_amount){

        eligible_amount = current_bonus_balance
 
    }

    const  remainning_bonus_amount = (parseFloat(current_bonus_balance) - parseFloat(eligible_amount)).toFixed(2)

    console.log(' remainning_bonus_amount', remainning_bonus_amount)

    const final_amount_to_pay =( parseFloat(current_cash_balance) + parseFloat(count_current_bouns_percentage) - parseFloat(total_amount)).toFixed(2)

    console.log('final_amount' , final_amount_to_pay)
    const response = {

        current_bonus_balance,
        current_cash_balance,
        eligible_amount,
        final_amount_to_pay,
        remainning_bonus_amount

    }

   console.log('response',response)

    return response

}
module.exports = {

    final_amount

}