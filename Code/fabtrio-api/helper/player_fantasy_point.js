
const knex = require('./knex');



const get_fantasy_point = async (match_id, player_id) => {

  // console.log(player_id);
  const get_point = await knex('public.player_stat')
    .select('fantasy_points')
    .where('match_id ', match_id)
    .where('player_id', player_id)



  if (get_point === 0) {

    return
  }

  let fantasy_points = '';

  for (const point of get_point) {

    fantasy_points = point.fantasy_points

  }
  const response = {
    fantasy_points
  }



  return response

}

const get_total_points = async (match_id, user_team_id) => {

  // console.log( 'match_id',match_id)
  // console.log( 'user_team_id',user_team_id)

 // console.log(`user_team_id -> ${user_team_id}`)

  const [get_trump_card_details] = await knex('public.user_trump_card')
    .where('user_team_id', user_team_id)
    .select('trump_card')

 // console.log(get_trump_card_details)
  let trump_card_fantasy_points = 0
  var total_points = 0;


  if (get_trump_card_details.trump_card === 1) {

    const get_bats_man_palyer_id = await knex('public.match_stat')
      .select('bat_om_player_id')
      .where('match_id', match_id)

  //  console.log('get_bats_man_palyer_id', get_bats_man_palyer_id)
    if (get_bats_man_palyer_id.length  && get_bats_man_palyer_id[0].bat_om_player_id !== null) {

      const [get_bats_man_palyer_points] = await knex('public.player_stat')
        .select('fantasy_points')
        .where('match_id', match_id)
        .where('player_id', get_bats_man_palyer_id[0].bat_om_player_id)

     // console.log('get_bats_man_palyer_points', get_bats_man_palyer_points)

      if(get_bats_man_palyer_points.fantasy_points !== null){
        trump_card_fantasy_points = parseFloat(get_bats_man_palyer_points.fantasy_points)

      }

     

    }


  }


  if (get_trump_card_details.trump_card === 2) {

    const get_bowl_man_palyer_id = await knex('public.match_stat')
      .select('bow_om_player_id')
      .where('match_id', match_id)

   // console.log('get_bowl_man_palyer_id', get_bowl_man_palyer_id)
    if (get_bowl_man_palyer_id.length  && get_bowl_man_palyer_id[0].bow_om_player_id !== null ) {

      const [get_bowler_palyer_points] = await knex('public.player_stat')
        .select('fantasy_points')
        .where('match_id', match_id)
        .where('player_id', get_bowl_man_palyer_id[0].bow_om_player_id)

     

      if(get_bowler_palyer_points.fantasy_points !== null) {

   // console.log('get_bowler_palyer_points', get_bowler_palyer_points.fantasy_points)

      trump_card_fantasy_points = parseFloat(get_bowler_palyer_points.fantasy_points)

     // console.log('get_bowler_palyer_points -> fantasy_' ,trump_card_fantasy_points)
      }
    }


  }


  if (get_trump_card_details.trump_card === 3) {

    const get_mom_palyer_id = await knex('public.match_stat')
      .select('mom_player_id')
      .where('match_id', match_id)

   //console.log('get_mom_palyer_id', get_mom_palyer_id)

    if (get_mom_palyer_id.length && get_mom_palyer_id[0].mom_player_id !== null ) {

      const [get_mom_points] = await knex('public.player_stat')
        .select('fantasy_points')
        .where('match_id', match_id)
        .where('player_id', get_mom_palyer_id[0].mom_player_id)

 // console.log('get_mom_points', get_mom_points.fantasy_points)

      if(get_mom_points.fantasy_points !== null){

     //  console.log('get_mom_points', get_mom_points.fantasy_points)

      trump_card_fantasy_points = parseFloat(get_mom_points.fantasy_points)
      }

    }


  }


  const player_details = await knex('public.user_team as ut')
  .leftJoin('public.player_stat as ps', 'ps.player_id', 'ut.player_id')
  .where('ut.user_team_id',user_team_id)
  .where('ps.match_id', match_id)
  .select('ps.fantasy_points', 'ut.is_star_player')

 // console.log('player_details' , player_details)


  
if(player_details.length > 0){

            //  console.log(player_details);

            for (const player of player_details) {

                if (player.is_star_player === 1) {

                    total_points = total_points + (2 * parseFloat(player.fantasy_points));



                }
                else {

                    total_points = total_points + parseFloat(player.fantasy_points);


                }
               // console.log(`user_team_id -> ${element.user_team_id} fantasy_points -> ${player.fantasy_points} player.is_star_player -> ${player.is_star_player} is_star_player_total_points -> ${total_points}`)

            }


          }







  // const players = [];

  // for (const player of player_ids) {
  //   players.push(player.player_id)
  // }

  // const [total_point] = await knex('public.player_stat')
  //   .sum('fantasy_points')
  //   .whereIn('player_id ', players)
  //   .where('match_id', match_id)
  // //console.log(total_point)




  if (total_points === 0) {

    return 0 + (2 * trump_card_fantasy_points)


  }

 // console.log('trump_card_fantasy_points' ,trump_card_fantasy_points)

  const count_final_total_points = parseFloat(total_points) + (2 * trump_card_fantasy_points)

 // console.log('count_final_total_points' , count_final_total_points)

  return parseFloat(count_final_total_points).toFixed(2)




  // const total_point = await knex.raw(`select SUM('fantasy_points') from player_stat where player_id in (select player_id from user_team where user_team_id = ${user_team_id}) and match_id = ${match_id}`).toSQL()
  // console.log(total_point)

}

module.exports = { get_fantasy_point, get_total_points }