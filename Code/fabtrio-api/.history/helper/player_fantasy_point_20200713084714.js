
const knex = require('./knex');



const get_fantasy_point = async (match_id , player_id) =>{

    const get_point =  await knex('public.player_stat')
    .select('fantasy_points')
    .where('match_id ' , match_id)
    .where('player_id' , player_id)

  if(get_point === 0){

    return
  }

    let fantasy_points = '';

    for (const point of get_point) {

        fantasy_points = point.fantasy_points
        
    }
    const response = {
        fantasy_points
    }



    return response

}

const get_total_points = async (match_id,user_team_id) =>{

    // console.log( 'match_id',match_id)
    // console.log( 'user_team_id',user_team_id)


    const player_ids = await knex('public.user_team as ut')
    .select('player_id')
    .where('user_team_id' , user_team_id)

  //  console.log(player_ids)

    const players = [];
    for (const player of player_ids) {
        players.push(player.player_id)       
    }

    const [total_point] =  await knex('public.player_stat')
    .sum('fantasy_points')
    .whereIn('player_id ' , players)
    .where('match_id',match_id)
  //  console.log(total_point)

    if( total_point.sum === null){

        return 0


    }

    return parseFloat(total_point.sum)


    

    // const total_point = await knex.raw(`select SUM('fantasy_points') from player_stat where player_id in (select player_id from user_team where user_team_id = ${user_team_id}) and match_id = ${match_id}`).toSQL()
    // console.log(total_point)

}

module.exports = {get_fantasy_point ,get_total_points}