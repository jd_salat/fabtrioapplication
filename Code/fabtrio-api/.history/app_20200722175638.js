const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const loggers = require('morgan');
const cors = require('cors');
const debug = require('debug')('temp-generator:server');
const http = require('http');
var https = require('https');
const fs = require('fs');
const dotenv = require('dotenv').config();

const app = express();

app.use(loggers('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

const body_parser = require('body-parser');


//middleware
app.use(body_parser.json());

// app.set('views',require('./routes/Post Datas/p'));

app.set('view engine', 'ejs');

var options = {
    cert : fs.readFileSync('./certificate/certificate.crt'),
    key : fs.readFileSync('./certificate/private.key')
};




// routesr
//
//User mangement routes
//
app.use('/v1', require('./routes/user_management/generate_otp'));
app.use('/v1', require('./routes/user_management/registration'));
app.use('/v1', require('./routes/user_management/verify-otp'));
app.use('/v1', require('./routes/user_management/refresh_token'));

//
//Get APIs
//

app.use('/v1', require('./routes/Get Data/get_me'));
app.use('/v1', require('./routes/Get Data/get_tournament'));
app.use('/v1', require('./routes/Get Data/get_state'));
app.use('/v1', require('./routes/Get Data/get_user_team'));
app.use('/v1', require('./routes/Get Data/get_user_contest_details'));
app.use('/v1', require('./routes/Get Data/get_participant_count'));
app.use('/v1', require('./routes/Get Data/get_match'));
app.use('/v1', require('./routes/Get Data/get_teams'));
app.use('/v1', require('./routes/Get Data/get_variants'));
app.use('/v1', require('./routes/Get Data/total_winner_count'));
app.use('/v1', require('./routes/Get Data/get_user_transaction'));
app.use('/v1', require('./routes/Get Data/get_last_five_match_details'));
app.use('/v1', require('./routes/Get Data/get_user_group_deatils'));
app.use('/v1', require('./routes/Get Data/get_user_match'));
app.use('/v1', require('./routes/Get Data/get_trump_card_values'));
app.use('/v1', require('./routes/Get Data/get_user_team_for_team_id'));
app.use('/v1', require('./routes/Get Data/get_match_commentry'));
app.use('/v1', require('./routes/Get Data/get_invite_code'));
app.use('/v1', require('./routes/Get Data/get_profile_avatar'));
app.use('/v1', require('./routes/Get Data/get_user_account_details'));
app.use('/v1', require('./routes/Get Data/application_version'));
app.use('/v1', require('./routes/Get Data/get_user_contest_status_wise'));
app.use('/v1', require('./routes/Get Data/get_user_rank'));
app.use('/v1', require('./routes/Get Data/get_notification'));
app.use('/v1', require('./routes/Get Data/get_weekly_winner'));





// Admin Portal apis
app.use('/v1', require('./routes/Admin API/get_registered_user'))








//
//post APIs
//

app.use('/v1', require('./routes/Post Datas/kyc_done'));
app.use('/v1', require('./routes/Post Datas/user_bank_detils'));
app.use('/v1', require('./routes/Post Datas/save_payment_details'));
app.use('/v1', require('./routes/Post Datas/save_user_team'));
app.use('/v1', require('./routes/Post Datas/save_user_conest'));
// app.use('/v1', require('./routes/Post Datas/save_group'));
app.use('/v1', require('./routes/Post Datas/upload_photo'));
app.use('/v1', require('./routes/Post Datas/submit_withdraw_request'));
app.use('/v1', require('./routes/Post Datas/initial_payment_flow'));
app.use('/v1', require('./routes/Post Datas/payment_getway'));
app.use('/v1', require('./routes/Post Datas/success_payment'));
app.use('/v1', require('./routes/Post Datas/fail_payment'));
app.use('/v1', require('./routes/Post Datas/get_payment_getway_status'));
// app.use('/v1' , require('./routes/Post Datas/send_notification'));




//
//update apis
//
app.use('/v1', require('./routes/Update Data/update_payment'));
app.use('/v1', require('./routes/Update Data/update_user_team'));
app.use('/v1', require('./routes/Update Data/update_user_contest'));
app.use('/v1', require('./routes/Update Data/update_fcm_token'));
app.use('/v1', require('./routes/Update Data/update_user_profile_url'));

//
//cronjob fetchdata
//
// app.use('/v1', require('./routes/Fetch Data/fetch_match_data'));
// app.use('/v1', require('./routes/Fetch Data/fetch_match_squads_data'));
// app.use('/v1', require('./routes/Fetch Data/fetch_get_player_stats_for_current_match'));
// app.use('/v1', require('./routes/Fetch Data/fetch_get_match_info_for_live_match'));
// app.use('/v1', require('./routes/Fetch Data/change_match_status_to_live_scheduler'));





// catch 404 and forward to error
// handler
app.use(function (req, res, next) {
    const err = new Error('Not found');
    err.status = 404;
    next(404);
});

// error handler
app.use(function (err, req, res, next) {

    console.error(err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req
        .app
        .get('env') === 'development'
        ? err
        : {};

    // render the error page
    res
        .json({ message: err.message })
        .status(err.status || 500);

});

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(options,app);


/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    // console.log('server server')
    console.log('Listening on ' + bind);
}