const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');


const { decode_token } = require('../auth/token')
const { log_error } = require('../config/config')



const total_winner_count = async (req, res, next) => {
    res.send('done')

    try {



        const input = {
            
            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)
    
        const check_number_exists_or_not = await knex('public.registration')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {
    
            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {
    
                }
    
            });
    
        }
    
       
    
        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            data: {
    
                total_winner_id_count:total_winner_id_count
    
            }
        })
    
    
    }catch (err) {
        
        console.log(err)

       const log = await log_error(err , '/api/get_weekly_winner')

      

}

}


router.get('/api/get_weekly_winner', get_weekly_winner);

module.exports = router;