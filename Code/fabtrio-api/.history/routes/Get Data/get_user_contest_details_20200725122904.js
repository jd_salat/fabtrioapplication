// 
//  Import Modules 
// 
const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const { decode_token } = require('../auth/token')
const player_url = require('../../helper/get_player_url');
const getUserRank = require('../../helper/get_rank')
const moment = require('moment')

const { get_fantasy_point, get_total_points } = require('../../helper/player_fantasy_point')
// 
//  Main route function
// 

const get_user_contest_details = async (req, res, next) => {
    // 
    //  Main try catch
    //

    try {
        //
        // Get match_id
        //

        const match_id = req.query.match_id;


        const input = {
            token: req.headers.authorization
        }

        // 
        //  Get phone number by token
        //
        const phone_number_by_token = await decode_token(input.token, res);

        // 
        //  Check phone number is exists in db or not
        // 

        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        // 
        //  If phone number is not exists in db then throw error
        // 

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const final_response = [];

        //
        // Get all match
        //

        const all_matches = await knex('public.match as m')
            .leftJoin('public.competition as c', 'c.cid', 'm.competition_id')
            .where('match_id', match_id)
            .select(
                'm.status',
                'c.competition_title',
                'm.timestamp_start',
                'm.team_one_id',
                'm.team_two_id',
            )

        //
        //  Get match details by every match
        //

        for (let index = 0; index < all_matches.length; index++) {
            const match = all_matches[index];

            //
            // prepare match data
            //


            const match_obj = {
                'match_id': match_id,
                'status': match.status,
                'competition_name': match.competition_title,
                'timestamp_start': moment(match.timestamp_start).unix()
            }

            //
            // Get every match teams
            //

            const team_a_details = await get_team_details(match.team_one_id);

            //
            // prepared  teama data
            //

            const team_a_obj = {
                'team_id': match.team_one_id,
                'short_name': team_a_details[0].short_name,
                'thumb_url': team_a_details[0].logo_url,
            }

            const team_b_details = await get_team_details(match.team_two_id);

            //
            // prepared teamb data
            //

            const team_b_obj = {
                'team_id': match.team_two_id,
                'short_name': team_b_details[0].short_name,
                'thumb_url': team_b_details[0].logo_url,
            }

            match_obj.teama = team_a_obj;
            match_obj.teamb = team_b_obj;

            let user_contest = []

            //
            //  Get user contest details by match and username
            //


            const get_user_contest_details = await knex('public.user_contest as uc')
                .leftJoin('public.contest_variance as cv', 'cv.v_id', 'uc.v_id')
                .orderBy('v_id', "asc")
                .leftJoin('public.contest as c', 'c.c_id', 'uc.c_id')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'uc.user_team_id')
                .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                .leftJoin('public.m_type_values as mtv1', 'mtv1.value_id', 'uc.payment_status')
                .where('uc.match_id', match_id)
                .where('uc.username', phone_number_by_token)
                .where('mtv.type_id', 3)
                .where('mtv1.type_id', 6)
                .distinct('uc.c_id', 'c.contest_name', 'c.contest_amount', 'uc.user_team_id', 'mtv.description')
                .orderBy('uc.c_id')
                .select(
                    'cv.v_id',
                    'cv.v_name',
                    'uc.c_id',
                    'c.contest_name',
                    'c.contest_amount',
                    'uc.user_team_id',
                    'mtv.description',
                    'mtv.value_name as trump_card_value',
                    'utc.trump_card',
                    'mtv1.value_name as payment_status',
                    'uc.user_contest_id'
                );

            //
            // every contest details
            //




            for (let index = 0; index < get_user_contest_details.length; index++) {
                const element = get_user_contest_details[index];

                //
                // Select group by detils
                //

                const user_group_id = await knex('public.user_contest_group ')
                    .where('user_team_id', element.user_team_id)
                    .where('match_id', match_id)
                    .where('c_id', element.c_id)
                    .where('v_id', element.v_id)
                    .select('group_id')



                for (const group_id of user_group_id) {

                    //
                    // Get rank
                    //

                    var rank = 0

                    //
                    // Select group participant user
                    //

                    const participating_group_user = await knex('public.user_contest_group ')
                        .where('group_id', group_id.group_id)
                        .select('username',
                            'total_points',
                            'user_team_id')
                        .orderBy('total_points', 'desc')
                    //
                    // Cureent postion user
                    //

                    const index = participating_group_user.findIndex(function(item){ return item.username == phone_number_by_token})
                    rank = await getUserRank(participating_group_user , phone_number_by_token ,participating_group_user[index].total_points)

                }

                //
                // Prepare variant data
                //

                const obj = {
                    "variant_id": element.v_id,
                    "variant_name": element.v_name,
                }


                const participating_contest = []

                //
                // Prepare contest data
                //

                const participating_contest_obj = {
                    "user_contest_id": element.user_contest_id,
                    "contest_id": element.c_id,
                    "contest_name": element.contest_name,
                    "contest_amount": element.contest_amount,
                    "payment_status": element.payment_status,
                    "rank": rank
                }

                //
                // Prepare team data
                //



                const team = {
                    "user_team_id": element.user_team_id,
                    "trump_card": {
                        "trump_card_url": element.description,
                        "trump_card_id": element.trump_card,
                        "trump_card_name": element.trump_card_value
                    }

                    //  "trump_card_url": element.description,
                }

                const players = []

                //
                // Get every team player details
                //

                const get_user_team = await knex('public.user_team as ut')
                    .leftJoin('public.player as p', 'p.player_id', 'ut.player_id')
                    .select('ut.is_star_player',
                        'p.thumb_url',
                        'p.player_id',
                        'p.first_name',
                        'p.last_name',
                        'ut.user_team_id'
                    )
                    .where('ut.user_team_id', element.user_team_id)
                // .where('ut.match_id', match_id)

                // console.log(get_user_team)




                //
                // Every palyers count total poins
                //

                var total_points = 0

                for (const element of get_user_team) {


                    const player_id = element.player_id


                    const point = await get_fantasy_point(match_id, player_id)

                    total_points = await get_total_points(match_id, element.user_team_id)


                  //  console.log('total_points ', total_points)


                    const url = await player_url(match_id, player_id)


                    //console.log(point)

                    if (total_points === 0) {


                        const player_obj = {

                            "player_id": element.player_id,
                            "player_name": `${element.first_name} ${element.last_name}`,
                            "is_start_player": element.is_star_player,
                            "thumb_url": url.thumb_url,
                            "logo_url": url.default_url,
                            "popularity": 0


                        }

                        players.push(player_obj);

                    } else {


                        const player_obj = {


                            "player_id": element.player_id,
                            "player_name": `${element.first_name} ${element.last_name}`,
                            "is_start_player": element.is_star_player,
                            "thumb_url": url.thumb_url,
                            "logo_url": url.default_url,
                            "popularity": Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2) === "NaN" ? 0 : Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2),


                        }
                        players.push(player_obj);

                    }

                }


                team.players = players;



                participating_contest_obj.team = team;


                participating_contest.push(participating_contest_obj)

                obj.participating_contest = participating_contest;


                user_contest.push(obj);


            }

            match_obj.user_contest = user_contest;

            final_response.push(match_obj);


        }

        //
        // Send response
        //



        return res.status(200).json({
            meta: {

                status: "OK",
                message: 'Get User Contest  ✅️'
            },
            data: {

                "response": final_response


            }



        })


    } catch (err) {

        console.log(err)
        //
        // log error
        //

        const log = await log_error(err, `${new Date()} , /api/get_user_contest_details`)

    }


}
//
// Teams details function
//

const get_team_details = async (team_id) => {

    const result = await knex('public.team')
        .where('team_id', team_id)
        .select(
            'short_name',
            'logo_url'
        )


    return result;
}
//
// Get user contest api
//


router.get('/api/get_user_contest_details', get_user_contest_details);

//
// export module
//

module.exports = router;