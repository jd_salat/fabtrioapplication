const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token');
const { log_error } = require('../config/config');



const get_weekly_winner = async (req, res, next) => {
    

    try {

        const input = {
            
            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)
    
        const check_number_exists_or_not = await knex('public.registration')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {
    
            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {
    
                }
    
            });
    
        }

        const limit = req.query.limit
    
       const get_all_details = await knex('public.weekly_winner as w')
                                .leftJoin('public.registration as r' ,'r.phone_number' , 'w.username')
                                .select('r.first_name',
                                'r.last_name',
                                'r.photo_url as ProfilePic',
                                'w.total_points as points',
                                'w.total_contest as contest',
                                'w.prize_money',
                                    )
                                .orderBy('w.prize_money' , decs)
                                .limit(limit)
                                

                                    console.log(get_all_details)

                            
    
        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            
                get_all_details
    
        })
    
    
    }catch (err) {
        
        console.log(err)

       const log = await log_error(err , '/api/get_weekly_winner')

      

}

}


router.get('/api/get_weekly_winner', get_weekly_winner);

module.exports = router;