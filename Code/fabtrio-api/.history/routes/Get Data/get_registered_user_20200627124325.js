const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')

const { decode_token } = require('../auth/token')

const user_details = async (req, res, next) => {

    try {

        const input = {
            token: req.headers.authorization
        }

        const phone_number_by_token = await decode_token(input.token, res)
        // const phone_number = token.encode_details.substring(0, 10);

        
        const check_number_exists_or_not = await knex('public.registration as r')
        .leftJoin('public.kyc as k' , 'k.username','r.phone_number')
        .leftJoin('public.user_bank as ub' , 'ub.username','r.phone_number')
            .where('phone_number', phone_number_by_token)
            .select('r.phone_number',
            'r.email',
            'r.first_name',
            'r.last_name',
            'r.photo_url',
            'r.state_name',
            'r.dob',
            'k.doc_number as kyc_doc_number',
            'ub.ifsc_code',
            'ub.account_number'
            )

            
        if (check_number_exists_or_not.length === 0) {


            return res.status(500).json({
                meta: {
                    status: '0',
                    message: 'Seems like security break.  ✅️'
                },
                data: {
                }
            })
        }

  

   
      const  final_response = check_number_exists_or_not[0]

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get User Profile  ✅️'
            },
             data: final_response

            
        })

    } catch (err) {

        console.log(err)

        const log = await log_error(err, '/api/get_me')

      

}
}


// registration  api
router.get('/api/get_me', user_details);

module.exports = router;