const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')



const get_registered_user = async (req, res, next) => {

    try {

       
        const user_registered = await knex('public.registration')
                                    .select('*')

            
   
      const  final_response = user_registered

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get User Profile  ✅️'
            },
             data: final_response

            
        })

    } catch (err) {

        console.log(err)

        const log = await log_error(err, '/api/get_registered_user')

      

}
}


// registration  api
router.get('/api/get_registered_user', get_registered_user);

module.exports = router;