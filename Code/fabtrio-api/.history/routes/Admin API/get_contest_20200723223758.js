const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const moment = require('moment-timezone')



const get_contest = async (req, res, next) => {

    try {


        const contest_details = await knex('public.contest')
            .select(
                'c_id',
                'contest_name',
                'contest_amount',
                'commission',
                'created_on',
                'modified_on')




        const final_response = contest_details

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get Contest Details  ✅️'
            },
            data: final_response


        })

    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()} , /api/get_contest`)



    }
}


// registration  api
router.get('/api/get_contest', get_contest);

module.exports = router;