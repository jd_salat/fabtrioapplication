const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')

const { decode_token } = require('../auth/token')

const player_url = require('../../helper/get_player_url');

const get_live_match_team_players = async (req, res, next) => {




    let response = {};

    try {
        const get_live_match = await knex('public.match')
            .select('title', 'status_str', 'match_id')
            .where('status', 3)


        for (let index = 0; index < get_live_match.length; index++) {
            const element = get_live_match[index];

            const team_details = await knex('public.match')
                .select('team_one_id', 'team_two_id')
                .where('match_id', element.match_id)


            const get_teama_details = await knex("public.player as p")
                .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
                .select('ts.player_id',
                    'p.playing_role as role',
                    'ts.role_str',
                    'ts.playing11',
                    'p.title',
                    // 'p.thumb_url',
                    // 'p.logo_url'
                )
                .where('ts.match_id', element.match_id)
                .where('ts.team_id', team_details[0].team_one_id);

            console.log('get_live_match_team_players_detais', get_teama_details.length)




            const teama_response = {}

            const teama_details = await get_team_details(team_details[0].team_one_id);
            
            teama_response.team_details = teama_details

            teama_response.get_teama_details = get_teama_details
            


            const get_teamb_details = await knex("public.player as p")
                .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
                .select('ts.player_id',
                    'p.playing_role as role',
                    'ts.role_str',
                    'ts.playing11',
                    'p.title',
                    // 'p.thumb_url',
                    // 'p.logo_url'
                )
                .where('ts.match_id', element.match_id)
                .where('ts.team_id', team_details[0].team_two_id);


            const teamb_response = {}

            const teamb_details = await get_team_details(team_details[0].team_two_id);


            teamb_response.team_details = teamb_details

            response.team_a = teama_response;
            response.team_b = teamb_response;


        }




        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched'
            },
            data: {
                response
            }
        })


    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()} /api/get_live_match_team_players`)



    }

}




const get_team_details = async (team_id) => {

    const result = await knex('public.team')
        .where('team_id', team_id)
        .select(
            'short_name',
            'logo_url',
            'name'
        )


    return result;
}

router.get('/api/get_live_match_team_players', get_live_match_team_players);


module.exports = router;