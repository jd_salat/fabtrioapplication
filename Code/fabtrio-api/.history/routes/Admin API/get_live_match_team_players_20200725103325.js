const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')

const { decode_token } = require('../auth/token')

const player_url = require('../../helper/get_player_url');

const get_live_match_team_players = async (req, res, next) => {
    
  


    let response = {};
    
    try {
        const get_live_match = await knex('public.match')
            .select('title', 'status_str', 'match_id')
            .where('status', 3)


console.log('get_live_matches' , get_live_match)





return







        const team_details = await knex('public.match')
            .select('team_one_id', 'team_two_id')
            .where('match_id', match_id)

        const get_teama_details = await knex("public.player as p")
            .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
            .select('ts.player_id',
                'p.playing_role as role',
                'ts.role_str',
                'ts.playing11',
                'p.title',
                // 'p.thumb_url',
                // 'p.logo_url'
            )
            .where('ts.match_id', match_id)
            .where('ts.team_id', team_details[0].team_one_id);

        console.log('get_live_match_team_players_detais', get_teama_details)


        const teama_response = {}

        const teama_details = await get_team_details(team_details[0].team_one_id);


        teama_response.team_details = teama_details



        const teama_players = [];


        if (get_teama_details.length) {



            for (let index = 0; index < get_teama_details.length; index++) {
                const element = get_teama_details[index];


                const player_id = element.player_id

                const url = await player_url(match_id, player_id)

                get_teama_details[index].thumb_url = url.thumb_url
                get_teama_details[index].logo_url = url.default_url



                delete element.fantasy_points;

                last_five_match_stats = [];

                const match_point = await knex('public.player_stat as ps')
                    .leftJoin('public.match as m', 'm.match_id', 'ps.match_id')
                    .where('ps.player_id', element.player_id)
                    .orderBy('m.date_start', 'asc')
                    .limit(5)
                    .select('ps.fantasy_points')


                for (let index = 0; index < match_point.length; index++) {
                    const element = match_point[index];

                    const obj = {
                        "match_point": element.fantasy_points
                    }

                    last_five_match_stats.push(obj);

                }

                element.last_five_match_stats = last_five_match_stats;
                teama_players.push(element);

            }


            // team_id: get_live_match_team_players_details[0].team_id,
            // squads = get_live_match_team_players_details
            //  console.log(squads)


        } else {

            response = {}
        }

        const get_teamb_details = await knex("public.player as p")
            .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
            .select('ts.player_id',
                'p.playing_role as role',
                'ts.role_str',
                'ts.playing11',
                'p.title',
                // 'p.thumb_url',
                // 'p.logo_url'
            )
            .where('ts.match_id', match_id)
            .where('ts.team_id', team_details[0].team_two_id);


        const teamb_response = {}

        const teamb_details = await get_team_details(team_details[0].team_two_id);


        teamb_response.team_details = teamb_details



        const teamb_players = [];


        if (get_teamb_details.length) {



            for (let index = 0; index < get_teamb_details.length; index++) {
                const element = get_teamb_details[index];


                const player_id = element.player_id

                const url = await player_url(match_id, player_id)

                get_teamb_details[index].thumb_url = url.thumb_url
                get_teamb_details[index].logo_url = url.default_url



                delete element.fantasy_points;

                last_five_match_stats = [];

                const match_point = await knex('public.player_stat as ps')
                    .leftJoin('public.match as m', 'm.match_id', 'ps.match_id')
                    .where('ps.player_id', element.player_id)
                    .orderBy('m.date_start', 'asc')
                    .limit(5)
                    .select('ps.fantasy_points')


                for (let index = 0; index < match_point.length; index++) {
                    const element = match_point[index];

                    const obj = {
                        "match_point": element.fantasy_points
                    }

                    last_five_match_stats.push(obj);

                }

                element.last_five_match_stats = last_five_match_stats;


                teamb_players.push(element);

            }





        } else {
            response = {}
        }

        teama_response.team_players = teama_players;
        teamb_response.team_players = teamb_players;

        response.team_a = teama_response;
        response.team_b = teamb_response;

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched'
            },
            data: {
                response
            }
        })


    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()} /api/get_live_match_team_players`)



    }

}




const get_team_details = async (team_id) => {

    const result = await knex('public.team')
        .where('team_id', team_id)
        .select(
            'short_name',
            'logo_url',
            'name'
        )


    return result;
}

router.get('/api/get_live_match_team_players', get_live_match_team_players);


module.exports = router;