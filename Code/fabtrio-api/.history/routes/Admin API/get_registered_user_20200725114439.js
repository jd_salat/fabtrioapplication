const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const moment = require('moment-timezone');
const { distinct } = require('../../helper/knex');


const get_registered_user = async (req, res, next) => {

    try {


        const user_registered = await knex('public.registration as r')
            .leftJoin('public.user_wallet as uw', 'uw.username', 'r.phone_number')
            .select(
                'r.id',
                'r.first_name',
                'r.last_name',
                'r.email',
                'r.phone_number',
                'r.created_at',
                'r.is_kyc_done',
                'uw.deposited_amount',
                'uw.bonus_amount'
            )
            
    

        const final_response = user_registered

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get User Profile  ✅️'
            },
            data: final_response


        })

    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()} , /api/get_registered_user`)



    }
}


// registration  api
router.get('/api/get_registered_user', get_registered_user);

module.exports = router;