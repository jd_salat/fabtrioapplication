const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const moment = require('moment-timezone')



const get_admin = async (req, res, next) => {

    try {


        const admin_details = await knex('public.admin')
            .select('*')

        
         const  final_response = admin_details



console.log(final_response)
       

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get Admin Profile  ✅️'
            },
            data: final_response


        })

    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()} , /api/get_admin`)



    }
}


// registration  api
router.get('/api/get_admin', get_admin);

module.exports = router;