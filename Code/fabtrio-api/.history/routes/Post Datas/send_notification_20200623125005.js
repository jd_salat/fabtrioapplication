const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const { admin } = require('../config/firebase_config');


const send_notification_with_notification = async (phone , notification) => {

    try {

        const payload = {
            notification: {
                title: "Account Deposit",
                body: "A deposit to your savings account has just cleared."
            },

        };

        const options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24, // 1 day
        };

        [{ fcm_token }] = await knex('public.registration')
            .select('fcm_token')
            .where('phone_number', phone)

        console.log(fcm_token)

        var registrationToken = `${fcm_token}`



        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(response => {

                console.log('Notification sent successfully')



                const [{ description }] = await knex('pubic.m_type_values')
                    .select(description)
                    .where('type_id', 13)
                    .where('value_id', 1)

                console.log(description)

                const prepared_data = {
                    notification_message: payload.notification.body,
                    notification_type: 1,
                    notification_image_url: description

                }

                const save_notification_in_db = await knex('public.notification')
                    .insert(prepared_data)

            })
            .catch(error => {
                console.log(error);


                const [{ description }] = await knex('pubic.m_type_values')
                .select(description)
                .where('type_id', 13)
                .where('value_id', 3)
    
            console.log(description)
    
            const prepared_data = {
                notification_message: payload.notification.body,
                notification_type: 3,
                notification_image_url: description
    
            }
    
            const save_notification_in_db = await knex('public.notification')
                .insert(prepared_data)


            });

       




    } catch (err) {

        console.log(err)

        const log = await log_error(err, 'send_notification_with_notification')



    }
}

const send_notification_with_data = async (phone , data) => {

    try {

        const payload = {
            data: {
                account: "Savings",
                balance: "$3020.25"
            }

        };

        const options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24, // 1 day
        };

        [{ fcm_token }] = await knex('public.registration')
            .select('fcm_token')
            .where('phone_number', phone)

        console.log(fcm_token)

        var registrationToken = `${fcm_token}`



        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(response => {

                console.log('Notification sent successfully')

                const [{ description }] = await knex('pubic.m_type_values')
                .select(description)
                .where('type_id', 13)
                .where('value_id', 2)
    
            console.log(description)
    
            const prepared_data = {
                notification_message: payload.notification.body,
                notification_type: 2,
                notification_image_url: description
    
            }
    
            const save_notification_in_db = await knex('public.notification')
                .insert(prepared_data)
    
    

            })
            .catch(error => {
                console.log(error);

                const [{ description }] = await knex('pubic.m_type_values')
                .select(description)
                .where('type_id', 13)
                .where('value_id', 3)
    
            console.log(description)
    
            const prepared_data = {
                notification_message: payload.notification.body,
                notification_type: 3,
                notification_image_url: description
    
            }
    
            const save_notification_in_db = await knex('public.notification')
                .insert(prepared_data)
    
    
            });




    } catch (err) {

        console.log(err)

        const log = await log_error(err, 'send_notification_with_data')



    }
}

const send_notification_with_topic = async (phone) => {

    try {

        var topic = "finance";

        [{ fcm_token }] = await knex('public.registration')
            .select('fcm_token')
            .where('phone_number', phone)

        console.log(fcm_token)

        var registrationToken = `${fcm_token}`



        admin.messaging().sendToDevice(registrationToken, topic)
            .then(response => {

                console.log('Notification sent successfully')

            })
            .catch(error => {
                console.log(error);
            });




    } catch (err) {

        console.log(err)

        const log = await log_error(err, 'send_notification_with_topic')



    }
}



module.exports = {

    send_notification_with_notification,
    send_notification_with_data,
    send_notification_with_topic

}