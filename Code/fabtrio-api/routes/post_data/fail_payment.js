

const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const salt = process.env.SALT;
const jsSHA = require("jssha");

const payment_failure = async (req, res, next) => {

    try {

        const data = req.body

        let generate_reverse_hash


        if (data.additional_charges != null) {

            const reverse_hash_sequence = `${data.additional_charges}|${salt}|${data.status}||||||${data.udf5}|${data.udf4}|${data.udf3}|${data.udf2}|${data.udf1}|${data.email}|${data.firstname}|${data.productinfo}|${data.amount}|${data.txnid}|${data.key}`

            const sha = new jsSHA('SHA-512', "TEXT");

            sha.update(reverse_hash_sequence);

            generate_reverse_hash = sha.getHash("HEX");

        } else {

            const reverse_hash_sequence = `${salt}|${data.status}||||||${data.udf5}|${data.udf4}|${data.udf3}|${data.udf2}|${data.udf1}|${data.email}|${data.firstname}|${data.productinfo}|${data.amount}|${data.txnid}|${data.key}`

            const sha = new jsSHA('SHA-512', "TEXT");

            sha.update(reverse_hash_sequence);

            generate_reverse_hash = sha.getHash("HEX");

        }


        const hash_from_response = data.hash


        if (generate_reverse_hash === hash_from_response) {

            const update_payment_status = await knex('public.transaction')
                .update('transaction_status', 3)
                .where('username', data.phone)
                .where('transaction_id', data.txnid)
        }



        res.render('failure', { data: data })

    } catch (err) {

        console.log(`${new Date()} , /failure -> ${err}`)

    }

}
//success
router.post("/failure", payment_failure)






module.exports = router;