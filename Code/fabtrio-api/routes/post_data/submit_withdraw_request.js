const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token');


const submit_withdrawal_request = async (req, res, next) => {


    const input = {
        amount: req.body.amount,
        token: req.headers.authorization
    }


    try {

        const phone_number_by_token = await decode_token(input.token, res)
       


        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const get_user_withdrawal_details_by_status = await knex('public.withdrawal_request')
            .where('username', phone_number_by_token)
            .where('status', 1)
            .select('*');

        if (get_user_withdrawal_details_by_status
            && get_user_withdrawal_details_by_status.length) {

            return res.status(409).json({
                meta: {
                    status: '0',
                    message: `⚠️ Already have one pending payment request`
                },
                data: {

                }

            })

        }



        delete input.token;

        input.username = phone_number_by_token

        const save_details_in_db = await knex('public.withdrawal_request')
            .insert(input)



        return res.status(200).json({
            meta: {
                status: 'true',

            },
            data: {

            }
        })


    }

    catch (err) {

        console.log(`${new Date() } , /api/submit_withdrawal_request -> ${err}`)

    }

}



// photo upload
router.post('/api/submit_withdrawal_request', submit_withdrawal_request);

module.exports = router;
