const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const multer = require('multer');
const storage = multer.memoryStorage();
const upload = multer({ storage });
const S3FS = require('s3fs');


const { decode_token } = require('../auth/token')

const user_profile_picture = new S3FS('fabtrio/user_profile_picture', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const bank_details_doc = new S3FS('fabtrio/bank_document', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const kyc_document = new S3FS('fabtrio/kyc_document', {

    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const team = new S3FS('fabtrio/team', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const trump_card_url = new S3FS('fabtrio/trump_card_url', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const avatar = new S3FS('fabtrio/avatar', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})
const notification_image_url = new S3FS('fabtrio/notification_image_url', {
    secretAccessKey: process.env.SECRETACCESSKEY,
    accessKeyId: process.env.ACCESSKEYID,
    region: process.env.REGION,
    ACL: 'public-read',
    ContentType: 'image/png',
})

const upload_photo = async (req, res, next) => {

    try {
        const input =
        {

            files: req.files,
            doc_type: req.body.doc_type,
            token: req.headers.authorization

        }

        const data = req.body;

        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                return res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }
            const phone_number_by_token = await decode_token(input.token, res)

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')

            if (check_number_exists_or_not.length === 0) {

                return res.status(424).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }



            const files = input.files;

            const url = [];

            for (let index = 0; index < files.length; index++) {
                const element = files[index];

                const buffer = element.buffer

                const file_name = `${phone_number_by_token +'_'+ input.doc_type}.png`;

                if (input.doc_type === 'PROFILE_PIC') {


                    s3ImageObj = await user_profile_picture.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;



                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/user_profile_picture/${file_name}`;
                    obj.url = s3_url;
                    // const add_urls_in_db = await knex('public.registration')
                    //     .where('phone_number', input.username)
                    //     .update('photo_url', s3_url)

                }

                if (input.doc_type === 'KYC_DOC') {


                    s3ImageObj = await kyc_document.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/kyc_document/${file_name}`;

                    obj.url = s3_url;

                    // console.log(obj)

                    // console.log(s3_url)



                    // const add_urls_in_db = await knex('public.kyc')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)
                }


                if (input.doc_type === 'BANK_DETAILS') {


                    s3ImageObj = await bank_details_doc.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/bank_document/${file_name}`;
                    obj.url = s3_url;
                    // console.log(s3_url)

                    // const add_urls_in_db = await knex('public.user_bank')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)

                }

                if (input.doc_type === 'TEAM') {


                    s3ImageObj = await team.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/team/${file_name}`;



                    obj.url = s3_url;

                    // const add_urls_in_db = await knex('public.user_bank')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)

                }

                if (input.doc_type === 'TRUMP_CARD_URL') {


                    s3ImageObj = await trump_card_url.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/trump_card_url/${file_name}`;



                    obj.url = s3_url;

                    // const add_urls_in_db = await knex('public.user_bank')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)

                }

                if (input.doc_type === 'AVATAR') {


                    s3ImageObj = await avatar.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/avatar/${file_name}`;



                    obj.url = s3_url;

                    // const add_urls_in_db = await knex('public.user_bank')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)

                }

                if (input.doc_type === 'NOTIFICATION_IMAGE_URL') {


                    s3ImageObj = await notification_image_url.writeFile(file_name, buffer);

                    const obj = new Object();

                    url[index] = obj;

                    const s3_url = `https://fabtrio.s3.ap-south-1.amazonaws.com/notification_image_url/${file_name}`;



                    obj.url = s3_url;

                    // const add_urls_in_db = await knex('public.user_bank')
                    //     .where('username', input.username)
                    //     .update('doc_link', s3_url)

                }




                return res.status(200).json({
                    meta: {
                        status: 'OK',
                        message: 'User successfully added photo ✅️'
                    },

                    data: {
                        'photo_url': url[0].url
                    }
                })

            }

        })

    }

    catch (err) {

        console.log(`${new Date() } , /api/upload_photo -> ${err}`)

    }

}

const schema = Joi.object({
    // username: Joi.number().allow(''),
    doc_type: Joi.string().valid('PROFILE_PIC', 'KYC_DOC', 'BANK_DETAILS', 'TEAM', 'TRUMP_CARD_URL', 'AVATAR' ,'NOTIFICATION_IMAGE_URL')

})

// photo upload
router.post('/api/upload_photo', upload.array('files', 1), upload_photo);

module.exports = router;
