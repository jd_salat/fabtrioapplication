const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi')

const { decode_token } = require('../auth/token')



const test_txn = async (req, res, next) => {
    try {


        const input = {
            match_id: req.body.match_id,
            token: req.headers.authorization,

        }





        const data = req.body



        Joi.validate(data, schema, async (err, value) => {



            if (err) {

                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }
            const phone_number_by_token = await decode_token(input.token, res)



            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }



            const transaction_id = new Date().valueOf();

            input.transaction_id = `FAB${transaction_id}`;

          //  console.log(transaction_id)


            // delete input.token;

            // console.log(input)

            const username = phone_number_by_token

            input.username = username;

            const transaction_array = [
                {

                    transaction_id: '1596523386289',
                },{
                    transaction_id: '1596523544653',
                },{
                    transaction_id: '1596523572773',

                }

            ]

            const data_to_insert = []
         for (let index = 0; index < transaction_array.length; index++) {
             const element = transaction_array[index];
             
              const   prepared_data = {
                    username:phone_number_by_token,
                    match_id:input.match_id,
                    txn_id:element.transaction_id
                }

                data_to_insert.push(prepared_data)
               // const save_details = await knex('public.txn_test')
                
                //.insert(prepared_data)

               const insert = await upsert(prepared_data.username,prepared_data.txn_id,prepared_data.match_id)

              
                
            }


            const insert_result = await knex.transaction(async function(txn){

               // const inserts = await txn('public.txn_test').insert(data_to_insert)
                
            })
           

                
        
                return res.status(200).json({
                    meta: {
                        status: '0',
                        message: `Success`
                    },
                    data: {

                    }

                });

        })
    } catch (err) {

        console.log(`${new Date()} , /api/test_txn -> ${err}`)

    }
}
async function upsert(username ,txn_id,match_id) {
    const result = await knex.raw(
       `insert into public.txn_test (username,txn_id ,match_id)  values (:username,:txn_id,:match_id) on conflict (txn_id) 
       do update set username = :username ,txn_id =:txn_id, match_id = :match_id returning *`,{username ,txn_id,match_id}
      );

    //  console.log(result)

    }

const schema = Joi.object({

    match_id: Joi.number(),

})

router.post('/api/test_txn', test_txn);

module.exports = router;