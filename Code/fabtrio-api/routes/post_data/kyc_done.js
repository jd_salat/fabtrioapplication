const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');

const { decode_token } = require('../auth/token')


const is_kyc_done = async (req, res, next) => {

    try {
        const input =
        {
           
            doc_type: req.body.doc_type,
            doc_link: req.body.doc_link,
           // is_verified: req.body.is_verified,
            //approved_by: req.body.approved_by,
            name_on_doc: req.body.name_on_doc,
            doc_number: req.body.doc_number,
            created_on: moment().format(),
            modified_on: moment().format(),
            token: req.headers.authorization

        }

        const data = req.body;





        Joi.validate(data, schema, async (err, value) => {

            if (err) {
                // send a 422 error response if validation fails
                return res.status(409).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
            // const phone_number = token.encode_details.substring(0, 10);


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(424).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

          

            const kyc_details = await knex('public.kyc')
                .where('username', phone_number_by_token)
                .select('*');

            if (kyc_details.length != 0) {

                res.status(400).json({
                    meta: {
                        status: '1',
                        message: `Kyc details for username is already present`
                    },
                    data: {

                    }

                });

                return;
            }

            const doc_type_mtype = await knex('public.m_type_values')
                .where('value_name', input.doc_type)
                .where('type_id', 7)
                .select('*')

            input.doc_type = doc_type_mtype[0].value_id;

            // const is_verified_mtype = await knex('public.m_type_values')
            //     .where('value_name', input.is_verified)
            //     .where('type_id', 4)
            //     .select('*')

            // input.is_verified = is_verified_mtype[0].value_id;


            delete input.token

            input.username = phone_number_by_token

            await save_details_in_db(input);

            return res.status(200).json({
                meta: {
                    status: '3',
                    message: 'Kyc successfully done ✅️'
                },
                data: {

                }
            })

        })
    }

    catch (err) {
        
        console.log(`${new Date() } , /api/is_kyc_done -> ${err}`)
}

}
const save_details_in_db = async (data) => {

    // console.log(data)  delete data.files;
    delete data.files;

    const result = await knex('public.kyc')
        .insert(data);

    return result;

}
const schema = Joi.object({
    // registration_id: Joi.number().required(),
    // username: Joi.string().required(),
    doc_type: Joi.string().valid('PAN Card').required(),
    doc_link:Joi.string(),
   // is_verified: Joi.string().valid('Yes', 'No').required(),
   // approved_by: Joi.string(),
    name_on_doc: Joi.string().required(),
    doc_number: Joi.string(),

})

// kyc done
router.post('/api/is_kyc_done', is_kyc_done);

module.exports = router;
