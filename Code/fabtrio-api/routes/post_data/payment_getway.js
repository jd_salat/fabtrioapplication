const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
const moment = require('moment-timezone');
const jsSHA = require("jssha");
const key = process.env.KEY;
const salt = process.env.SALT;
const productinfo = process.env.PRODUCTINFO;
const email = process.env.EMAIL;
const request = require('request');

const { final_amount } = require('../../helper/payment')

const payment_getway = async (req, res, next) => {

    try {


        const input = {
            match_id: req.body.match_id,
            token: req.headers.authorization
        }

        const data = req.body

        Joi.validate(data, schema, async (err, value) => {

            if (err) {

                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)

            // console.log(phone_number_by_token)

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')




            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

            const [{ first_name }] = await knex('public.registration')
                .where('phone_number', phone_number_by_token)
                .select('first_name')

             
    
            const time_stamp = new Date().valueOf();
            const txnid = `FABPG${phone_number_by_token}${time_stamp}`;
            const amount = req.body.amount

           // console.log(txnid)

            const transaction_data = {
                transaction_id: txnid,
                transaction_type: 1,
                transaction_amount: amount,
                transaction_date: moment().format('YYYY-MM-DD'),
                username: phone_number_by_token,
                match_id: input.match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                transaction_message: 'Wallet Recharge'
            }

            const save_transaction_details_in_db = await knex('public.transaction')
            .insert(transaction_data)
            .returning('*')

         // console.log(save_transaction_details_in_db)

         const phone_number = phone_number_by_token

            const hashSequence = `${key}|${txnid}|${amount}|${productinfo}|${first_name}|${email}|||||||||||${salt}`;
            const sha = new jsSHA('SHA-512', "TEXT");
             sha.update(hashSequence);

          const hash = sha.getHash("HEX");

                  

            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Payment Successfully ✅️'
                },
                data: {
                    txnid,hash,phone_number,first_name


                }
            })

            

        })

    } catch (err) {

        console.log(`${new Date() } , /api/payment_getway -> ${err}`)

    }
}




const schema = Joi.object({

    match_id: Joi.number(),
    amount: Joi.number(),

})

router.post('/api/payment_getway', payment_getway);



module.exports = router;