const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token } = require('../auth/token')

const { final_amount } = require('../../helper/payment')

const get_payment_getway_status = async (req, res, next) => {


    try {

        const input = {
            token: req.headers.authorization,
            match_id:req.body.match_id,
           
        }

        const phone_number_by_token = await decode_token(input.token, res)
        


        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {


            return res.status(500).json({
                meta: {
                    status: '0',
                    message: 'Seems like security break.  ✅️'
                },
                data: {
                }
            })
        }

       
      const  total_amount = req.body.total_amount;
      const  transaction_id = req.body.transaction_id;
      let final_balance;

      const [get_transaction_status] =  await knex('public.transaction')
                                        .select('transaction_status')
                                        .where('transaction_id' , transaction_id)
                                        .where('match_id' , input.match_id)

                                      
                                        if(get_transaction_status.transaction_status  === 1){

                                            final_balance = await final_amount(phone_number_by_token, total_amount)

                                            
                                        } else {
                                            return res.status(404).json({
                                                meta: {
                                                    status: '0',
                                                    message: 'Transaction status not success'
                                                },
                                                data: {
                                                }
                                            })


                                        }

                                        const test_amount = {
                                            total_amount,
                                            current_bonus_balance: final_balance.current_bonus_balance,
                                            current_cash_balance: final_balance.current_cash_balance,
                                            final_amount_to_pay: final_balance.final_amount_to_pay,
                                            eligible_amount: final_balance.eligible_amount,
                                             
                            
                                        }
                                               

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            data: {
                total_amount,
                current_bonus_balance: final_balance.current_bonus_balance,
                current_cash_balance: final_balance.current_cash_balance,
                final_amount_to_pay: final_balance.final_amount_to_pay,
                eligible_amount: final_balance.eligible_amount,
                 

            }
        })

    } catch (err) {

        console.log(`${new Date() } , /api/get_payment_getway_status -> ${err}`)

    }
}


// registration  api
router.post('/api/get_payment_getway_status', get_payment_getway_status);

module.exports = router;