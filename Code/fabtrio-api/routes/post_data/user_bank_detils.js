const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')


const user_bank_details = async (req, res, next) => {

    try {
        const input =
        {
            
          
            bank_name: req.body.bank_name,
            account_number: req.body.account_number,
            ifsc_code: req.body.ifsc_code,
            branch_name: req.body.branch_name,
            doc_link:req.body.doc_link,
           // is_verified: req.body.is_verified,
           // verified_by: req.body.verified_by,
            token: req.headers.authorization,

        }
       
        const data = req.body
        

        Joi.validate(data, schema, async (err, value) => {

            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });

            }
            const phone_number_by_token = await decode_token(input.token , res)
            
        
        
            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')
        

            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

         

            const bank_details = await knex('public.user_bank')
                .where('username', phone_number_by_token)
                .select('*');


            if (bank_details.length != 0) {

                delete input.token;
                const update_bank_details = await knex('public.user_bank')
                .where('username' , phone_number_by_token)
                .update(input)
                .update('is_verified', 2)
                .update('modified_on', moment())
                .returning('*')

                console.log(update_bank_details)

                return res.status(201).json({
                    meta: {
                        status: '1',
                        message: `Successfully Update`
                    },
                    data: {

                    }

                });


            }

            

        //     const is_verified_mtype = await knex('public.m_type_values')
        //     .where('value_name', input.is_verified)
        //     .where('type_id', 4)
        //     .select('*')

        //    input.is_verified = is_verified_mtype[0].value_id;

            delete input.token;
            input.username = phone_number_by_token;

            await save_details_in_db(input);
            //  console.log(url);



            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Bank details successfully added ✅️'
                },
                data: {

                }
            })



        })
    }

    catch (err) {
        
        console.log(`${new Date() } , /api/user_bank_details -> ${err}`)

}

}
const save_details_in_db = async (data) => {

    // console.log(data)  delete data.files;
    delete data.files;

    const result = await knex('public.user_bank')
        .insert(data);

    return result;

}
const schema = Joi.object({
   
    doc_link:Joi.string().required(),
    bank_name: Joi.string().required(),
    account_number: Joi.string().required(),
    ifsc_code: Joi.string().required(),
    branch_name: Joi.string().required(),
   // is_verified: Joi.string().valid('Yes', 'No').required(),
    // verified_by: Joi.string(),

})

// photo upload
router.post('/api/user_bank_details',  user_bank_details);

module.exports = router;
