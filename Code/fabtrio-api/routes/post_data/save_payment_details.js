const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');


const { decode_token } = require('../auth/token')



const save_payment_details = async (req, res, next) => {
    try {


        const input = {
            match_id: req.body.match_id,
            transaction_amount: req.body.transaction_amount,
            transaction_type: 1,
            transaction_status: 2,
            created_on: moment().format(),
            modified_on: moment().format(),
            token: req.headers.authorization,
            transaction_message:2

        }


    


        const data = req.body



        Joi.validate(data, schema, async (err, value) => {



            if (err) {
               
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }
            const phone_number_by_token = await decode_token(input.token, res)
           


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }



            const transaction_id = new Date().valueOf();

            input.transaction_id = `FAB${transaction_id}`;

            input.transaction_date = moment().format('MM/DD/YYYY');




           delete input.token;

           // console.log(input)

           const username = phone_number_by_token

           input.username = username;


            await save_details_in_db(input);

            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Transaction Successfully ✅️'
                },
                data: {
                    transaction_id: input.transaction_id
                }
            })


        })
    } catch (err) {
        
        console.log(`${new Date() } , /api/save_payment_details -> ${err}`)

}
}

const save_details_in_db = async (data) => {

   

    const result = await knex('public.transaction')
        .insert(data);

    return result;

}

const schema = Joi.object({
   
    match_id: Joi.number(),
    transaction_amount: Joi.number(),

})

router.post('/api/save_payment_details', save_payment_details);

module.exports = router;