const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const jsSHA = require("jssha");

const salt = process.env.SALT;

const payment_suceess = async (req, res, next) => {
    try {

        const data = req.body

        let generate_reverse_hash


        if (data.additional_charges != null) {

            const reverse_hash_sequence = `${data.additional_charges}|${salt}|${data.status}||||||${data.udf5}|${data.udf4}|${data.udf3}|${data.udf2}|${data.udf1}|${data.email}|${data.firstname}|${data.productinfo}|${data.amount}|${data.txnid}|${data.key}`

            const sha = new jsSHA('SHA-512', "TEXT");

            sha.update(reverse_hash_sequence);

            generate_reverse_hash = sha.getHash("HEX");

        } else {

            const reverse_hash_sequence = `${salt}|${data.status}||||||${data.udf5}|${data.udf4}|${data.udf3}|${data.udf2}|${data.udf1}|${data.email}|${data.firstname}|${data.productinfo}|${data.amount}|${data.txnid}|${data.key}`

            const sha = new jsSHA('SHA-512', "TEXT");

            sha.update(reverse_hash_sequence);

            generate_reverse_hash = sha.getHash("HEX");

        }


        const hash_from_response = data.hash


        if (generate_reverse_hash === hash_from_response) {

            const update_payment_status = await knex('public.transaction')
                .update('transaction_status', 1)
                .where('username', data.phone)
                .where('transaction_id', data.txnid)
        }



        const deposited_amount = await knex('public.user_wallet')
            .select('deposited_amount')
            .where('username', data.phone)

        // console.log(deposited_amount)

        if (deposited_amount.length === 0) {
            const prepared_data = {
                username: data.phone,
                bonus_amount: 0,
                deposited_amount: parseFloat(data.amount),
                winning_amount: 0
            }

            const add_amount = await knex('public.user_wallet')
                .insert(prepared_data)

        }
        else {

            // console.log(deposited_amount)

            const final_amount = (parseFloat(deposited_amount[0].deposited_amount) + parseFloat(data.amount)).toFixed(2)

            // console.log(final_amount)

            const upadate_amount = await knex('public.user_wallet')
                .update('deposited_amount', final_amount)
                .where('username', data.phone)

            //  console.log(upadate_amount)
        }
        // console.log(data.phone)

        const [user_register] = await knex('public.registration')
            .select('is_referal_bonus_credited', 'invitee_code')
            .where('phone_number', data.phone)

        //console.log(user_register)

        if (user_register.is_referal_bonus_credited === false) {

            //  console.log('is_referal_bonus_credited = false')

            if (user_register.invitee_code !== null) {

                // console.log('intiteecodenotnull')

                // console.log(user_register.invitee_code)
                const [referal_user] = await knex('public.invite_code')
                    .select('username')
                    .where('invite_code', user_register.invitee_code)

                //  console.log(referal_user)

                const get_user_wallet_details = await knex('public.user_wallet')
                    .select('bonus_amount')
                    .where('username', referal_user.username)

                // console.log('get_user_wallet_details.length', get_user_wallet_details.length)

                const [get_bonus_amount] = await knex('public.m_type_values')
                    .select('value_name')
                    .where('type_id', 10)
                    .where('value_id', 1)

                //  console.log('bonus_amount', get_bonus_amount)

                if (get_user_wallet_details.length === 0) {

                    const prepared_data = {
                        username: referal_user.username,
                        bonus_amount: parseInt(get_bonus_amount.value_name),
                        deposited_amount: 0,
                        winning_amount: 0


                    }

                    const add_bonus_amount = await knex('public.user_wallet')
                        .insert(prepared_data)
                    // console.log('islength=0', add_bonus_amount)

                    const update_is_bonus_cretided = await knex('public.registration')
                        .update('is_referal_bonus_credited', true)
                        .where('phone_number', data.phone)

                    //  console.log(update_is_bonus_cretided)

                    const time_stamp = new Date().valueOf();
                    const transaction_id = `FAB${time_stamp}`;
    
    
                    const transaction_data = {
                        transaction_id: transaction_id,
                        transaction_type: 1,
                        transaction_amount: parseInt(get_bonus_amount.value_name),
                        transaction_date: moment().format('YYYY-MM-DD'),
                        username: data.phone,
                        match_id: null,
                        created_on: moment().format(),
                        modified_on: moment().format(),
                        transaction_message: 'Welcome Bonus',
                        transaction_status: 1
                    }
    
                    // console.log(' tarntable', transaction_data)
    
                    const save_transaction_details_in_db = await knex('public.transaction')
                        .insert(transaction_data)
                        .returning('*')
    
    
                    const txn_id = `FABMASTER${time_stamp}`;
    
                    const prepare_data_status_1 = {
    
                        user_transaction_id: transaction_id,
                        username: data.phone,
                        transaction_id: txn_id,
                        amount: parseInt(get_bonus_amount.value_name),
                        transaction_type: 2,
                        payment_date: moment().format('YYYY-MM-DD'),
                        match_id: null,
                        created_on: moment().format(),
                        modified_on: moment().format(),
                        payment_status: 1
                    }
                    //   console.log('mastrtrnx', prepare_data_status_1)
                    
                    const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                        .insert(prepare_data_status_1)
                        .returning('*')

                }
                else {

                    const final_bonus_amount = parseFloat(get_user_wallet_details[0].bonus_amount) + parseFloat(get_bonus_amount.value_name)

                    //  console.log('final_bonus_amount', final_bonus_amount)

                    const update_bonus_amount = await knex('public.user_wallet')
                        .update('bonus_amount', final_bonus_amount)
                        .where('username', referal_user.username)

                    //  console.log('update_bonus_amount', update_bonus_amount)

                    const update_is_bonus_cretided = await knex('public.registration')
                        .update('is_referal_bonus_credited', true)
                        .where('phone_number', data.phone)

                    //console.log(update_is_bonus_cretided)

                    const time_stamp = new Date().valueOf();
                    const transaction_id = `FAB${time_stamp}`;
    
    
                    const transaction_data = {
                        transaction_id: transaction_id,
                        transaction_type: 1,
                        transaction_amount: parseInt(get_bonus_amount.value_name),
                        transaction_date: moment().format('YYYY-MM-DD'),
                        username: referal_user.username,
                        match_id: null,
                        created_on: moment().format(),
                        modified_on: moment().format(),
                        transaction_message:`fabtrio referral bonus for ${referal_user.username}`,
                        transaction_status: 1
                    }
    
                    // console.log(' tarntable', transaction_data)
    
                    const save_transaction_details_in_db = await knex('public.transaction')
                        .insert(transaction_data)
                        .returning('*')
    
    
                    const txn_id = `FABMASTER${time_stamp}`;
    
                    const prepare_data_status_1 = {
    
                        user_transaction_id: transaction_id,
                        username: referal_user.username,
                        transaction_id: txn_id,
                        amount: parseInt(get_bonus_amount.value_name),
                        transaction_type: 2,
                        payment_date: moment().format('YYYY-MM-DD'),
                        match_id: null,
                        created_on: moment().format(),
                        modified_on: moment().format(),
                        payment_status: 1
                    }
                    //   console.log('mastrtrnx', prepare_data_status_1)
                    
                    const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                        .insert(prepare_data_status_1)
                        .returning('*')

                }

            }


        }


        return res.render('success', { data: data })




    } catch (err) {

        console.log(`${new Date()} /success + err -> ${err}`)

    }

}
//success
router.post("/success", payment_suceess)


module.exports = router;