const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
const moment = require('moment-timezone');


const save_user_conest = async (req, res, next) => {

    try {

        const input = {
            match_id: req.body.match_id,
            user_team_id: req.body.user_team_id,
            participating_contest: req.body.participating_contest,
            total_amount: req.body.total_amount,
            token: req.headers.authorization
        }

        const data = req.body

        Joi.validate(data, schema, async (err, value) => {

            if (err) {

                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)

            // console.log(phone_number_by_token)

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')




            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

            const get_user_team_id = await knex('public.user_team')
                .where('user_team_id', input.user_team_id)
                .select('user_team_id')

            //  console.log(get_user_team_id)


            if (get_user_team_id.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Team not found`
                    },
                    data: {

                    }

                });
            }



            const participating_contest = input.participating_contest

            var flag = false;

            for (const contest of participating_contest) {

                const get_contest_details = await knex('public.user_contest')
                    .where('user_team_id', input.user_team_id)
                    .where('username', phone_number_by_token)
                    .where('c_id', contest.c_id)
                    .where('v_id', contest.v_id)
                    .select('*')



                if (get_contest_details.length != 0) {

                    flag = true

                }

            }

            if (flag === true) {
                return res.status(409).json({
                    meta: {
                        status: '1',
                        message: `Team and contest combination is already present`
                    },
                    data: {

                    }

                });
            }





            let user_contest_ids = []


            for (let index = 0; index < participating_contest.length; index++) {
                const element = participating_contest[index];

                const prepare_data = {
                    username: phone_number_by_token,
                    match_id: input.match_id,
                    user_team_id: input.user_team_id,
                    v_id: element.v_id,
                    c_id: element.c_id,
                    amount_paid: element.c_amount,
                    // transaction_id: 'null',

                }

                const add_contest_details = await save_details_in_db(prepare_data);

                const get_user_contest_id = add_contest_details[0].user_contest_id


                user_contest_ids.push(get_user_contest_id);



            }



            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Save User Contest Successfully ✅️'
                },
                data: {

                    user_contest_ids

                }
            })

        })

    } catch (err) {

        console.log(`${new Date()} , /api/save_user_conest -> ${err}`)

    }
}

const save_details_in_db = async (data) => {


    const result = await knex('public.user_contest')
        .insert(data)
        .returning('*');

    return result;

}

const schema = Joi.object({

    match_id: Joi.number(),
    user_team_id: Joi.number(),
    total_amount: Joi.number(),
    participating_contest: Joi.array(),

})

router.post('/api/save_user_conest', save_user_conest);

module.exports = router;