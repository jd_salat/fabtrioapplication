const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
const moment = require('moment-timezone');

const { final_amount } = require('../../helper/payment')

const initial_payment_flow = async (req, res, next) => {

    try {

        const input = {
            match_id: req.body.match_id,
            token: req.headers.authorization
        }

        const data = req.body

        Joi.validate(data, schema, async (err, value) => {

            if (err) {

                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)

          //  console.log(phone_number_by_token)

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')




            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

           const user_contest_ids = req.body.user_contest_ids

             // console.log(user_contest_ids)

          let  total_amount = 0
            for (const contest_id of user_contest_ids) {

               
                const [ {amount_paid} ]= await knex('public.user_contest')
                .where('user_contest_id' , contest_id)
                .select('amount_paid')

               
                   total_amount = total_amount + parseInt(amount_paid)

                   //  console.log(total_amount)
               
                
            }
            
            const get_match = await knex('public.match')
            .where('match_id' , input.match_id)
            .select('date_start' ,'title')
         


            const time_stamp = new Date().valueOf();
            const transaction_id = `FAB${time_stamp}`;
            

            const transaction_data = {
                transaction_id: transaction_id,
                transaction_type: 2,
                transaction_amount: total_amount,
                transaction_date: moment().format('YYYY-MM-DD'),
                username: phone_number_by_token,
                match_id: input.match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                transaction_message: `Towards contest entry fees for ${get_match[0].title} - ${moment(get_match[0].date_start).format('DD-MMM')}`
            }


            const save_transaction_details_in_db = await knex('public.transaction')
                .insert(transaction_data)
                .returning('*')

         // console.log('trantable',save_transaction_details_in_db)

            const txn_id = `FABMASTER${time_stamp}`;

            const prepare_data_status_1 = {

                user_transaction_id: transaction_id,
                username: phone_number_by_token,
                transaction_id: txn_id,
                amount: total_amount,
                transaction_type: 1,
                payment_date: moment().format('YYYY-MM-DD'),
                match_id: input.match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
            }

            const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                .insert(prepare_data_status_1)
                .returning('*')

         //   console.log('mastrtrn',save_transaction_status_1_details_in_db);
                 let final_balance;

                // console.log(transaction_id);

                // console.log(user_contest_ids);
            

                const update_user_contest_transaction_id = await knex('public.user_contest')
                .whereIn('user_contest_id', user_contest_ids)
                .update('transaction_id' , transaction_id)

         
               // console.log(update_user_contest_transaction_id)

               const get_payment_details = await knex('public.user_wallet')
               .where('username', phone_number_by_token)
               .select('bonus_amount',
                   'deposited_amount',
                   'winning_amount'
               )
       
               console.log(get_payment_details.length);
               
             if(get_payment_details.length === 0){
       
               return res.status(200).json({
                   
                   data: {
       
                       transaction_id,
                       total_amount,
                       current_bonus_balance : 0,
                       current_cash_balance:0,
                       eligible_amount : 0,
                       final_amount_to_pay:total_amount,
                       
       
                   }
       
               });
            }

                final_balance = await final_amount(phone_number_by_token, total_amount)

 //console.log(final_balance);




            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Payment Successfully ✅️'
                },
                data: {
                    transaction_id,
                    total_amount,
                    current_bonus_balance: final_balance.current_bonus_balance,
                    current_cash_balance: final_balance.current_cash_balance,
                    final_amount_to_pay: final_balance.final_amount_to_pay,
                    eligible_amount: final_balance.eligible_amount,
                }
            })

        })

    } catch (err) {

        console.log(`${new Date() } , /api/initial_payment_flow -> ${err}`)

        



    }
}



const schema = Joi.object({

    // username: Joi.string(),
     match_id: Joi.number(),
    // user_team_id: Joi.number(),
    // total_amount: Joi.number(),
    user_contest_ids: Joi.array(),

})

router.post('/api/initial_payment_flow', initial_payment_flow);

module.exports = router;