const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');

const { decode_token } = require('../auth/token')

const save_user_team = async (req, res, next) => {
    try {


        const input = {
          
            match_id: req.body.match_id,
            user_team: req.body.user_team,
            trump_card_id: req.body.trump_card_id,
            token: req.headers.authorization
        }
       

        const data = req.body;


        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }
            const phone_number_by_token = await decode_token(input.token, res)
            


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')

            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

           

            const player_ids = [];

            const user_team = input.user_team;

            user_team.forEach(element => {


                player_ids.push(element.player_id)
            });


          

            const player_1_data = await knex('public.user_team as ut')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .where('ut.username', phone_number_by_token)
                .where('ut.match_id', input.match_id)
                .where('ut.player_id', player_ids[0])
                .where('utc.trump_card', input.trump_card_id)
                .select('*')

            const player_2_data = await knex('public.user_team as ut')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .where('ut.username', phone_number_by_token)
                .where('ut.match_id', input.match_id)
                .where('ut.player_id', player_ids[1])
                .where('utc.trump_card', input.trump_card_id)
                .select('*')


            const player_3_data = await knex('public.user_team as ut')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .where('ut.username', phone_number_by_token)
                .where('ut.match_id', input.match_id)
                .where('ut.player_id', player_ids[2])
                .where('utc.trump_card', input.trump_card_id)
                .select('*')


            if (player_1_data.length
                && player_2_data.length
                && player_3_data.length) {

                return res.status(409).json({
                    meta: {
                        status: '1',
                        message: `Team with same players is already present`
                    },
                    data: {

                    }

                });


            }


            const [{ last_value: latest_team_id }] = await knex('public.user_team_seq')
                .select('last_value')

            for (let index = 0; index < user_team.length; index++) {
                const element = user_team[index];

                const prepare_data = {
                    user_team_id: parseInt(latest_team_id) + 1,
                    username: phone_number_by_token,
                    match_id: input.match_id,
                    player_id: element.player_id,
                    is_star_player: element.is_star,
                   
                }

                await save_details_in_db(prepare_data);

            }

            const trump_card_mapping = {
                user_team_id: parseInt(latest_team_id) + 1,
                trump_card: input.trump_card_id,
               
            }


            const save_trump_card_mapping = await knex('public.user_trump_card')
                .insert(trump_card_mapping);


            const update_sequence = await knex.schema.withSchema('public').raw(`ALTER SEQUENCE user_team_seq RESTART WITH ${trump_card_mapping.user_team_id}`)


            
            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Save User Team Successfully ✅️'
                },
                data: {
                    "user_team_id": parseInt(latest_team_id) + 1
                }
            })


        })
    } catch (err) {
        
        console.log(`${new Date() } , /api/save_user_team -> ${err}`)



      

}

}
const save_details_in_db = async (input) => {


    const result = await knex('public.user_team')
        .insert(input);

    return result;

}

const schema = Joi.object({
    
    match_id: Joi.number(),
    user_team: Joi.array().min(3).max(3),
    trump_card_id: Joi.number(),

})


router.post('/api/save_user_team', save_user_team);

module.exports = router;