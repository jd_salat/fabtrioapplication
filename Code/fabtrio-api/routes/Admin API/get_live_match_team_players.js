const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')

const player_url = require('../../helper/get_player_url');
const { get_fantasy_point } = require('../../helper/player_fantasy_point')

const get_live_match_team_players = async (req, res, next) => {




    let response = {};

    try {
        const get_live_match = await knex('public.match')
            .select('title', 'status_str', 'match_id')
            .whereIn('status',[3,5])
            .orderBy('status' ,'desc')
            .where('format', '!=' ,17)

           // console.log(get_live_match.length)
            

        for (let index = 0; index < get_live_match.length; index++) {
            const element = get_live_match[index];

           // console.log(element.match_id)

            const teama_details_array = [];
            const teamb_details_array = [];

            const team_details = await knex('public.match')
                .select('team_one_id', 'team_two_id')
                .where('match_id', element.match_id)

          //  console.log(team_details)

            const get_teama_details = await knex("public.player as p")
                .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
                .select('ts.player_id',
                    'p.playing_role as role',
                    'ts.role_str',
                    'ts.playing11',
                    'p.title',
                    'p.first_name',
                    'p.last_name'
                    // 'p.thumb_url',
                    // 'p.logo_url'
                )
                .where('ts.playing11', 'true')
                .where('ts.match_id', element.match_id)
                .where('ts.team_id', team_details[0].team_one_id);

          //  console.log('get_live_match_team_players_detais', get_teama_details.length)


            for (let i = 0; i < 11; i++) {
                const player = get_teama_details[i];

               // console.log('get_live_match_team_players_detais', get_teama_details.length)
               // console.log(i);
                // console.log(player);
                const player_id = player.player_id
                 console.log(player_id)
                const { fantasy_points } = await get_fantasy_point(element.match_id, player_id)

                const { thumb_url } = await player_url(element.match_id, player_id);


                const player_obj = {
                    "player_id": player_id,
                    "role": player.role,
                    "role_str": player.role_str,
                    "playing11": player.playing11,
                    "title": player.title,
                    "first_name": player.first_name,
                    "last_name": player.last_name,
                    "thumb_url": thumb_url,
                    "fantasy_point": fantasy_points
                }

                // console.log(point)

                teama_details_array.push(player_obj)
            }







            const teama_response = {}

            const teama_details = await get_team_details(team_details[0].team_one_id);

            teama_response.team_details = teama_details

            teama_response.get_teama_details = teama_details_array



            const get_teamb_details = await knex("public.player as p")
                .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
                .select(
                    'ts.player_id',
                    'p.playing_role as role',
                    'ts.role_str',
                    'ts.playing11',
                    'p.title',
                    'p.first_name',
                    'p.last_name',
                    //'p.thumb_url'
                    // 'p.logo_url'
                )
                .where('ts.playing11', 'true')
                .where('ts.match_id', element.match_id)
                .where('ts.team_id', team_details[0].team_two_id);


           // console.log('get_live_match_team_players_detais', get_teamb_details.length)


            for (let index = 0; index < 11; index++) {
                const player = get_teamb_details[index];


                // console.log(player);
                const player_id = player.player_id
                // console.log(player_id)
                const { fantasy_points } = await get_fantasy_point(element.match_id, player_id)

                const { thumb_url } = await player_url(element.match_id, player_id);

                const player_obj = {
                    "player_id": player_id,
                    "role": player.role,
                    "role_str": player.role_str,
                    "playing11": player.playing11,
                    "title": player.title,
                    "first_name": player.first_name,
                    "last_name": player.last_name,
                    "thumb_url": thumb_url,
                    "fantasy_point": fantasy_points
                }
               // console.log(player_obj)

                teamb_details_array.push(player_obj)
            }



            const teamb_response = {}

            const teamb_details = await get_team_details(team_details[0].team_two_id);


            teamb_response.team_details = teamb_details

            teamb_response.get_teamb_details = teamb_details_array;

            response.get_live_match = get_live_match;
            response.team_a = teama_response;
            response.team_b = teamb_response;


        }




        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched'
            },
            data: {
                response
            }
        })


    } catch (err) {

        console.log(`${new Date()} /api/get_live_match_team_players -> ${err}`)


    }

}




const get_team_details = async (team_id) => {

    const result = await knex('public.team')
        .where('team_id', team_id)
        .select(
            'short_name',
            'logo_url',
            'name'
        )


    return result;
}

router.get('/api/get_live_match_team_players', get_live_match_team_players);


module.exports = router;