const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')


const add_admin = async (req, res, next) => {

    try {
        const input =
        {
            name: req.body.name,
            role: req.body.role,
            user_id: req.body.user_id,
            email: req.body.email,
            phone_number: req.body.phone_number,
            status: req.body.status,
            password: req.body.password


        }

        const data = req.body


        Joi.validate(data, schema, async (err, value) => {

            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });

            }



            const get_admin = await knex('public.admin')
                             .where('phone_number', input.phone_number)
                

                console.log(get_admin.length)

                if(get_admin.length !== 0){

                    return res.status(409).json({
                        meta: {
                            message: 'Admin Alredy Presenrt ✅️'
                        },
                        data: {
        
                        }
                    })
                }



          await save_details_in_db(input);
            //  console.log(url);



            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Admin successfully added ✅️'
                },
                data: {

                }
            })


        })

    }

    catch (err) {

        console.log(`${new Date()} , /api/add_admin -> ${err}`)


    }

}
const save_details_in_db = async (data) => {

    // console.log(data)  

    const result = await knex('public.admin')
        .insert(data);

    return result;

}
const schema = Joi.object({
    name: Joi.string().required(),
    role: Joi.string().required(),
    user_id: Joi.string().required(),
    email: Joi.string().allow(''),
    phone_number: Joi.string().regex(/^\d{3}\d{3}\d{4}$/).required(),
    status: Joi.string().required(),
    password: Joi.string().required()


})

// photo upload
router.post('/api/add_admin', add_admin);

module.exports = router;
