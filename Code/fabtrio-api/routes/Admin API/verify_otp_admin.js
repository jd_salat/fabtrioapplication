
//import  module

const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { generate_token} = require('../auth/token')

//verify otp
const verify_otp = async (req, res, next) => {


    try {

        const input = {
            phone_number:req.body.phone_number,
            password:req.body.password,
            otp:req.body.otp
        }
        
            console.log(input)
           
        
        const [get_user] = await knex('public.admin_login')
            .where('phone_number', input.phone_number)
            .where('password' , input.password)
            .select('*')

        // return;

       console.log(get_user)
        if (get_user.length === 0) {

            return res.status(400).send({
                meta: {
                    status: '0',
                    message: '⚠️ You are not registered with us'
                }
            })
        } else if (get_user.password !== input.password || get_user.mobile_otp !== input.otp) {

            console.log('elseif with password and otp')

            return res.status(400).send({

                meta: {
                    status: '1',
                    message: '⚠️ Enter valid password & otp'
                },
                data: {

                }
            })


        } else if (get_user.verification_status === true) {


           // console.log(get_user.verification_flag)

            return res.status(400).send({

                meta: {
                    status: '2',
                    message: '⚠️ You are alredy verify this Otp , Please generate new otp'
                },
                data: {

                }

            })

        } else {

            const opt_generation_time = get_user.modified_at;

            const current_time = new Date();

            const current_time_in_unix_time = Math.floor(current_time / 1000);
            const otp_generation_time_in_unix_time = Math.floor(opt_generation_time / 1000);

            const time_diffrenece = current_time_in_unix_time - otp_generation_time_in_unix_time;

            if (time_diffrenece > 900) {

                return res.status(401).send({

                    meta: {
                        status: '3',
                        message: '⚠️ Your otp is expired'
                    },
                    data: {

                    }

                })
            }

            const get_token = await generate_token(input.phone_number, '1h');

            const save_token_db = await knex('public.admin_login')
                .update('token', get_token)
                .update('verification_status', true)
                .where('phone_number', req.body.phone_number)

            //console.log(get_user.refresh_token);


            return res.status(200).send({

                meta: {
                    status: 'OK',
                    message: 'Otp verified and login successfully ✅️'
                },
                data:{
                    auth: true,
                    token: get_token,
                    expires_in: 3600,
                    
                }

            })
        }

    } catch (err) {
        
        console.log(`${new Date() } , /api/verify_otp -> ${err}`)

}
}

// const decrypt = (text) => {

//     console.log(text)
//     var decipher = crypto.createDecipher(algorithm, my_secret+text)
//     var dec = decipher.update(text, 'hex', 'utf8')
//     dec += decipher.final('utf8');
//     return dec;
// }


router.post('/api/verify_otp_admin', verify_otp);

module.exports = router;

