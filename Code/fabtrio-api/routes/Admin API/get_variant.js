const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone')



const get_variant = async (req, res, next) => {

    try {


        const variant_details = await knex('public.contest_variance')
            .select(
                'v_id',
                'v_name',
                'max_participants_per_group',
                'no_of_winners_per_group',
                'created_on',
                'modified_on',
                'reward_distribution'
                )



        const final_response = variant_details

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get Variant Details  ✅️'
            },
            data: final_response


        })

    } catch (err) {

        console.log(`${new Date()} , /api/get_variant -> ${err}`)

        
    }
}


// registration  api
router.get('/api/get_variant', get_variant);

module.exports = router;