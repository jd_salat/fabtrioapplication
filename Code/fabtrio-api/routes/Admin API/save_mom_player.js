const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex')
const moment = require('moment-timezone')

const add_mom = async (req, res, next) => {

    try {

        const input = {

            match_id: req.body.match_id,
            player_id: req.body.player_id
        }

        const update_player_id = await knex('public.match_stat')
            .where('match_id', input.match_id)
            .update('mom_player_id', input.player_id)
            .returning('*')

            console.log(update_player_id)

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully add mom'
            },
            data: {
                update_player_id
            }
        })


    } catch (err) {

        console.log(`${new Date()} , /api/upadate_mom -> ${err}`)

    }
}
router.put('/api/upadate_mom', add_mom);

module.exports = router