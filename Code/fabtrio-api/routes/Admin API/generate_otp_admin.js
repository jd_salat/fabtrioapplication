//import  module

const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
var unirest = require("unirest");
const crypto = require('crypto');

const my_secret = process.env.MY_SECRET;
const algorithm = process.env.ALGORITHM;



const Joi = require('joi');

const otp_generate = async (req, res, next) => {


    try {

        const data = req.body;

        const input = {
            phone_number: data.phone_number,
            password: data.password
        };


        console.log(input);

        Joi.validate(data, schema, async (err, value) => {

            const find_details = await knex('public.admin_login')
                .where('phone_number', input.phone_number)
                .where('password', input.password)
            // console.log(find_details)


            console.log(find_details.length)

            if (find_details && find_details.length > 0) {

                const otp = await generateOTP();

                const add_otp_in_db = await knex('public.admin_login')
                    .update("mobile_otp", otp)
                    .update('modified_on', moment())
                    .update('verification_status', false)
                    .where("phone_number", "=", input.phone_number)
                    .returning("*")

                //await send_sms(input, otp);
                return res
                    .status(200)
                    .json({
                        meta:
                        {
                            status: 'OK',
                            message: 'Send Otp ✅️'
                        },
                        data: {

                            'otp': otp,
                        }
                    })

            } else {

                return res
                    .status(404)
                    .json({
                        meta:
                        {
                            status: 'OK',
                            message: "⚠️ We didn't found your mobile number in our record"
                        },
                        data: {
                            // 'otp': otp,
                        }
                    })


            }
        })

    } catch (err) {

        console.log(`${new Date()} , /api/otp/generate_otp_admin -> ${err}`)


    }
}

// Function to generate OTP
const generateOTP = () => {

    // Declare a digits variable which stores all digits
    const digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < 6; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
}

// create new contact
const send_sms = async (data, otp, res) => {

    // console.log(data);
    // console.log(otp)

    let phone_number = data.phone_number;
    try {
        var req = unirest("POST", "http://mobicomm.dove-sms.com/submitsms.jsp?");

        // req.headers({
        //     "authorization": "8db582baeaXX"
        // });

        req.form({
            "user": "HARISH",
            "senderid": "SCHOOL",
            "key": "8db582baeaXX",
            "message": `Your otp is ${otp}`,
            "language": "english",
            "accusage": "1",
            // "route": "p",
            "mobile": phone_number,
        });

        req.end(function (res) {
            if (res.error) throw new Error(res.error);

            console.log(res.body);
        });
        // res.json(resMsg);

    } catch (error) {
        // throw error;

        console.log(error);

    }
}


const schema = Joi.object({
    phone_number: Joi.string().regex(/^\d{3}\d{3}\d{4}$/).required(),
    password: Joi.string().required()

})

// const decrypt = (text) => {

//     var decipher = crypto.createDecipher(algorithm, my_secret+text)
//     var dec = decipher.update(text, 'hex', 'utf8')
//     dec += decipher.final('utf8');
//     return dec;
// }

// otp generate api
router.put('/api/otp/generate_otp_admin', otp_generate);

module.exports = router;
