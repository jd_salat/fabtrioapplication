const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
var CronJob = require('cron').CronJob;


// var job = new CronJob('*/180  * * * *', async () => {


//     await change_match_status_to_live_scheduler()

//     console.log('change_match_status_to_live_scheduler-You will see this message every 2 minutes');


// }, null, true, 'America/Los_Angeles');

// job.start();




const change_match_status_to_live_scheduler = async () => {

    try {

        const get_matches = await knex('public.match')
            .where('status', 1)
            .select('match_id', 
            'timestamp_start')


        const current_time = moment()

       

        if(get_matches.length === 0){

            return
        }

        for (let index = 0; index < get_matches.length; index++) {
            const element = get_matches[index];
            

        if (element.timestamp_start <= current_time) {

            const update_match_status = await knex('public.match')
                .where('match_id', element.match_id)
                .update('status', 3)
                .returning('*')


            console.log(update_match_status)


        }
    }

    } catch (err) {

        console.log(`${new Date()} ,/api/change_match_status_to_live_scheduler -> ${err}`)


    }





}







router.get('/api/change_match_status_to_live_scheduler', change_match_status_to_live_scheduler);

module.exports = router;