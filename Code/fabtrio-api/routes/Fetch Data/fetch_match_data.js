const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');


const rp = require('request-promise-native');

const token = process.env.TOKEN;

//format pass thay to append krvani nai to nai
var CronJob = require('cron').CronJob;
// var job = new CronJob('*/180 * * * *', async () => {

//     const data = {
//         status: 2,
//         per_page: 7,
//         pre_squad: true,
//         paged: 1,
//         // format: 6,

//     }

//     await get_fetch_match_data(data)


//     console.log(' Get_fetch_match_data - You will see this message every 30 minutes');


// }, null, true, 'America/New_York');
// job.start();

const get_fetch_match_data = async (data) => {

    // console.log(`req params ${JSON.stringify(req.query)}`)

    const options = {
        uri: `https://rest.entitysport.com/v2/matches/`,
        qs: {
            status: data.status,
            per_page: data.per_page,
            token: token,
            pre_squad: data.pre_squad,
            paged: data.paged,
            // format: data.format,

        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    }
    // console.log(options)

    rp(options).then(async (repos) => {

        await save_details_in_db(repos.response.items);

        return;
        // return res
        //     .status(200)
        //     .send({
        //         status: 'SUCCESS',
        //         response: repos.response
        //     });

    })
        .catch(function (err) {

            // console.error(err.message); API call failed...
            return
            // res
            //     .status(500)
            //     .send({ status: 'FAILURE' });

        });


}


const save_details_in_db = async (data) => {

    try {


        for (let index = 0; index < data.length; index++) {
            const element = data[index];



            const get_venue = await knex('public.venue')
                .where('name', element.venue.name)
                .where('location', element.venue.location)
                .select('*')

            if (get_venue.length === 0) {

                const venue_obj = {
                    "name": element.venue.name,
                    "location": element.venue.location,
                    "timezone": element.venue.timezone
                }

                const [{ venue_id: venue_id }] = await knex('public.venue')
                    .insert(venue_obj)
                    .returning('*')

                data.venue_id = venue_id

            } else {

                data.venue_id = get_venue[0].venue_id;
            }


            const toss_obj = {
                "text": element.toss.text,
                "winner": element.toss.winner,
                "decision": element.toss.decision,
                "match_id": element.match_id
            }
            // console.log(toss_obj)

            let toss_id;

            const get_toss = await knex('public.toss_table')
                .where('match_id', element.match_id)
                .select('*')


            if (get_toss.length === 0) {

                const add_toss = await knex('public.toss_table')
                    .insert(toss_obj)
                    .returning('*')

                toss_id = add_toss[0].toss_id;

            } else {

                const replace_toss_data = await knex('public.toss_table')
                    .where('match_id', element.match_id)
                    .update(toss_obj)
                    .returning('*')

              //  console.log('inside else')

                toss_id = replace_toss_data[0].toss_id;

            }


            // console.log(element.match_id);

            const match_table_obj = {
                "match_id": element.match_id,
                "title": element.title,
                "subtitle": element.subtitle,
                "format": element.format,
                "format_str": element.format_str,
                "status": element.status,
                "status_str": element.status_str,
                "status_note": element.status_note,
                "verified": element.verified,
                "pre_squad": element.pre_squad,
                "game_state": element.game_state,
                "game_state_str": element.game_state_str,
                "competition_id": element.competition.cid,
                "team_one_id": element.teama.team_id,
                "team_two_id": element.teamb.team_id,
                "date_start": element.date_start,
                "date_end": element.date_end,
                "timestamp_start": moment.unix(element.timestamp_start).format(),
                "timestamp_end": moment.unix(element.timestamp_end).format(),
                "venue_id": data.venue_id,
                "umpires": element.umpires,
                "referee": element.referee,
                "equation": element.equation,
                "live": element.live,
                "win_margin": element.win_margin,
                "toss_id": toss_id,
            }

          //  console.log(element.timestamp_start)

            const get_match = await knex('public.match')
                .where('match_id', element.match_id)
                .select('*')


            // console.log(get_match.length)
            if (get_match.length === 0) {

                con

                const match = await knex('public.match')
                    .insert(match_table_obj)
                    .returning('*')

            } else {

                const replace_match_data = await knex('public.match')
                    .where('match_id', element.match_id)
                    .update(match_table_obj)
                    .returning('*')

            }


            const competition_obj = {
                "cid": element.competition.cid,
                "title": element.competition.competition_title,
                "abbr": element.competition.abbr,
                "category": element.competition.category,
                "game_format": element.competition.match_format,
                "status": element.competition.status,
                "season": element.competition.season,
                "datestart": element.competition.datestart,
                "dateend": element.competition.dateend,
                "total_matches": element.competition.total_matches,
                // "thumbanail_url": null,
                "total_rounds": element.competition.total_rounds,
                "total_teams": element.competition.total_teams,
                "squad_type": null,
                "country": element.competition.country,
                "table": null,
                "rounds": null
            }

            const get_cometition = await knex('public.competition')
                .where('cid', element.competition.cid)
                .select('*')

            if (get_cometition.length === 0) {

                const competition = await knex('public.competition')
                    .insert(competition_obj)
                    .returning('*')

            } else {

                const replace_competition_obj = await knex('public.competition')
                    .where('cid', element.competition.cid)
                    .update(competition_obj)
                    .returning('*')

            }

            // const teama_obj = {
            //     "team_id": element.teama.team_id,
            //     "name": element.teama.name,
            //     "short_name": element.teama.short_name,
            //     "logo_url": element.teama.logo_url,
            //     "scores_full": element.teama.scores_full,
            //     "scores": element.teama.scores,
            //     "overs": element.teama.overs
            // }

            // const get_teama = await knex('public.team')
            //     .where('team_id', element.teama.team_id)
            //     .select('*')

            // if (get_teama.length === 0) {

            //     const team1 = await knex('public.team')
            //         .insert(teama_obj)
            //         .returning('*')

            // } else {

            //     const team1 = await knex('public.team')
            //         .where('team_id', element.teama.team_id)
            //         .update(teama_obj)
            //         .returning('*')
            // }



            // const teamb_obj = {
            //     "team_id": element.teamb.team_id,
            //     "name": element.teamb.name,
            //     "short_name": element.teamb.short_name,
            //     "logo_url": element.teamb.logo_url,
            //     "scores_full": element.teamb.scores_full,
            //     "scores": element.teamb.scores,
            //     "overs": element.teamb.overs
            // }


            // const get_teamb = await knex('public.team')
            //     .where('team_id', element.teamb.team_id)
            //     .select('*')

            // if (get_teamb.length === 0) {

            //     const team2 = await knex('public.team')
            //         .insert(teamb_obj)
            //         .returning('*')

            // } else {

            //     const team2 = await knex('public.team')
            //         .where('team_id', element.teamb.team_id)
            //         .update(teamb_obj)
            //         .returning('*')
            // }



        }

    } catch (err) {
        
        console.log(err ,'/api/get_fetch_match_data')

      

}

}




router.get('/api/get_fetch_match_data', get_fetch_match_data);

module.exports = router;