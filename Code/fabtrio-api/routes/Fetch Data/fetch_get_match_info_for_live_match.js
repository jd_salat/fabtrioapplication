const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const rp = require('request-promise-native');
var CronJob = require('cron').CronJob;

const token = process.env.TOKEN;


// var job = new CronJob('*/180 * * * *', async () => {


//     await get_match_info_for_live_match()

//     console.log('Get_match_info_for_live_match - You will see this message every 2 minutes');


// }, null, true, 'America/Los_Angeles');

// job.start();




const get_match_info_for_live_match = async () => {


    const live_matches = await knex('public.match')
        .where('status', 3)
        .select('match_id')

    if (live_matches.length === 0) {

        return
    }

    console.log(live_matches);

    for (let index = 0; index < live_matches.length; index++) {
        const { match_id } = live_matches[index];

        const options = {
            uri: `https://rest.entitysport.com/v2/matches/${match_id}/info`,
            qs: {

                token: token,

            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        }


        rp(options).then(async (repos) => {

            await save_details_in_db(repos.response, match_id);


            return;

        })
            .catch(function (err) {

                console.error(err.message); // API call failed...
                return

            });


    }






}


const save_details_in_db = async (data, match_id) => {


    try {

        if (data.verified === 'true') {


            await update_match_status_in_db(data, match_id);

        }

        const match_state_model = {
            match_id: data.match_id,
            match_date: moment(data.date_start).format('YYYY-MM-DD'),
            teama_id: data.teama.team_id,
            teamb_id: data.teamb.team_id,
            result: data.result,
            winning_team_id: data.winning_team_id,
            competition_name: data.competition.title,
            teama_scores: data.teama.scores,
            teama_overs: data.teama.overs,
            teamb_scores: data.teamb.scores,
            teamb_overs: data.teamb.overs
        }


        const get_match_data_from_db = await knex('public.match_stat')
            .where('match_id', match_id)
            .select('*')

        if (get_match_data_from_db.length === 0) {

            const add_details_in_db = await knex('public.match_stat')
                .where('match_id', match_id)
                .insert(match_state_model);

            return add_details_in_db

        } else {

            match_state_model.modified_on = moment();

            const update_details_in_db = await knex('public.match_stat')
                .where('match_id', match_id)
                .update(match_state_model);

            return update_details_in_db;
        }




    } catch (err) {

        console.log(err, '/api/get_match_info_for_live_match')


    }

}


const update_match_status_in_db = async (data, match_id) => {

    const update_model = {
        'status': data.status,
        'status_str': data.status_str,
        'status_note': data.status_note,
        'verified': data.verified,
    }

    const update_match_status = await knex('public.match')
        .where('match_id', match_id)
        .update(update_model)


    const options = {
        uri: `https://rest.entitysport.com/v2/matches/${match_id}/point`,
        qs: {

            token: token,
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    }


    rp(options).then(async (repos) => {


        await find_batsmen_and_bowler_of_the_match(repos.response.points, match_id);

        return;
        //  res
        //     .status(200)
        //     .send({
        //         status: 'SUCCESS',
        //         response: repos.response
        //     });

    })
        .catch(function (err) {

            console.error(err.message); // API call failed...
            return
            // res
            //     .status(500)
            //     .send({ status: 'FAILURE' });

        });

}

const find_batsmen_and_bowler_of_the_match = async (data, match_id) => {


    const batsmens_points = [];

    const bowler_points = [];

    const teama_playing11 = data.teama.playing11;

    for (let index = 0; index < teama_playing11.length; index++) {
        const element = teama_playing11[index];

        if (element.role === 'bat') {

            const obj = {
                player_id: element.pid,
                total_points: parseFloat(element.point)
            }

            batsmens_points.push(obj);
        }

        if (element.role === 'bowl') {

            const obj = {
                player_id: element.pid,
                total_points: parseFloat(element.point)
            }

            bowler_points.push(obj);
        }

    }

    const teamb_playing11 = data.teamb.playing11;

    for (let index = 0; index < teamb_playing11.length; index++) {
        const element = teamb_playing11[index];

        if (element.role === 'bat') {

            const obj = {
                player_id: element.pid,
                total_points: parseFloat(element.point)
            }

            batsmens_points.push(obj);
        }

        if (element.role === 'bowl') {

            const obj = {
                player_id: element.pid,
                total_points: parseFloat(element.point)
            }

            bowler_points.push(obj);
        }

    }

    const batsmens_with_higher_points = batsmens_points.reduce(function (prev, current) {

        return (prev.total_points > current.total_points) ? prev : current

    })


    const bowler_with_higher_points = bowler_points.reduce(function (prev, current) {

        return (prev.total_points > current.total_points) ? prev : current

    })

    const update_in_db = await knex('public.match_stat')
        .where('match_id', match_id)
        .update('bow_om_player_id', bowler_with_higher_points.player_id)
        .update('bat_om_player_id', batsmens_with_higher_points.player_id);



}

router.get('/api/get_match_info_for_live_match', get_match_info_for_live_match);

module.exports = router;