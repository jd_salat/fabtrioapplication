const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token } = require('../auth/token')

const player_url = require('../../helper/get_player_url');

const get_teams = async (req, res, next) => {
    const input = {
        token: req.headers.authorization
    }


    const phone_number_by_token = await decode_token(input.token, res)

    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }
    const match_id = req.query.match_id;
    //console.log('match_id' , match_id)

    let final_response = [];
   

    try {

        const get_teams_details = await knex("public.player as p")
            .leftJoin('public.team_squad as ts', 'ts.player_id', 'p.player_id')
            .select('ts.player_id',
                'p.playing_role as role',
                'ts.role_str',
                'ts.playing11',
                'p.short_name as title',
                // 'p.thumb_url',
                // 'p.logo_url'
            )
            .where('ts.match_id', match_id);

      //  console.log('get_teams_detais' , get_teams_details)


        if (get_teams_details.length) {



            for (let index = 0; index < get_teams_details.length; index++) {
                const element = get_teams_details[index];

               
                const player_id = element.player_id

                const url = await player_url(match_id, player_id)

                get_teams_details[index].thumb_url = url.thumb_url
                get_teams_details[index].logo_url = url.default_url
                
                

                delete element.fantasy_points;

                last_five_match_stats = [];

                const match_point = await knex('public.player_stat as ps')
                    .leftJoin('public.match as m', 'm.match_id', 'ps.match_id')
                    .where('ps.player_id', element.player_id)
                    .orderBy('m.date_start', 'asc')
                    .limit(5)
                    .select('ps.fantasy_points')


                for (let index = 0; index < match_point.length; index++) {
                    const element = match_point[index];

                    const obj = {
                        "match_point": element.fantasy_points
                    }

                    last_five_match_stats.push(obj);

                }

                element.last_five_match_stats = last_five_match_stats;

            }


            // team_id: get_teams_details[0].team_id,
           squads = get_teams_details
            //  console.log(squads)


        }else {

            squads = []
        }

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched'
            },
            data: {
                squads
            }
        })


    } catch (err) {

        console.log(`${new Date() } /api/get_teams -> ${err}`)

        
    }

}






router.get('/api/get_teams', get_teams);


module.exports = router;