const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token');
const moment = require('moment')
const team_url = require('../../helper/get_team_url')




const get_match_commentry = async (req, res, next) => {

    const input = {
        token: req.headers.authorization
    }

    const phone_number_by_token = await decode_token(input.token, res)

    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }

    const match_id = req.query.match_id;

    var final_response = {}


    try {

        const match_details = await knex('public.match as m')
            .leftJoin('public.match_stat as mt', 'mt.match_id', 'm.match_id')
            .where('m.match_id', match_id)
            .select('m.team_one_id',
                'm.team_two_id',
                'mt.teama_scores',
                'mt.teama_overs',
                'mt.teamb_scores',
                'mt.teamb_overs',
                )


        let commentry = []

        const teams_select_query = await knex('public.team as t')
            .whereIn('team_id', [match_details[0].team_one_id, match_details[0].team_two_id])
            .select('*')

           // console.log( new Date () , `teams_id -> ${JSON.stringify(teams_select_query)}`)
           
        if (teams_select_query.length > 0) {

            const teama_url = await team_url(teams_select_query[0].team_id)
            teama_data_obj = {

                'team_id': teams_select_query[0].team_id,
                'name': teams_select_query[0].name,
                'short_name': teams_select_query[0].short_name,
                'logo_url': teama_url.logo_url,
                'scores': match_details[0].teama_scores,
                'overs': match_details[0].teama_overs
            }

           // console.log( new Date(),'teama_data_obj',JSON.stringify(teama_data_obj))
           const teamb_url = await team_url(teams_select_query[1].team_id)
            teamb_data_obj = {
                'team_id': teams_select_query[1].team_id,
                'name': teams_select_query[1].name,
                'short_name': teams_select_query[1].short_name,
                'logo_url': teamb_url.logo_url,
                'scores': match_details[0].teamb_scores,
                'overs': match_details[0].teamb_overs
            }

           // console.log(new Date (),'teamb_data_obj' , JSON.stringify(teamb_data_obj))

            const commentary = await knex('public.ball_by_ball')
                .where('match_id', match_id)
                .orderBy('inning_number' , 'desc')
                .orderBy('over', "desc")
                .orderBy('ball', "desc")
                .limit(10)
                .select('*')

            for (let index = 0; index < commentary.length; index++) {
                const element = commentary[index];

                const commentry_obj = {
                    event: element.event,
                    over: element.over,
                    ball: element.ball,
                    score: element.score,
                    commentary: element.commentary,
                }

                commentry.push(commentry_obj);
            }

            prepared_data = {
                match_id: match_id,
                commentary: commentry,
                teama: teama_data_obj,
                teamb: teamb_data_obj,
            }

            final_response = prepared_data;
        }


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                "items": final_response
            }
        })



    } catch (err) {

        console.log(`${new Date() } ,/api/get_match_commentry ->${err}`)

    
    }


}



router.get('/api/get_match_commentry', get_match_commentry);


module.exports = router;