const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token } = require('../auth/token')


const get_last_five_match_details = async (req, res, next) => {


    const input = {
        token: req.headers.authorization
    }

    //console.log(input);

    const phone_number_by_token = await decode_token(input.token, res)

    // console.log(input.token)

    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }

    const match_id = req.query.match_id;

    const match_date = req.query.match_date;

    try {

        const final_response = {};

        final_response.match_id = match_id


        const last_five_match_winning_team = await knex('public.match_stat as ms')
            .leftJoin('public.match as m', 'm.team_one_id', '=', 'ms.teama_id', 'm.team_two_id', '=', 'ms.teamb_id')
            .where('m.match_id', match_id)
            .where('ms.match_date', '<', match_date)
            .limit('5')
            .orderBy('ms.match_date', 'desc')
            .select('ms.winning_team_id')

        final_response.last_five_match_stats = last_five_match_winning_team;


        const team_ids = await knex('public.match')
            .where('match_id', match_id)
            .select('team_one_id', 'team_two_id')

        const teama_obj = {
            "team_id": team_ids[0].team_one_id
        }

        const teamb_obj = {
            "team_id": team_ids[0].team_two_id
        }



        const teama_last_five_match_winning_status = await knex('public.match_stat')
            .orWhere('teama_id', team_ids[0].team_one_id)
            .orWhere('teamb_id', team_ids[0].team_two_id)
            .orderBy('match_date', 'desc')
            .limit('5')

        const teama_last5_results = [];

        teama_last_five_match_winning_status.forEach(element => {


            if (element.teama_id === element.winning_team_id) {

                const result = {
                    "hasWon": true
                }

                teama_last5_results.push(result);

            } else {

                const result = {
                    "hasWon": false
                }

                teama_last5_results.push(result);

            }

        });

        teama_obj.last_five_result = teama_last5_results;

        final_response.teama = teama_obj


        const teamb_last_five_match_winning_status = await knex('public.match_stat')
            .orWhere('teama_id', team_ids[0].team_two_id)
            .orWhere('teamb_id', team_ids[0].team_two_id)
            .orderBy('match_date', 'desc')
            .limit('5')

        const teamb_last5_results = [];

        teamb_last_five_match_winning_status.forEach(element => {


            if (element.teamb_id === element.winning_team_id) {

                const result = {
                    "hasWon": true
                }

                teamb_last5_results.push(result);

            } else {

                const result = {
                    "hasWon": false
                }

                teamb_last5_results.push(result);

            }

        });
        
        teamb_obj.last_five_result = teamb_last5_results;

        final_response.teamb = teamb_obj


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                items: final_response
            }
        })



    } catch (err) {

        console.log( `${new Date() }/api/get_last_five_match_details -> ${err}`)


    }


}





router.get('/api/get_last_five_match_details', get_last_five_match_details);


module.exports = router;