// 
//  Import Modules 
// 
const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const player_url = require('../../helper/get_player_url');

const { get_fantasy_point, get_total_points } = require('../../helper/player_fantasy_point')


// 
//  Main route function
// 
const get_user_team_for_team_id = async (req, res, next) => {

    // 
    //  Main try catch
    // 
    try {

        const match_id = req.query.match_id;
        const user_team_id = req.query.user_team_id;
        const contest_id = req.query.contest_id;
        const variant_id = req.query.variant_id

        const input = {
            token: req.headers.authorization
        }

        // 
        //  Get phone number by token
        // 
        const phone_number_by_token = await decode_token(input.token, res)

        // console.log(phone_number_by_token)

        // 
        //  Check phone number is exists in db or not
        // 
        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        // 
        //  If phone number is not exists in db then throw error
        // 

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const [get_user_group_deatils] = await knex('public.user_contest_group')
            .where('match_id', match_id)
            .where('c_id', contest_id)
            .where('v_id', variant_id)
            .where('user_team_id', user_team_id)
            .pluck('group_id')

        // console.log(get_user_group_deatils);


        const uniq_team_id = await knex('public.user_team as ut')
            .leftJoin('public.user_contest_group as ucg', 'ucg.user_team_id', 'ut.user_team_id')
            .where('ut.match_id', match_id)
            .where('ucg.c_id', contest_id)
            .where('ucg.v_id', variant_id)
            .where('ut.username', phone_number_by_token)
            .where('ucg.group_id', get_user_group_deatils)
            .distinct('ut.user_team_id')
            .select('ut.user_team_id','ucg.total_points')
            .orderBy('ucg.total_points' ,"desc")
        //  console.log(uniq_team_id.length)



        const teams = []

        for (const user_team_id of uniq_team_id) {

            const element = parseInt(user_team_id.user_team_id)
            const player_details = await knex('public.player as p')
                .leftJoin('public.user_team as ut', 'p.player_id', 'ut.player_id')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                .select(
                    'ut.is_star_player',
                    'utc.trump_card',
                    'ut.player_id',
                    'p.short_name',
                    'p.thumb_url',
                    'p.fantasy_player_rating',
                    'mtv.value_name',
                    'mtv.description',
                    'ut.user_team_id'
                )
                .where('ut.user_team_id', element)
                .where('ut.match_id', match_id)
                //.where('ps.match_id', match_id)
                .where('mtv.type_id', 3)
                .where('ut.username', phone_number_by_token)


           // console.log(player_details.length);


            const team_player_details = []

            var total_points = 0


            for (const player of player_details) {

                const player_id = player.player_id

                const point = await get_fantasy_point(match_id, player_id)


                total_points = await get_total_points(match_id, player.user_team_id)

                const url = await player_url(match_id, player_id)

                if (total_points === 0) {

                    const obj = {
                        "player_id": player.player_id,
                        "player_name": `${player.short_name}`,
                        "thumb_url": url.thumb_url,
                        'logo_url': url.default_url,
                        "is_start_player": player.is_star_player,
                        "fantasy_points": player.is_star_player === 1? parseFloat(2* parseFloat((point.fantasy_points))).toFixed(2) :point.fantasy_points,
                        "popularity": 0
                    }

                    team_player_details.push(obj)

                } else {
                    const count_popularity = Number(((player.is_star_player === 1? parseFloat(2* parseFloat(point.fantasy_points)).toFixed(2) :parseFloat(point.fantasy_points)) / total_points) * 100).toFixed(2)
                    const obj = {
                        "player_id": player.player_id,
                        "player_name": `${player.short_name}`,
                        "thumb_url": url.thumb_url,
                        'logo_url': url.default_url,
                        "is_start_player": player.is_star_player,
                        "fantasy_points": player.is_star_player === 1? parseFloat(2* parseFloat(point.fantasy_points)).toFixed(2) :point.fantasy_points,
                        "popularity": count_popularity === "NaN" ? 0 : count_popularity,
                    }

                    team_player_details.push(obj)
                }

            }


            var trump_card_obj = {}

            if (player_details.length > 0) {
                trump_card_obj = {
                    "trump_card_url": player_details[0].description,
                    "trump_card_id": player_details[0].trump_card,
                    "trump_card_name": player_details[0].value_name
                }



            }

            // 
            //  Find  group by details
            // 
            const user_group_id = await knex('public.user_contest_group ')
                .where('user_team_id', element)
                .where('match_id', match_id)
                .where('c_id', contest_id)
                .where('v_id', variant_id)
                .select('group_id')

           // console.log(user_group_id)

            //
            // Find player current_standing
            //


            var current_standing = 0

            for (const group_id of user_group_id) {

               // console.log(group_id);
                //
                // Select group participant user
                //

                const participating_group_user = await knex('public.user_contest_group ')
                    .where('group_id', group_id.group_id)
                    .select('username',
                        'total_points',
                        'user_team_id')
                    .orderBy('total_points', 'desc')

                // console.log(participating_group_user)
                //
                //  All user position
                //


                for (let index = 0; index < participating_group_user.length; index++) {
                    const user_position = participating_group_user[index];

                    //
                    // If total point > 0 
                    //
                    if (user_position.total_points > 0 && user_position.user_team_id == element) {

                        //
                        // current user position
                        //

                        if (user_position.username == phone_number_by_token) {

                            //
                            // Count current stading
                            //

                            current_standing = index + 1


                        }


                    }


                }

            }

            // 
            //  Prepare team detail
            // 
            const team_details =
            {
                user_team_id: user_team_id.user_team_id,
                team_player_details: team_player_details,
                trump_card: trump_card_obj,
                total_points: total_points,
                current_standing: current_standing
            }

            teams.push(team_details)



        }

        // teams.push(team_detils)


        // 
        //  Prepare final response
        // 
        const final_response = {
            match_id: match_id,
            teams: teams
        }

        // 
        //  Send Response
        // 
        return res.status(200).json({
            meta: {
                status: "OK",
                message: 'Get User Team  ✅️'
            },
            data: {

                "response": final_response
            }
        })


    }
    catch (err) {

        console.log(`${new Date()} /api/get_user_team_for_team_id -> ${err}`)
    }

}
//
// get user team for team_id api
//
router.get('/api/get_user_team_for_team_id', get_user_team_for_team_id);

//export module

module.exports = router;