const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')


const get_variants = async (req, res, next) => {

    const input = {
        token: req.headers.authorization
    }


    const phone_number_by_token = await decode_token(input.token, res)



    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }

    try {

        const response = []

        const get_all_variant = await knex('public.contest_variance')
            .orderBy('v_id', "asc")
            .select('*')




        for (let index = 0; index < get_all_variant.length; index++) {
            const element = get_all_variant[index];


            const variant_obj = {};
            const contest_array = [];

            variant_obj.variant_id = element.v_id;
            variant_obj.variant_name = element.v_name;
            variant_obj.variant_description = element.v_description;
            variant_obj.sample_award_amount = JSON.parse(element.sample_award_amount)

            // console.log(variant_obj)

            const contest_details = await knex('public.contest')
                .where('v_id', element.v_id);

            contest_details.forEach(element => {

                contest_obj = {

                    contest_id: element.c_id,
                    contest_name: element.contest_name,
                    contest_amount: element.contest_amount,
                    contest_prize: element.contest_prize
                }

                contest_array.push(contest_obj)
            });


            variant_obj.contest = contest_array;

            response.push(variant_obj);
        }


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                'response': response

            }



        })




    } catch (err) {

        console.log(`${new Date()} , /api/get_variants -> ${err}`)


    }
}




router.get('/api/get_variants', get_variants);


module.exports = router;