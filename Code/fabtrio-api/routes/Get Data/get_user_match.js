const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const moment = require('moment')
const team_url = require('../../helper/get_team_url');
const get_user_match = async (req, res, next) => {



    const input = {
        token: req.headers.authorization
    }

    const phone_number_by_token = await decode_token(input.token, res)

   
    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }



    const status = req.query.status;

    const final_response = []


    try {

        const get_match_details_by_user = await knex('public.user_contest as uc')
            .leftJoin('public.match as m', 'm.match_id ', 'uc.match_id')
            .distinct('uc.match_id')
            .where('uc.username', phone_number_by_token)
            .where('m.status', status)
            .orderBy('uc.match_id', "asc")
            .select('uc.match_id')


        for (let index = 0; index < get_match_details_by_user.length; index++) {
            const element = get_match_details_by_user[index];


            const all_match = await knex('public.match as m')
                .leftJoin('public.competition as c' , 'c.cid' , 'm.competition_id')
                .where('m.match_id', element.match_id)
                .select('m.*','c.competition_title')


            for (const data of all_match) {


                let teams_data = [];

                const teams_select_query = await knex('public.team as t')
                    .whereIn('team_id', [data.team_one_id, data.team_two_id])
                    .select('*')

                    for (let index = 0; index < teams_select_query.length; index++) {
                        const element = teams_select_query[index];

                        const url = await team_url(element.team_id)

                        teams_data.push({
                            'team_id': element.team_id,
                            'name': element.name,
                            'short_name': element.short_name,
                            'logo_url': url.logo_url
                        }
                        )
                        
                    }


                prepared_data = {
                    match_id: data.match_id,
                    title: data.title,
                    subtitle: data.subtitle,
                    format: data.format,
                    format_str: data.format_str,
                    status: status,
                    status_str: data.status_str,
                    status_note: data.status_note,
                    verified: data.verified,
                    pre_squad: data.pre_squad,
                    game_state: data.game_state,
                    game_state_str: data.game_state_str,
                    domestic: data.domestic,
                    competition_id: data.competition_id,
                    competition_name: data.competition_title,
                    teama: teams_data[0],
                    teamb: teams_data[1],
                    date_start: data.date_start,
                    date_end: data.date_end,
                    timestamp_start: moment(data.timestamp_start,'YYYY-MM-DDTHH:mm:ss Z').valueOf(),
                    timestamp_end: moment(data.timestamp_end,'YYYY-MM-DDTHH:mm:ss Z').valueOf(),


                }

                final_response.push(prepared_data);
            }
        }






        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                items: final_response
            }
        })



    } catch (err) {

        console.log(`${new Date() } , /api/get_user_match -> ${err}`)
    }


}





router.get('/api/get_user_match', get_user_match);


module.exports = router;