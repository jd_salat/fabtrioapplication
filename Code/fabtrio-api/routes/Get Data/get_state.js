const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');



const get_state = async (req, res, next) => {


    try {
        
    
    const all_state = await knex("public.state")
        .select('*')

    return res.status(200).json({
        meta: {
            status: 'OK',
            message: ` All state`
        },
        data: {

            all_state

        }

    });
}
catch (err) {
        
    console.log(`${new Date() } , /api/get_state -> ${err}`)


}

}

router.get('/api/get_state', get_state);

module.exports = router;