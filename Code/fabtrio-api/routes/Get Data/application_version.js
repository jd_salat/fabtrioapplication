const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const application_version = async (req, res, next) => {


    try {

        const app_version = await knex('public.application_version')
            .select('*')

       // console.log(app_version)

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: `Success`
            },
            data: {

                app_version

            }

        });

    } catch (err) {
        
        console.log(`${new Date() },/api/application_version -> ${err}`)
      

}



}

router.get('/api/application_version', application_version);

module.exports = router;