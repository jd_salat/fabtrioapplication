const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const getUserRank = require('../../helper/get_rank')


const get_user_rank = async (req, res, next) => {

    try {


        const input = {
            token: req.headers.authorization
        }



        const phone_number_by_token = await decode_token(input.token, res)


        //  console.log(phone_number_by_token)
        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }
        const match_id = req.query.match_id;
        const contest_id = req.query.contest_id;
        const variant_id = req.query.variant_id;
        const user_team_id = req.query.user_team_id;



        const get_user_group_deatils = await knex('public.user_contest_group')
            .where('match_id', match_id)
            .where('c_id', contest_id)
            .where('v_id', variant_id)
            .where('user_team_id', user_team_id)
            .select('group_id')

        // console.log(get_user_group_deatils)

        var rank = 0

        for (const group_id of get_user_group_deatils) {
            //
            // Select group participant user
            //

            const participating_group_user = await knex('public.user_contest_group ')
                .where('group_id', group_id.group_id)
                .select('username',
                    'total_points',
                    'user_team_id')
                .orderBy('total_points', 'desc')

            //
            // Cureent postion user
            //
            //    console.log(participating_group_user)

            const index = participating_group_user.findIndex(function (item) { return item.username == phone_number_by_token })



            rank = await getUserRank(participating_group_user, phone_number_by_token, participating_group_user[index].total_points, participating_group_user[index].user_team_id)


            //  console.log('users_rank' , rank)

            // for (let index = 0; index < participating_group_user.length; index++) {
            //     const element = participating_group_user[index]; 
            //     //
            //     //  Find rank evey user in group if total point > 0
            //     //

            //     if (element.total_points > 0) {


            //         if (element.username === phone_number_by_token) {

            //         //
            //         // Count rank 
            //         //

            //             rank = index + 1



            //         }


            //     }



            // }

        }


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: `Success`
            },
            data: {

                'user_rank': rank


            }

        });


    } catch (err) {

        console.log(`${new Date()} /api/get_user_rank -> ${rank}`)


    }
}

router.get('/api/get_user_rank', get_user_rank);

module.exports = router;