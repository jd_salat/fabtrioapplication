const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')


const get_trump_card_values = async (req, res, next) => {
   



    const input = {
        token: req.headers.authorization
    }

    const phone_number_by_token = await decode_token(input.token, res)

   
    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }


    try {

        const trump_card_values = await knex('public.m_type_values')
        .where('type_id' , 3)
        .select('value_id',
        'value_name',
        'description as url')
        
        const final_response = trump_card_values


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                items: final_response
            }
        })



    } catch (err) {

        console.log(`${new Date() } /api/get_trump_card_values -> ${err}`)


    }


}


router.get('/api/get_trump_card_values', get_trump_card_values);


module.exports = router;