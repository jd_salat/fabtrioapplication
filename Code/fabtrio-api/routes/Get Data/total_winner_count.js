const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')



const total_winner_count = async (req, res, next) => {

    try {
        const input = {

            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)

        const check_number_exists_or_not = await knex('public.registration')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });

        }

        const total_winner_count = await knex('public.winners')
            .select('winner_id')

        // console.log(total_winner_count);


        const total_winner_id_count = total_winner_count[0] + 1;

        //console.log(total_winner_id_count)


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            data: {

                total_winner_id_count: total_winner_id_count

            }
        })


    } catch (err) {

        console.log(`${new Date()} , /api/total_winner_count -> ${err}`)

    }

}


router.get('/api/total_winner_count', total_winner_count);

module.exports = router;