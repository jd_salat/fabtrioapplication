const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token } = require('../auth/token')

const get_participant_count = async (req, res, next) => {

    try {

        const input = {
            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)



        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const match_id = req.query.match_id

        const final_response = []
        const get_all_details = await knex('public.contest_variance as cv')
            .leftJoin('public.user_contest as uci', 'uci.v_id', 'cv.v_id')
            .select(
                'cv.v_id',
                'cv.v_name'
            )
            .where('uci.match_id', match_id)
            .groupBy('cv.v_id', 'cv.v_name')
            .count('uci.username')

        

        if (get_all_details.length === 0) {
            const random_number1 =Math.floor(Math.random() * 90 + 10)
            const obj1 = {
                "v_id": 1,
                "participants": random_number1,
                "v_name": 'SUPER 7'
            }
            const random_number2 = Math.floor(Math.random() * 990 + 100)
            const obj2 = {
                "v_id": 2,
                "participants": random_number2,
                "v_name": 'HIGH 5'
            }
            const random_number3 = Math.floor(Math.random() * 9990 + 1000)
            const obj3 = {
                "v_id": 3,
                "participants": random_number3,
                "v_name": 'FABULOUS 3'
            }
            
            final_response.push(obj1);
            final_response.push(obj2);
            final_response.push(obj3);


        }


        for (let index = 0; index < get_all_details.length; index++) {
            const element = get_all_details[index];

            const final_count = parseInt(element.count)

            var count = 0;

            if (final_count <= 10) {
                count = final_count * 100
            }
            else if (final_count > 10 && final_count <= 100) {
                count = final_count * 50
            }
            else if (final_count > 100 && final_count <= 1000) {
                count = final_count * 10
            } else {
                count = final_count
            }



            const obj = {
                "v_id": element.v_id,
                "participants": count,
                "v_name": element.v_name
            }

            final_response.push(obj)
           // console.log(final_response)

        }

        return res.status(200).json({
            meta: {
                status: "OK",
                message: 'Get User Team  ✅️'
            },
            data: {


                "response": final_response


            }



        })


    } catch (err) {

        console.log(`${new Date() } /api/get_participant_count -> ${err}`)

    }



}


router.get('/api/get_participant_count', get_participant_count);

module.exports = router;