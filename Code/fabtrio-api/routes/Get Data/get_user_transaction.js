const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const moment = require('moment')

const get_user_transaction = async (req, res, next) => {

    try {


        const input = {
            token: req.headers.authorization
        }



        const phone_number_by_token = await decode_token(input.token, res)



        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }



        const transaction_detalis_by_user = await knex('public.transaction as t')
            .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 't.transaction_type')
           // .leftJoin('public.m_type_values as mt', 'mt.value_id', 't.transaction_message')
            .where('mtv.type_id', 5)
           // .where('mt.type_id', 8)
            .orderBy('t.transaction_date', 'desc')
            .where('username', phone_number_by_token)
            .select(
                't.transaction_id',
                't.match_id',
                't.transaction_date',
                'mtv.description as transaction_type_url',
                't.transaction_message',
                't.transaction_amount',
                't.transaction_type',
                't.transaction_status'
            )
            .limit(20)


        for (let index = 0; index < transaction_detalis_by_user.length; index++) {
            const element = transaction_detalis_by_user[index];



            const team_ids = await knex('public.match')
                .where('match_id', element.match_id)
                .select('team_one_id',
                    'team_two_id')



            const teams_select_query = await knex('public.team as t')
                .whereIn('team_id', [team_ids[0].team_one_id, team_ids[0].team_two_id])
                .select('*')


            const short_name = teams_select_query[0].short_name + ' VS ' + teams_select_query[1].short_name


            transaction_detalis_by_user[index].short_name = short_name

            transaction_detalis_by_user[index].transaction_date = moment(transaction_detalis_by_user[index].transaction_date).format('YYYY-MM-DD')


        }



        return res.status(200).json({
            meta: {
                status: 'OK',
                message: `Success`
            },
            data: {

                transaction_detalis: transaction_detalis_by_user

            }

        });


    } catch (err) {

        console.log(`${new Date()}/api/get_user_transaction -> ${err}`)


    }
}

router.get('/api/get_user_transaction', get_user_transaction);

module.exports = router;