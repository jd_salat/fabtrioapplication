const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')


const get_user_account_details = async (req, res, next) => {


    try {

        const input = {
            token: req.headers.authorization
        }

        const phone_number_by_token = await decode_token(input.token, res)

        console.log(phone_number_by_token)

        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {


            return res.status(500).json({
                meta: {
                    status: '0',
                    message: 'Seems like security break.  ✅️'
                },
                data: {
                }
            })
        }

        const response = {};

        const [user_wallet_details] = await knex('public.user_wallet')
            .where('username', phone_number_by_token)
            .select(
                'bonus_amount',
                'deposited_amount',
                'winning_amount'
            )

        if (user_wallet_details) {
            response.user_wallet = user_wallet_details;

        }



        const [user_kyc_details] = await knex('public.kyc as k')
            .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'k.doc_type')
            .where('mtv.type_id', 7)
            .where('k.username', phone_number_by_token)
            .select(
                'k.doc_link',
                'mtv.value_name as doc_type',
                'k.is_verified',
                'k.doc_number',
                'k.doc_type'
            )

        if (user_kyc_details) {
            const user_kyc = {
                doc_type: user_kyc_details.doc_type,
                doc_number: user_kyc_details.doc_number,
                doc_link: user_kyc_details.doc_link,
                is_verified: (user_kyc_details.is_verified === 1) ? 'Yes' : 'No'
            }

            response.user_kyc = user_kyc;
        }


        const [user_bank_details] = await knex('public.user_bank as ub')
            .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'ub.is_verified')
            .where('mtv.type_id', 4)
            .where('ub.username', phone_number_by_token)
            .select(
                'ub.bank_name',
                'ub.account_number',
                'ub.ifsc_code',
                'ub.branch_name',
                'ub.doc_link',
                'mtv.value_name as is_verified',
                'ub.verified_by'
            )
     //   console.log(user_bank_details)

        if (user_bank_details) {

            response.user_bank = user_bank_details;
        }


        if (!user_kyc_details && !user_bank_details && !user_wallet_details) {

            return res.status(412).json({
                meta: {
                    status: '0',
                    message: 'User account details not found for this user'
                },
                data: {
                    
                    "bonus_amount" : 0,
                    "deposited_amount": 0,
                    "winning_amount": 0
                }
            })

        }


        if (!user_kyc_details && !user_bank_details && user_wallet_details) {

            return res.status(413).json({
                meta: {
                    status: 'OK',
                    message: 'Get User Account Details ✅️'
                },
                data: {

                    "response": response

                }
            })

        }
        if (user_kyc_details && !user_bank_details && user_wallet_details) {

            return res.status(414).json({
                meta: {
                    status: 'OK',
                    message: 'Get User Account Details ✅️'
                },
                data: {

                    "response": response

                }
            })

        }


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get User Account Details ✅️'
            },
            data: {

                "response": response

            }
        })

    } catch (err) {

        console.log(`${new Date() } /api/get_user_account_details -> ${err}`)

    }
}


// registration  api
router.get('/api/get_user_account_details', get_user_account_details);

module.exports = router;