const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const player_url = require('../../helper/get_player_url');
const getUserRank = require('../../helper/get_rank')


const { get_fantasy_point, get_total_points } = require('../../helper/player_fantasy_point');


const get_user_group_deatils = async (req, res, next) => {



    try {

        const input = {
            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)



        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

      //  console.log(check_number_exists_or_not)

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const match_id = req.query.match_id;
        const contest_id = req.query.contest_id;
        const variant_id = req.query.variant_id;
        const user_team_id = req.query.user_team_id;

        final_response = []



        const get_user_group_deatils = await knex('public.user_contest_group')
            .where('match_id', match_id)
            .where('c_id', contest_id)
            .where('v_id', variant_id)
            .where('user_team_id', user_team_id)
            .select('group_id')


        console.log('group_id' , get_user_group_deatils)

        const participant_group = await knex('public.user_contest_group as ucg')
            .leftJoin('public.registration as r', 'r.phone_number', 'ucg.username')
            .where('ucg.group_id', get_user_group_deatils[0].group_id)
            .select('ucg.username',
                'ucg.total_points',
                'ucg.user_team_id',
                'r.first_name',
                'r.last_name'
            )
            .orderBy('ucg.total_points', 'desc')
            

           
           console.log('participant_group' , participant_group.length)

            let rank = 0

        for (const participant of participant_group) {

           // console.log(participant.to)

            var group_participants_obj = {
                'participant_name': `${participant.first_name} ${participant.last_name}`

            }
            rank = await getUserRank(participant_group , participant.username, participant.total_points ,participant.user_team_id)

             console.log(`username-> ${participant.username} ******name ->${participant.first_name} ******** Rank -> ${rank} ****** total_points -> ${participant.total_points}`)

            // console.log('group_participant_obj' , group_participants_obj)

            const get_participant_team = await knex('public.user_team as ut')
                .leftJoin('public.player as p', 'p.player_id', 'ut.player_id')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                .select(
                    'ut.is_star_player',
                    'ut.player_id',
                    'p.thumb_url',
                    'p.short_name',
                    'mtv.value_name',
                    'mtv.description',
                    'utc.trump_card',
                    'ut.user_team_id'
                )
                .where('ut.user_team_id', participant.user_team_id)
                .where('mtv.type_id', 3)
               // .where('ut.match_id', match_id)

               //console.log( 'get_participant_team',get_participant_team.length)


            if (get_participant_team.length !== 0) {

                var total_points = 0

                let participant_team = [];

                for (const player of get_participant_team) {


                    const player_id = player.player_id

                    const point = await get_fantasy_point(match_id, player_id)
                  //  console.log('point ' , point)


                    total_points = await get_total_points(match_id, player.user_team_id)



                 //   console.log('total_points', total_points)

                    const url = await player_url(match_id, player_id)

                    if (total_points === 0) {

                        const obj = {

                            "player_id": player.player_id,
                            "player_name": `${player.short_name}`,
                            "is_start_player": player.is_star_player,
                            "thumb_url": url.thumb_url,
                            "logo_url": url.default_url,
                            "popularity": 0


                        }

                        participant_team.push(obj);



                    } else {


                        const obj = {

            

                            "player_id": player.player_id,
                            "player_name": `${player.short_name}`,
                            "is_start_player": player.is_star_player,
                            "thumb_url": url.thumb_url,
                            "logo_url": url.default_url,
                            "popularity": Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2) === "NaN" ? 0 : Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2),


                        }

                      // console.log('else obj')

                        participant_team.push(obj);

                    }

                }


                group_participants_obj.trump_card_url = get_participant_team[0].description,
                group_participants_obj.total_points = total_points
                group_participants_obj.rank =rank



                group_participants_obj.participant_team = participant_team


               
                final_response.push(group_participants_obj)
             //  console.log( 'final_response', final_response)

            }
        }

                               // console.log(final_response)


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Get Data ✅️'
            },
            data: {

                response: final_response
            }
        })



    }
    catch (err) {

        console.log(`${new Date()} /api/get_user_group_deatils -> ${err}`)

    }

}

router.get('/api/get_user_group_deatils', get_user_group_deatils);

module.exports = router;