const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const player_url = require('../../helper/get_player_url');

const { get_fantasy_point, get_total_points } = require('../../helper/player_fantasy_point')

const get_user_team = async (req, res, next) => {

    try {


        const match_id = req.query.match_id;

        const input = {
            token: req.headers.authorization
        }


        // console.log(input);

        const phone_number_by_token = await decode_token(input.token, res)


        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }


        const uniq_team_id = await knex('public.user_team')
            .where('match_id', match_id)
            .where('username', phone_number_by_token)
            .distinct('user_team_id')
            .select('user_team_id')

       // console.log(uniq_team_id)

        const teams = []


        for (const user_team_id of uniq_team_id) {



            const element = parseInt(user_team_id.user_team_id)
            const player_details = await knex('public.player as p')
                .leftJoin('public.user_team as ut', 'p.player_id', 'ut.player_id')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                .select(
                    'ut.is_star_player',
                    'utc.trump_card',
                    'ut.player_id',
                    'p.short_name',
                    'p.thumb_url',
                    'p.fantasy_player_rating',
                    'mtv.value_name',
                    'mtv.description',
                    'ut.user_team_id'
                )
                .where('ut.user_team_id', element)
                .where('ut.match_id', match_id)
                //.where('ps.match_id', match_id)
                .where('mtv.type_id', 3)
                .where('ut.username', phone_number_by_token)


            //console.log(player_details);





            const team_player_details = []

            var total_points = 0


            for (const player of player_details) {

                const player_id = player.player_id

                const point = await get_fantasy_point(match_id, player_id)


                total_points = await get_total_points(match_id, player.user_team_id)

                const url = await player_url(match_id, player_id)

                if(total_points === 0 ){

                    const obj = {
                        "player_id": player.player_id,
                        "player_name": `${player.short_name}`,
                        "thumb_url": url.thumb_url,
                        'logo_url':url.default_url,
                        "is_start_player": player.is_star_player,
                        "fantasy_points": point.fantasy_points,
                        "popularity": 0
                    }
        
                    team_player_details.push(obj)
    
                }else{
                const obj = {
                    "player_id": player.player_id,
                    "player_name": `${player.short_name}`,
                    "thumb_url": url.thumb_url,
                    'logo_url':url.default_url,
                    "is_start_player": player.is_star_player,
                    "fantasy_points": point.fantasy_points,
                    "popularity": Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2) === "NaN" ? 0 : Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2),
                }
    
                team_player_details.push(obj)
            }

            }


            var trump_card_obj = {}

            if (player_details.length > 0) {
                trump_card_obj = {
                    "trump_card_url": player_details[0].description,
                    "trump_card_id": player_details[0].trump_card,
                    "trump_card_name": player_details[0].value_name
                }



            }

            // const current_standing =await knex('public.user_contest_group ')
            //     .where('user_team_id' , element)
            //     .where('match_id' ,match_id)
            //     .select('group_id')

            //     console.log(current_standing)




            const team_detils =
            {
                user_team_id: element,
                team_player_details: team_player_details,
                trump_card: trump_card_obj,
                total_points: total_points
            }

            teams.push(team_detils)


        }

        const final_response = {
            match_id: match_id,
            teams: teams
        }






        return res.status(200).json({
            meta: {
                status: "OK",
                message: 'Get User Team  ✅️'
            },
            data: {

                "response": final_response
            }
        })


    }
    catch (err) {

        console.log(`${new Date()} , /api/get_user_team -> ${err}`)

       
    }

}

router.get('/api/get_user_team', get_user_team);

module.exports = router;