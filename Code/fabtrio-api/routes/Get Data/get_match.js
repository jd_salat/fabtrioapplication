const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const moment = require('moment')
const team_url = require('../../helper/get_team_url')



async function get_match_data(match_data, req) {



    const status = req.query.status;

    const match_details = [];
    var prepared_data = {};



    for (const data of match_data) {


        let teams_data = [];
        const teams_select_query = await knex('public.team')
            .whereIn('team_id', [data.team_one_id, data.team_two_id])
            .select('*')


        for (let index = 0; index < teams_select_query.length; index++) {
            const element = teams_select_query[index];

       
            const url = await team_url(element.team_id)

            teams_data.push({
                'team_id': element.team_id,
                'name': element.name,
                'short_name': element.short_name,
                'logo_url': url.logo_url
            })

        }


        const get_team_squad = await knex('public.team_squad')
            .select('match_id')
            .where('match_id', data.match_id)

        // console.log(get_team_squad.length)

        prepared_data = {
            is_team_squad_available: get_team_squad.length > 0 ? true : false,
            match_id: data.match_id,
            title: data.title,
            subtitle: data.subtitle,
            format: data.format,
            format_str: data.format_str,
            status: status,
            status_str: data.status_str,
            status_note: data.status_note,
            verified: data.verified,
            pre_squad: data.pre_squad,
            game_state: data.game_state,
            game_state_str: data.game_state_str,
            domestic: data.domestic,
            competition_id: data.competition_id,
            competition_name: data.competition_title,
            teama: teams_data[0],
            teamb: teams_data[1],
            date_start: moment(data.date_start).format('YYYY-MM-DD'),
            date_end: moment(data.date_end).format('YYYY-MM-DD'),
            timestamp_start: moment(data.timestamp_start, 'YYYY-MM-DDTHH:mm:ss Z').valueOf(),
            timestamp_end: moment(data.timestamp_end, 'YYYY-MM-DDTHH:mm:ss Z').valueOf(),
        }
        //console.log(moment(data.timestamp_start,'YYYY-MM-DDTHH:mm:ss Z').valueOf())
        ////console.log('convert',prepared_data.timestamp_start)

        match_details.push(prepared_data);



    }

    return match_details
}



const get_match = async (req, res, next) => {
    const input = {
        token: req.headers.authorization
    }


    const phone_number_by_token = await decode_token(input.token, res)

    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }

    const tournament_id = req.query.tournament_id;

    const status = req.query.status;

    const per_page = req.query.per_page;

    const page_number = req.query.page_number;



    try {


        let match_data = [];


        const { offset, limit } = await get_offset_and_limit(per_page, page_number)



        if (tournament_id === undefined) {

            const all_match_data = await knex('public.match as m')
                .leftJoin('public.competition as c', 'c.cid', 'm.competition_id')
                .where('m.status', status)
                .limit(limit)
                .offset(offset)
                .orderBy('timestamp_start', "asc")
                .select('*')
                .where('format', '!=' ,17)

            match_data = all_match_data;

        } else {

            const all_match_data = await knex.select('*').from('public.match as m')
                .leftJoin('public.competition as c', 'c.cid', 'm.competition_id')
                .where('m.status', status)
                .limit(limit)
                .offset(offset)
                .orderBy('timestamp_start', "asc")
                .where('cid', tournament_id)
                .where('format', '!=' ,17)


            match_data = all_match_data;

        }

        const final_response = await get_match_data(match_data, req)


        const [{ count }] = await knex('public.match')
            .count('*')


        // final_response.map(item => console.log(`match_id-> ${item.match_id}  timestamp->${item.timestamp_start} teama->${item.teams_data.short_name} teamb->${item.teamb.short_name} page -.${page_number}`))

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully data fetched✅️'
            },
            data: {
                'items': final_response,
                "total_items": count,
                "total_pages": Math.ceil(count / per_page),
            }
        })



    } catch (err) {

        console.log( `${new Date()} /api/get_match -> ${err}`)

        


    }


}


const get_offset_and_limit = async (per_page = 10, page_number = 1) => {

    let limit = +per_page || 10;

    let page = +page_number || 1;

    if (limit < 1) {

        limit = 1;
    }

    if (page < 1) {

        page = 1;
    }

    const offset = (
        page - 1
    ) * limit;

    return {
        offset,
        limit,
    }
}



router.get('/api/get_match', get_match);


module.exports = router;