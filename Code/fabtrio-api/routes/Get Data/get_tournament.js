const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const team_url = require('../../helper/get_team_url')


const get_tournament = async (req, res, next) => {
    const input = {
        token: req.headers.authorization
    }

    //console.log(input);

    const phone_number_by_token = await decode_token(input.token, res)

    // console.log(input.token)

    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')

    if (check_number_exists_or_not.length === 0) {

        return res.status(404).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });


    }
    try {
      
           
            const tournamentData = await knex('public.competition as c')
            .innerJoin('public.match as m', 'm.competition_id', 'c.cid')
            .innerJoin('public.match_stat as ms', 'ms.match_id', 'm.match_id')
            .select('c.*', 'ms.*')

            const tournamentDetail = tournamentData.map((data) => {
                const data1 = data;
            
                return {
                    tournament_id: data1.cid,
                    title: data1.title,
                    category: data1.category,
                    total_matches: data1.total_matches,
                    total_rounds: data1.total_rounds,
                    total_teams: data1.total_teams,
                    match_format: data1.game_format,
                    status: data1.status,
                    country: data1.country,
                    type: data1.squad_type,
                    datestart: data1.datestart,
                    dateend: data1.dateend,
                    latest_match : {
                        'match_id' : data1.match_id,
                        'teama_id': data1.teama_id,
                        'teamb_id' : data1.teamb_id,
                        'teama_thumb_url': data1.teama_thumb_url,
                        'teamb_thumb_url' : data1.teamb_thumb_url,
                        'match_start_time' : data1.datestart
                    }
                }
               
            })
           
            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'successfully data fetched✅️'
                },
                data: {
                    tournamentDetail
                }
            })
            // console.log('tournamentData', getTournament);
        
    } catch (err) {
        
        console.log(`${new Date() } , /api/get_tournament -> ${err}`)

}

}

router.get('/api/get_tournament', get_tournament);

module.exports = router;