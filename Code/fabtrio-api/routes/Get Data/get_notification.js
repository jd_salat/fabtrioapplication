const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment')
const { decode_token } = require('../auth/token')




const get_notification = async (req, res, next) => {

    try {



        const input = {

            token: req.headers.authorization
        }


        const phone_number_by_token = await decode_token(input.token, res)

        const check_number_exists_or_not = await knex('public.registration')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });

        }

       

        const get_notification_details = await knex('public.notification as n')
            .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'n.notification_type')
            .where('n.username', phone_number_by_token)
            .where('mtv.type_id', 13)
            .orderBy('n.notification_date', "desc")
            .select(
                'n.username',
                'n.notification_message',
                'mtv.value_name as notification_type',
                'n.notification_date',
                'n.notification_image_url'
            )
            .limit(50)

        //console.log('get_notification' , get_notification_details)

        for (let index = 0; index < get_notification_details.length; index++) {
            const element = get_notification_details[index];

            element.notification_date = moment(element.notification_date).format('YYYY-MM-DD')
        }

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            data: {

                get_notification_details

            }
        })


    } catch (err) {

        console.log(`${new Date() } /api/get_notification -> ${err}`)
    }

}


router.get('/api/get_notification', get_notification);

module.exports = router;