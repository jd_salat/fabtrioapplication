// 
//  Import Modules 
// 
const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { decode_token } = require('../auth/token')
const moment = require('moment')
const player_url = require('../../helper/get_player_url');
const getUserRank = require('../../helper/get_rank')
const team_url = require('../../helper/get_team_url')
const { get_fantasy_point, get_total_points } = require('../../helper/player_fantasy_point')

// 
//  Main route function
// 

const get_user_contest_status_wise = async (req, res, next) => {

    console.log(new Date(), 'get_user_contest_status_wise')

    // 
    //  Main try catch
    //

    try {
        //
        // Get match_id
        //

        const status = req.query.status;
        //  console.log(status)


        const input = {
            token: req.headers.authorization
        }

        // 
        //  Get phone number by token
        //
        const phone_number_by_token = await decode_token(input.token, res);

        // 
        //  Check phone number is exists in db or not
        // 

        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        // 
        //  If phone number is not exists in db then throw error
        // 

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const final_response = [];

        //
        // Get all match
        //

        const get_match_details_by_user = await knex('public.user_contest as uc')
            .leftJoin('public.match as m', 'm.match_id ', 'uc.match_id')
            .leftJoin('public.competition as c', 'c.cid', 'm.competition_id')
            .distinct('uc.match_id')
            .where('uc.username', phone_number_by_token)
            .where('m.status', status)
            .orderBy('m.date_start', status==="2"?"desc":"asc")
            .select(
                'm.*',
                'c.competition_title'
            )


         //console.log('get_match_details_by_user' ,get_match_details_by_user)
        for (let index = 0; index < get_match_details_by_user.length; index++) {
            const match = get_match_details_by_user[index];

            const match_obj = {
                'match_id': match.match_id,
                'title': match.title,
                'subtitle': match.subtitle,
                'format': match.format,
                'format_str': match.format_str,
                'status': match.status,
                'status_str': match.status_str,
                'status_note': match.status_note,
                'verified': match.verified,
                'pre_squad': match.pre_squad,
                'game_state': match.game_state,
                'game_state_str': match.game_state_str,
                'competition_id': match.competition_id,
                'competition_name': match.competition_title,
                'timestamp_start': moment(match.timestamp_start,'YYYY-MM-DDTHH:mm:ss Z').valueOf()
            }

            //   console.log(match_obj)

            if (match.status === 2) {

                //  console.log(match.status)


                if (index < 10) {

                    const [winning_team_id] = await knex('public.match_stat')
                    .where('match_id' , match.match_id)
                    .select('winning_team_id')

                  //  console.log('winning_team_id' , winning_team_id)

                    const [participants_amount] = await knex('public.user_contest')
                    .where('match_id' , match.match_id)
                    .where('username' ,phone_number_by_token)
                    .sum('amount_paid as participation_amount')

                    // console.log(get_participants_amount)

                    const [winning_amount] = await knex('public.user_contest_group')
                    .where('match_id',match.match_id)
                    .where('username' , phone_number_by_token)
                    .sum('winning_amount as winning_amount')

                    

                    

                    //
                    // Get every match teams
                    //
                    const team_a_details = await get_team_details(match.team_one_id);
                    const teama_url = await team_url(match.team_one_id)

                    //
                    // prepared  teama data
                    //
                    const team_a_obj = {
                        'team_id': match.team_one_id,
                        'name': team_a_details[0].name,
                        'short_name': team_a_details[0].short_name,
                        'logo_url': teama_url.logo_url,
                    }

                    const team_b_details = await get_team_details(match.team_two_id);
                    const teamb_url = await team_url(match.team_two_id)
                    //
                    // prepared teamb data
                    //
                    const team_b_obj = {
                        'team_id': match.team_two_id,
                        'name': team_b_details[0].name,
                        'short_name': team_b_details[0].short_name,
                        'logo_url': teamb_url.logo_url,
                    }

                    match_obj.teama = team_a_obj;
                    match_obj.teamb = team_b_obj;
                    match_obj.winning_team_id = winning_team_id.winning_team_id;
                    match_obj.participants_amount = participants_amount.participation_amount === null ?0: participants_amount.participation_amount
                    match_obj.winning_amount  = winning_amount.winning_amount === null ? 0:winning_amount.winning_amount

                }

            }

            if (match.status === 1) {

                if (index === 0) {

                    let user_contest = []

                    //
                    //  Get user contest details by match and username
                    //
                    const get_user_contest_details = await knex('public.user_contest as uc')
                        .leftJoin('public.contest_variance as cv', 'cv.v_id', 'uc.v_id')
                        .orderBy('v_id', "asc")
                        .leftJoin('public.contest as c', 'c.c_id', 'uc.c_id')
                        .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'uc.user_team_id')
                        .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                        .leftJoin('public.m_type_values as mtv1', 'mtv1.value_id', 'uc.payment_status')
                        .where('uc.match_id', match.match_id)
                        .where('uc.username', phone_number_by_token)
                        .where('mtv.type_id', 3)
                        .where('mtv1.type_id', 6)
                        .distinct('uc.c_id', 'c.contest_name', 'c.contest_amount', 'uc.user_team_id', 'mtv.description')
                        .orderBy('uc.c_id')
                        .select(
                            'cv.v_id',
                            'cv.v_name',
                            'uc.c_id',
                            'c.contest_name',
                            'c.contest_amount',
                            'uc.user_team_id',
                            'mtv.description',
                            'mtv.value_name as trump_card_value',
                            'utc.trump_card',
                            'mtv1.value_name as payment_status',
                            'uc.user_contest_id'
                        );

                    //
                    // every contest details
                    //
                    for (let index = 0; index < get_user_contest_details.length; index++) {
                        const element = get_user_contest_details[index];

                        //
                        // Select group by detils
                        //
                        const user_group_id = await knex('public.user_contest_group ')
                            .where('user_team_id', element.user_team_id)
                            .where('match_id', match.match_id)
                            .where('c_id', element.c_id)
                            .where('v_id', element.v_id)
                            .select('group_id')


                        for (const group_id of user_group_id) {
                            //
                            // Get rank
                            //

                            var rank = 0

                            //
                            // Select group participant user
                            //

                            const participating_group_user = await knex('public.user_contest_group ')
                                .where('group_id', group_id.group_id)
                                .select('username',
                                    'total_points',
                                    'user_team_id')
                                .orderBy('total_points', 'desc')
                            //
                            // Cureent postion user
                            //

                            const index = participating_group_user.findIndex(function(item){ return item.username == phone_number_by_token})
                            rank = await getUserRank(participating_group_user , phone_number_by_token , participating_group_user[index].total_points , participating_group_user[index].user_team_id)
                        }

                        //
                        // Prepare variant data
                        //

                        const obj = {
                            "variant_id": element.v_id,
                            "variant_name": element.v_name,
                        }


                        const participating_contest = []

                        //
                        // Prepare contest data
                        //

                        const participating_contest_obj = {
                            "user_contest_id": element.user_contest_id,
                            "contest_id": element.c_id,
                            "contest_name": element.contest_name,
                            "contest_amount": element.contest_amount,
                            "payment_status": element.payment_status,
                            "rank": rank
                        }

                        //
                        // Prepare team data
                        //
                        const team = {
                            "user_team_id": element.user_team_id,
                            "trump_card": {
                                "trump_card_url": element.description,
                                "trump_card_id": element.trump_card,
                                "trump_card_name": element.trump_card_value
                            }
                        }

                        const players = []

                        //
                        // Get every team player details
                        //

                       
                const get_user_team = await knex('public.user_team as ut')
                .leftJoin('public.player as p', 'p.player_id', 'ut.player_id')
                .select('ut.is_star_player',
                    'p.thumb_url',
                    'p.player_id',
                    'p.short_name',
                    'ut.user_team_id'
                )
                .where('ut.user_team_id', element.user_team_id)

                        const match_id = match.match_id
                        var total_points = 0
                        //
                        // Every palyers count total poins
                        //

                        for (const element of get_user_team) {

                            const player_id = element.player_id
        
        
                            const point = await get_fantasy_point(match_id , player_id)
        
                            
        
        
                          total_points =  await get_total_points(match_id ,element.user_team_id)
                            
                                
                           // total_points = total_points + parseFloat(point.fantasy_points)
        
                            //    console.log('total_points ' , total_points)
                            
        
        
                            //
                            // prepare player data
                            //
        
                          
                            
                            
        
                            const url = await player_url(match_id, player_id)
        
                           
        
                            // const fantasy_points = point
        
                            //console.log(point)
        
                            if (total_points === 0) {
        
        
                                const player_obj = {
        
                                    "player_id": element.player_id,
                                    "player_name": `${element.short_name}`,
                                    "is_start_player": element.is_star_player,
                                    "thumb_url": url.thumb_url,
                                    "logo_url": url.default_url,
                                    "popularity": 0
        
        
                                }
        
                                players.push(player_obj);
        
                            } else {
        
        
                                const player_obj = {
        
        
                                    "player_id": element.player_id,
                                    "player_name": `${element.short_name}`,
                                    "is_start_player": element.is_star_player,
                                    "thumb_url": url.thumb_url,
                                    "logo_url": url.default_url,
                                    "popularity": Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2) === "NaN" ? 0 : Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2),
        
        
                                }
                                players.push(player_obj);
        
                            }
        
                        }

                        team.players = players;


                        participating_contest_obj.team = team;


                        participating_contest.push(participating_contest_obj)

                        obj.participating_contest = participating_contest;


                        user_contest.push(obj);


                    }

                    match_obj.user_contest = user_contest;


                    //
                    // Get every match teams
                    //

                    const team_a_details = await get_team_details(match.team_one_id);
                    const teama_url = await team_url(match.team_one_id)

                    //
                    // prepared  teama data
                    //

                    const team_a_obj = {
                        'team_id': match.team_one_id,
                        'name': team_a_details[0].name,
                        'short_name': team_a_details[0].short_name,
                        'thumb_url': teama_url.logo_url,
                    }

                    const team_b_details = await get_team_details(match.team_two_id);
                    const teamb_url = await team_url(match.team_two_id)
                    //
                    // prepared teamb data
                    //

                    const team_b_obj = {
                        'team_id': match.team_two_id,
                        'name': team_b_details[0].name,
                        'short_name': team_b_details[0].short_name,
                        'thumb_url': teamb_url.logo_url,
                    }

                    match_obj.teama = team_a_obj;
                    match_obj.teamb = team_b_obj;

                } else {
                    //
                    // Get every match teams
                    //

                    const team_a_details = await get_team_details(match.team_one_id);
                    const teama_url = await team_url(match.team_one_id)
                    //
                    // prepared  teama data
                    //

                    const team_a_obj = {
                        'team_id': match.team_one_id,
                        'name': team_a_details[0].name,
                        'short_name': team_a_details[0].short_name,
                        'thumb_url': teama_url.logo_url,
                    }

                    const team_b_details = await get_team_details(match.team_two_id);
                    const teamb_url = await team_url(match.team_two_id)
                    //
                    // prepared teamb data
                    //

                    const team_b_obj = {
                        'team_id': match.team_two_id,
                        'name': team_b_details[0].name,
                        'short_name': team_b_details[0].short_name,
                        'thumb_url': teamb_url.logo_url,
                    }

                    match_obj.teama = team_a_obj;
                    match_obj.teamb = team_b_obj;

                }

            }

            if (match.status === 3) {

                let user_contest = []

                //
                //  Get user contest details by match and username
                //
                const get_user_contest_details = await knex('public.user_contest as uc')
                    .leftJoin('public.contest_variance as cv', 'cv.v_id', 'uc.v_id')
                    .orderBy('v_id', "asc")
                    .leftJoin('public.contest as c', 'c.c_id', 'uc.c_id')
                    .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'uc.user_team_id')
                    .leftJoin('public.m_type_values as mtv', 'mtv.value_id', 'utc.trump_card')
                    .leftJoin('public.m_type_values as mtv1', 'mtv1.value_id', 'uc.payment_status')
                    .where('uc.match_id', match.match_id)
                    .where('uc.username', phone_number_by_token)
                    .where('mtv.type_id', 3)
                    .where('mtv1.type_id', 6)
                    .distinct('uc.c_id', 'c.contest_name', 'c.contest_amount', 'uc.user_team_id', 'mtv.description')
                    .orderBy('uc.c_id')
                    .select(
                        'cv.v_id',
                        'cv.v_name',
                        'uc.c_id',
                        'c.contest_name',
                        'c.contest_amount',
                        'uc.user_team_id',
                        'mtv.description',
                        'mtv.value_name as trump_card_value',
                        'utc.trump_card',
                        'mtv1.value_name as payment_status',
                        'uc.user_contest_id'
                    );

                // console.log('get_user_contest_details', get_user_contest_details)

                //
                // every contest details
                //
                for (let index = 0; index < get_user_contest_details.length; index++) {
                    const element = get_user_contest_details[index];

                    //
                    // Select group by detils
                    //

                    const user_group_id = await knex('public.user_contest_group ')
                        .where('user_team_id', element.user_team_id)
                        .where('match_id', match.match_id)
                        .where('c_id', element.c_id)
                        .where('v_id', element.v_id)
                        .select('group_id')

                    // console.log('user_group_id', user_group_id)



                    for (const group_id of user_group_id) {

                        //
                        // Get rank
                        //

                        var rank = 0

                        //
                        // Select group participant user
                        //

                        const participating_group_user = await knex('public.user_contest_group ')
                            .where('group_id', group_id.group_id)
                            .select('username',
                                'total_points',
                                'user_team_id')
                            .orderBy('total_points', 'desc')

                        //  console.log('participating_group_user.length', participating_group_user.length)

                        //  console.log('participating_group_user', participating_group_user)
                        //
                        // Cureent postion user
                        //
                        const index = participating_group_user.findIndex(function(item){ return item.username == phone_number_by_token})

                        rank = await getUserRank(participating_group_user , phone_number_by_token ,participating_group_user[index].total_points,participating_group_user[index].user_team_id)

                        console.log(`username-> ${participating_group_user[index].username} ******name ->${participating_group_user[index].first_name} ******** Rank -> ${rank} ****** total_points -> ${participating_group_user[index].total_points} *****user_team_id -> ${participating_group_user[index].user_team_id}`)
                    }

                    //
                    // Prepare variant data
                    //

                    const obj = {
                        "variant_id": element.v_id,
                        "variant_name": element.v_name,
                    }
                    //console.log('variant_obj' ,obj)

                    const participating_contest = []

                    //
                    // Prepare contest data
                    //

                    const participating_contest_obj = {
                        "user_contest_id": element.user_contest_id,
                        "contest_id": element.c_id,
                        "contest_name": element.contest_name,
                        "contest_amount": element.contest_amount,
                        "payment_status": element.payment_status,
                        "rank": rank
                    }
                    // console.log('participating_contest_obj', participating_contest_obj)

                    //
                    // Prepare team data
                    //
                    const team = {
                        "user_team_id": element.user_team_id,
                        "trump_card": {
                            "trump_card_url": element.description,
                            "trump_card_id": element.trump_card,
                            "trump_card_name": element.trump_card_value
                        }
                    }

                    //   console.log('team' , team)

                    const players = []

                    //
                    // Get every team player details
                    //

                    const get_user_team = await knex('public.user_team as ut')
                        .leftJoin('public.player as p', 'p.player_id', 'ut.player_id')
                        .select('ut.is_star_player',
                            'p.thumb_url',
                            'p.player_id',
                            'p.short_name',
                            'ut.user_team_id'
                        )
                        .where('ut.user_team_id', element.user_team_id)
                 // console.log('get_user_team' , get_user_team)

                    
                    //
                    // Every palyers count total poins
                    //
                    const match_id = match.match_id
                    // const player_id = element.player_id
                    // const url = await player_url(match_id, player_id)

                    // console.log('url', url)

                    var total_points = 0

                    for (const element of get_user_team) {

                        const player_id = element.player_id
    
    
                        const point = await get_fantasy_point(match_id , player_id)
    
                        
    
    
                      total_points =  await get_total_points(match_id ,element.user_team_id)
                        
                            
                       // total_points = total_points + parseFloat(point.fantasy_points)
    
                          //  console.log('total_points ' , total_points)
                        
    
    
                        //
                        // prepare player data
                        //
    
                      
                        
                        
    
                        const url = await player_url(match_id, player_id)
    
                       
    
                        // const fantasy_points = point
    
                        //console.log(point)
    
                        if (total_points === 0) {
    
    
                            const player_obj = {
    
                                "player_id": element.player_id,
                                "player_name": `${element.short_name}`,
                                "is_start_player": element.is_star_player,
                                "thumb_url": url.thumb_url,
                                "logo_url": url.default_url,
                                "popularity": 0
    
    
                            }
    
                            players.push(player_obj);
    
                        } else {
    
    
                            const player_obj = {
    
    
                                "player_id": element.player_id,
                                "player_name": `${element.short_name}`,
                                "is_start_player": element.is_star_player,
                                "thumb_url": url.thumb_url,
                                "logo_url": url.default_url,
                                "popularity": Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2) === "NaN" ? 0 : Number((parseFloat(point.fantasy_points) / total_points) * 100).toFixed(2),
    
    
                            }
                            players.push(player_obj);
    
                        }
    
                    }

                    team.players = players;

                    participating_contest_obj.team = team;


                    participating_contest.push(participating_contest_obj)

                    obj.participating_contest = participating_contest;


                    user_contest.push(obj);


                }

                match_obj.user_contest = user_contest;


                //
                // Get every match teams
                //

                const team_a_details = await get_team_details(match.team_one_id);
                const teama_url = await team_url(match.team_one_id)
                //  console.log('teama' , team_a_details)


                //
                // prepared  teama data
                //

                const team_a_obj = {
                    'team_id': match.team_one_id,
                    'name': team_a_details[0].name,
                    'short_name': team_a_details[0].short_name,
                    'thumb_url': teama_url.logo_url,
                }

                const team_b_details = await get_team_details(match.team_two_id);
                const teamb_url = await team_url(match.team_two_id)

                //  console.log('teamb' , team_b_details)

                //
                // prepared teamb data
                //

                const team_b_obj = {
                    'team_id': match.team_two_id,
                    'name': team_b_details[0].name,
                    'short_name': team_b_details[0].short_name,
                    'thumb_url': teamb_url.logo_url,
                }

                match_obj.teama = team_a_obj;
                match_obj.teamb = team_b_obj;


            }

            match_obj.date_start = moment(match.date_start).format('YYYY-MM-DD');
            match_obj.date_end = moment(match.date_end).format('YYYY-MM-DD');
            match_obj.timestamp_start = moment(match.timestamp_start,'YYYY-MM-DDTHH:mm:ss Z').valueOf();
            match_obj.timestamp_end = moment(match.timestamp_end,'YYYY-MM-DDTHH:mm:ss Z').valueOf();

            final_response.push(match_obj);


        }

        //console.log(new Date(), 'get_contest_status_wise_final_response', final_response)
        //
        // Send response
        //



        return res.status(200).json({
            meta: {

                status: "OK",
                message: 'Success  ✅️'
            },
            data: {

                "response": final_response


            }



        })


    } catch (err) {

        console.log(`${new Date()}/api/get_user_contest_status_wise -> ${err}`)

    }


}
//
// Teams details function
//

const get_team_details = async (team_id) => {

    const result = await knex('public.team')
        .where('team_id', team_id)
        .select(
            'name',
            'short_name',
            'logo_url'
        )

    //console.log('get_team-details', get_team_details)


    return result;
}
//
// Get user contest api
//


router.get('/api/get_user_contest_status_wise', get_user_contest_status_wise);

//
// export module
//

module.exports = router;