const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token } = require('../auth/token')

const get_profile_avatar = async (req, res, next) => {



    try {

        const input = {
            token: req.headers.authorization
        }

        const phone_number_by_token = await decode_token(input.token, res)
        // const phone_number = token.encode_details.substring(0, 10);


        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')


        if (check_number_exists_or_not.length === 0) {


            return res.status(500).json({
                meta: {
                    status: '0',
                    message: 'Seems like security break.  ✅️'
                },
                data: {
                }
            })
        }

        const profile_avatar = await knex('public.profile_avatar')
            .select('avatar_url')
           
       


        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'Success ✅️'
            },
            data: {

                 "response": profile_avatar,

            }
        })

    } catch (err) {

        console.log(`${new Date() } /api/get_profile_avatar -> ${err}`)

    }
}


// registration  api
router.get('/api/get_profile_avatar', get_profile_avatar);

module.exports = router;