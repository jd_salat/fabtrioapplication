const express = require('express');
const router = express.Router();
const { decode_token } = require('../auth/token')
const knex = require('../../helper/knex');



const get_invite_code = async (req, res, next) => {


    try {

        const input = {
            token: req.headers.authorization
        }



        const phone_number_by_token = await decode_token(input.token, res)



        const check_number_exists_or_not = await knex('public.login')
            .where('phone_number', phone_number_by_token)
            .select('*')

        if (check_number_exists_or_not.length === 0) {

            return res.status(404).json({
                meta: {
                    status: '0',
                    message: `⚠️ We didn't found your mobile number in our record`
                },
                data: {

                }

            });


        }

        const invite_code = await get_invite_code_from_db(phone_number_by_token)



        const description = await knex('public.m_type_values')
            .where('type_id', 9)
            .where('value_id', 1)
            .select('description')


        const response = {
            'invite_code': invite_code.invite_code,
            'description': description[0].description
        }

        return res.status(200).json({
            meta: {
                status: 'OK',
                message: 'successfully invite code fetched✅️'
            },
            data: {
                response
            }
        })





    }
    catch (err) {

        console.log( `${new Date() } , /api/get_invite_code -> ${err}`)

    }

}

const get_invite_code_from_db = async (username) => {


    const get_code_from_db = await knex('public.invite_code')
        .where('username', username)
        .select('*')



    if (get_code_from_db.length === 0) {

        const invite_code = await generate_invite_code();



        if (await check_invite_code(invite_code) === false) {



            const invite_code_model = {
                'username': username,
                'invite_code': invite_code,
                'description': 1,
            }


            const insert_invite_code = await knex('public.invite_code')
                .insert(invite_code_model)
                .returning('*')



            return invite_code_model;



        } else {

            get_invite_code_from_db(username)



        }





    }


    return get_code_from_db[0];

}

    const check_invite_code = async (invite_code) => {

    const check_alredy_exsist_invite_code = await knex('public.invite_code')
        .where('invite_code', invite_code)

    if (check_alredy_exsist_invite_code.length === 0) {

        return false

    } else {

        return true


    }



}

const generate_invite_code = () => {


    const string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let invite_code = '';
    var len = string.length;
    for (let i = 0; i < 6; i++) {
        invite_code += string[Math.floor(Math.random() * len)];
    }
    return invite_code;
}

router.get('/api/get_invite_code', get_invite_code);

module.exports = router;