const moment = require('moment-timezone');
const jwt = require('jsonwebtoken');
const my_secret = process.env.MY_SECRET;


const generate_token = async (phone_number, expiration_time) => {

    const encode_details = phone_number + moment();
    const token = jwt.sign({ encode_details }, my_secret, {
        expiresIn: expiration_time // expires in 24 hours
    });

    return token;
}


const generate_refresh_token = async (phone_number, expiration_time) => {

    const encode_details = phone_number + moment();
    const token = jwt.sign({ encode_details }, my_secret, {});

    return token;
}


const decode_token = async (token , res ) => {

  // console.log(token);

    const verify_token = await jwt.verify(token, my_secret, async (err, decoded) => {

        if (err) return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });


        return decoded;
    })


    const phone_number = verify_token.encode_details.substring(0, 10);


    return phone_number;
};


module.exports = {
    generate_token,
    generate_refresh_token,
    decode_token
}

