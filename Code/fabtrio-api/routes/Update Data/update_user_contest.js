const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const { decode_token } = require('../auth/token')

const update_user_conest = async (req, res, next) => {

    try {


        const input = {
           
            match_id: req.body.match_id,
            user_team_id: req.body.user_team_id,
            participating_contest: req.body.participating_contest,
            token: req.headers.authorization
        }

        const data = req.body

        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                return res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
            // const phone_number = token.encode_details.substring(0, 10);


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

            if (input.username !== phone_number_by_token) {

                return res.status(424).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Warning - Seems like security Break`
                    },
                    data: {

                    }

                });

            }


            const array = []

            for (let index = 0; index < input.participating_contest.length; index++) {
                const element = input.participating_contest[index];

                const prepare_data = {
                    user_team_id: input.user_team_id,
                    username: phone_number_by_token,
                    match_id: input.match_id,
                    v_id: element.v_id,
                    c_id: element.c_id,
                    amount_paid: element.c_amount
                }

                array.push(prepare_data);


            }

            const get_user_contest_details = await knex('public.user_contest')
                .where('username', phone_number_by_token)
                .where('match_id', input.match_id)
                .where('user_team_id', input.user_team_id)
                .select('*')

            for (let index = 0; index < get_user_contest_details.length; index++) {
                const element = get_user_contest_details[index];


                const update_data = await knex('public.user_contest')
                    .where('username', phone_number_by_token)
                    .where('match_id', input.match_id)
                    .where('user_team_id', input.user_team_id)
                    .where('user_contest_id', element.user_contest_id)
                    .update(array[index])
                    .returning('*')


            }

           

            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Save User Contest Successfully ✅️'
                },
                data: {

                }
            })


        })

    }catch (err) {
        
        console.log(`${new Date() } , /api/update_user_conest -> ${err}`)
}
}



const schema = Joi.object({
    // registration_id: Joi.number().required(),
    //username: Joi.string(),
    match_id: Joi.number(),
    user_team_id: Joi.number(),
    participating_contest: Joi.array(),

})

router.put('/api/update_user_conest', update_user_conest);

module.exports = router;