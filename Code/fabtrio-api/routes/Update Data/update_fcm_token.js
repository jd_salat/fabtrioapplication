const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')




const update_fcm_token = async (req, res, next) => {

   
  
    try {

        const input = {
            token: req.headers.authorization,
            fcm_token:req.body.fcm_token
        }
        const data = req.body;

       


        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
          
           

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(424).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }
         

           
            const [{ fcm_token : fcm_token }]   = await knex("public.registration")
                .where('phone_number', phone_number_by_token)
                .update('fcm_token', input.fcm_token)
                .returning('fcm_token')

                console.log(fcm_token)
               
            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Token Upated successfully ✅️'
                },
                data: {
                    fcm_token:fcm_token
                }
            })


        })

    } catch (err) {
        
        console.log(`${new Date() } , /api/update_fcm_token -> ${err}`)

}

}
const schema = Joi.object({
  fcm_token:Joi.string()
})

router.put('/api/update_fcm_token',update_fcm_token);

module.exports = router;