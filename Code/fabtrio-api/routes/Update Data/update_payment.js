const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
const { final_amount } = require('../../helper/payment')


const update_payment_detalis = async (req, res, next) => {

    try {

        const input = {

            match_id: req.body.match_id,
            transaction_id: req.body.transaction_id,
            transaction_status: req.body.is_canceled === true ?4:1,
            transaction_amount: req.body.transaction_amount,
            token: req.headers.authorization,
            is_canceled:req.body.is_canceled
        }
        const data = req.body;


        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
            // const phone_number = token.encode_details.substring(0, 10);


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }


            const payment_details = await knex('public.transaction')
                .where('transaction_id', input.transaction_id)
                .select('*');

            if (payment_details.length === 0) {

                res.status(400).json({
                    meta: {
                        status: '1',
                        message: `payment details for transaction_id is not present`
                    },
                    data: {

                    }

                });

                return;
            }

            const user_contest_ids =  req.body.user_contest_ids


           
            const update_user_contest_payment_status =  await knex('public.user_contest')
            .whereIn('user_contest_id'  ,user_contest_ids)
            .update('payment_status' , input.transaction_status)

          //  console.log(update_user_contest_payment_status)


            // const transaction_status_mtype = await knex('public.m_type_values')
            //     .where('value_id', input.transaction_status)
            //     .where('type_id', 6)
            //     .select('*')

           // console.log(transaction_status_mtype)


          //  input.transaction_status = transaction_status_mtype[0].value_id;

           // console.log(input.transaction_status)

            // const transaction_status_number = transaction_status_json[input.transaction_status];

            const update_transaction = await knex("public.transaction")
                .where('username', phone_number_by_token)
                .where('transaction_id', input.transaction_id)
                .update('transaction_amount', input.transaction_amount)
                .update('transaction_status', input.transaction_status)
                .update('modified_on', moment())
                .returning("*")


            const update_master_payment_details = await knex('public.master_payment_details')
                .where('username', phone_number_by_token)
                .where('user_transaction_id', input.transaction_id)
                .update('amount', input.transaction_amount)
                .update('payment_status', input.transaction_status)
                .update('modified_on', moment())
                .returning('*')

             //  console.log('total_amount' , input.transaction_amount)

               if(input.transaction_status === 1){

            const final_balance = await final_amount(phone_number_by_token, input.transaction_amount)
               
            //console.log(final_balance)

            const update_bouns = await knex('public.user_wallet')
                .where('username', phone_number_by_token)
                .update('bonus_amount', final_balance.remainning_bonus_amount)
                .update('deposited_amount', final_balance.final_amount_to_pay)
                .returning('*')

               

               // console.log(update_bouns)

               }
               

            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Transaction Details Upated successfully ✅️'
                },
                data: {
                    transaction_id: input.transaction_id
                }
            })


        })

    } catch (err) {

        console.log(`${new Date() } , /api/update_payment_detalis -> ${err}`)

    }

}
const schema = Joi.object({

    match_id: Joi.number(),
    transaction_amount: Joi.number(),
    transaction_id: Joi.string(),
    user_contest_ids:Joi.array(),
    is_canceled:Joi.boolean()
})

router.put('/api/update_payment_detalis', update_payment_detalis);

module.exports = router;