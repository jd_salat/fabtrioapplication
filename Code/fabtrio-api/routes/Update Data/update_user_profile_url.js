const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')




const update_user_profile_url = async (req, res, next) => {

   
  
    try {

        const input = {
            token: req.headers.authorization,
            photo_url : req.body.photo_url
            
        }
        const data = req.body;

       


        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
          
           

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')


            if (check_number_exists_or_not.length === 0) {

                return res.status(424).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }
           

           
            const update_url = await knex('public.registration')
            .where('phone_number' , phone_number_by_token)
            .update('photo_url' , input.photo_url)

            console.log(update_url)
               
            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'Success ✅️'
                },
                data: {
                    
                }
            })


        })

    } catch (err) {
        
        console.log(`${new Date() } , /api/update_user_profile_url -> ${err}`) 

}

}
const schema = Joi.object({
  photo_url:Joi.string().required()
})

router.put('/api/update_user_profile_url',update_user_profile_url);

module.exports = router;