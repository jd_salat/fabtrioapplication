const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
const moment = require('moment-timezone');




const update_user_team = async (req, res, next) => {
    // res.send('done')

    try {

        const input = {
           
            match_id: req.body.match_id,
            user_team: req.body.user_team,
            user_team_id: req.body.user_team_id,
            trump_card_id: req.body.trump_card_id,
            token: req.headers.authorization
        }
        const data = req.body;


        Joi.validate(data, schema, async (err, value) => {



            if (err) {
                // send a 422 error response if validation fails
                res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }

            const phone_number_by_token = await decode_token(input.token, res)
            // const phone_number = token.encode_details.substring(0, 10);


            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')

            // console.log(check_number_exists_or_not)

            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

          

            const array = []

            for (let index = 0; index < input.user_team.length; index++) {
                const element = input.user_team[index];

                const prepare_data = {
                    user_team_id: input.user_team_id,
                    username: phone_number_by_token,
                    match_id: input.match_id,
                    player_id: element.player_id,
                    is_star_player: element.is_star,
                }

                array.push(prepare_data);


            }

            const get_team_details = await knex('public.user_team')
                .where('username', phone_number_by_token)
                .where('match_id', input.match_id)
                .where('user_team_id', input.user_team_id)
                .select('*')

    


            const delete_old_details = await knex('public.user_team')
            .where('username', phone_number_by_token)
            .where('match_id', input.match_id)
            .where('user_team_id', input.user_team_id)
            .delete('*')
                

            for (let index = 0; index < array.length; index++) {
                const element = array[index];

                console.log(element);

                const user_team_obj = {
                    user_team_id: element.user_team_id,
                    username: element.username,
                    match_id: element.match_id,
                    player_id: element.player_id,
                    is_star_player: element.is_star_player,
                    created_on:get_team_details[0].created_on,
                    modified_on: moment().format(),

                }

                const add_data = await knex('public.user_team')
                    .insert(user_team_obj);

                   // console.log(update_data)


            }

            const update_trump_card_data = await knex("public.user_trump_card")
                .where('user_team_id', input.user_team_id)
                .update('trump_card', input.trump_card_id)
                .returning("*")

            return res.status(200).json({
                meta: {
                    status: 'OK',
                    message: 'User Team Upated successfully ✅️'
                },
                data: {
                    
                }
            })


        })

    } catch (err) {
        
        console.log(`${new Date() } , /api/update_user_team  -> ${err}`)

}
}

const schema = Joi.object({
    // registration_id: Joi.number().required(),
   // username: Joi.string(),
    match_id: Joi.number(),
    user_team_id: Joi.number(),
    user_team: Joi.array().min(3).max(3),
    trump_card_id: Joi.number(),

})

router.put('/api/update_user_team', update_user_team);

module.exports = router;