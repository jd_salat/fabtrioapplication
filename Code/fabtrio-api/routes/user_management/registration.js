const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const Joi = require('joi');
const { decode_token } = require('../auth/token')
var fs = require('fs'); //Filesystem    
const path = require('path');

// console.log('path',path.resolve(__dirname, "../../email/success_email.html"));

var content = fs.readFileSync(path.resolve(__dirname, "../../email/template/success_email.html"),"utf-8");

const main = require('../../helper/send_email')

const registration = async (req, res, next) => {

    try {
        const input = {
            phone_number: req.body.phone_number,
            email: req.body.email,
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            is_kyc_done: req.body.is_kyc_done,
            dob: req.body.dob,
            state_id: req.body.state_id,
            photo_url:req.body.photo_url,
            created_at: moment().format(),
            modified_at: moment().format(),
            token: req.headers.authorization,
            fcm_token: req.body.fcm_token,
            invitee_code:req.body.invite_code
        }


       

        data = req.body
       

        Joi.validate(data, schema, async (err, value) => {

            if (err) {
                // send a 422 error response if validation fails
                return res.status(422).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Enter ${err}`
                    },
                    data: {

                    }

                });


            }
            const phone_number_by_token = await decode_token(input.token , res)

            const check_number_exists_or_not = await knex('public.login')
                .where('phone_number', phone_number_by_token)
                .select('*')

                

            if (check_number_exists_or_not.length === 0) {

                return res.status(404).json({
                    meta: {
                        status: '0',
                        message: `⚠️ We didn't found your mobile number in our record`
                    },
                    data: {

                    }

                });


            }

            if (input.phone_number !== phone_number_by_token) {

                return res.status(500).json({
                    meta: {
                        status: '0',
                        message: `⚠️ Warning - Seems like security Break`
                    },
                    data: {

                    }

                });

            }

            const user_details_by_phone_number = await knex('public.registration')
                .where('phone_number', input.phone_number)
                .select('*');

            if (user_details_by_phone_number.length !== 0) {

                return res.status(424).json({
                    meta: {
                        status: '1',
                        message: '⚠️ User Alredy Registered'
                    },
                   
                })

            } else {


              
                const [{ state_name: state_name }] = await knex('public.state')
                    .where('id', input.state_id)
                    .select('state_name')


                
                input.state_name = state_name;

                const [{verification_status : verification_status}] = await knex('public.login')
                    .where('phone_number', input.phone_number)
                    .select('verification_status')

                input.is_mobile_verified = verification_status;

                delete input.state_id;

                delete input.token;

                await save_details_in_db(input);

                const bonus_amount= await knex('public.m_type_values')
                .where('type_id', 10)
                .where('value_id', 1)
                .select('value_name')

              //  console.log(bonus_amount)

                user_wallet_model = {
                    username: input.phone_number,
                    bonus_amount: parseInt(bonus_amount[0].value_name),
                    deposited_amount:0,
                    winning_amount:0


                }

                const save_bouns_point = await knex('public.user_wallet')
                .insert(user_wallet_model)

                const time_stamp = new Date().valueOf();
                const transaction_id = `FAB${time_stamp}`;


                const transaction_data = {
                    transaction_id: transaction_id,
                    transaction_type: 1,
                    transaction_amount: bonus_amount[0].value_name,
                    transaction_date: moment().format('YYYY-MM-DD'),
                    username: input.phone_number,
                    match_id: null,
                    created_on: moment().format(),
                    modified_on: moment().format(),
                    transaction_message: 'Welcome Bonus',
                    transaction_status: 1
                }

                // console.log(' tarntable', transaction_data)

                const save_transaction_details_in_db = await knex('public.transaction')
                    .insert(transaction_data)
                    .returning('*')


                const txn_id = `FABMASTER${time_stamp}`;

                const prepare_data_status_1 = {

                    user_transaction_id: transaction_id,
                    username: input.phone_number,
                    transaction_id: txn_id,
                    amount: bonus_amount[0].value_name,
                    transaction_type: 2,
                    payment_date: moment().format('YYYY-MM-DD'),
                    match_id: null,
                    created_on: moment().format(),
                    modified_on: moment().format(),
                    payment_status: 1
                }
                //   console.log('mastrtrnx', prepare_data_status_1)
                
                const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                    .insert(prepare_data_status_1)
                    .returning('*')


               //  email send sucessfull email
                let mail_template = content.replace('{UserName}', input.first_name + ' ' +input.last_name);

               // console.log('mailTemplate', mailTemplate);
               const successful_registration_email = await main(mail_template, input.email)

                return res.status(200).json({
                    meta: {
                        status: 'OK',
                        message: 'User registered successfully ✅️'
                    },
                })

            }

        })

    } catch (err) {
        
        console.log(`${new Date() } , /api/registration -> ${err}`)

}

}
const save_details_in_db = async (data) => {

    // console.log(data)  delete data.files;

    const result = await knex('public.registration')
        .insert(data);

    return result;

}


const schema = Joi.object({
    phone_number: Joi.string().regex(/^\d{3}\d{3}\d{4}$/).required(),
    email:Joi.string().allow(''),
    first_name: Joi.string().min(3).max(40).required(),
    last_name: Joi.string().min(3).max(40).required(),
    is_mobile_verified: Joi.string(),
    is_email_verified: Joi.string(),
    is_kyc_done: Joi.string(),
    dob: Joi.string().regex(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/),
    state_id: Joi.number(),
    photo_url:Joi.string(),
    fcm_token:Joi.string().optional().allow(''),
    invitee_code:Joi.string().optional().allow('')

})


// registration  api
router.post('/api/registration', registration);

module.exports = router;
