const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');

const { decode_token , generate_token} = require('../auth/token')

const refresh_token = async (req, res, next) => {
    try {
        
    

    const input = {
        token: req.body.refresh_token
    }
    

    const phone_number_by_token = await decode_token(input.token , res)
    // const phone_number = token.encode_details.substring(0, 10);


    const check_number_exists_or_not = await knex('public.login')
        .where('phone_number', phone_number_by_token)
        .select('*')


    if (check_number_exists_or_not.length === 0) {


        return res.status(424).json({
            meta: {
                status: '0',
                message: `⚠️ We didn't found your mobile number in our record`
            },
            data: {

            }

        });
    }

    const generate_new_token = await generate_token(phone_number_by_token, '1h');

    const save_token_db = await knex('public.login')
        .update('token', generate_new_token)
        .where('phone_number', phone_number_by_token)



    return res
        .status(200)
        .send({

            meta: {
                status: '1',
                message: 'token refreshed successfully'
            },
            data: {
                auth: false,
                token: generate_new_token,
                refresh_token: input.token
            }

        })
    } catch (err) {
        
        console.log(`${new Date() } , /api/refresh_token -> ${err}`) 

}
    
}


router.post('/api/refresh_token', refresh_token);

module.exports = router;