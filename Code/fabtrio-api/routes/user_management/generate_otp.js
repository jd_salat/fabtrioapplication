//import  module

const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
var unirest = require("unirest");
const crypto = require('crypto');

const my_secret = process.env.MY_SECRET;
const algorithm = process.env.ALGORITHM;



const Joi = require('joi');

const otp_generate = async (req, res, next) => {


    try {
        const data = req.body;
        const input = {
            
           phone_number: data.phone_number
        };

       
        console.log(input.phone_number);

        Joi.validate(data, schema, async (err, value) => {

            const find_details = await knex('public.login')
                .where('phone_number', input.phone_number)



            if (find_details && find_details.length) {
                let otp = '';
                if (input.phone_number == '9967605522'){

                    otp = '123456'

                }else {

                     otp = await generateOTP();
                     
                }

                

                const add_otp_in_db = await knex('public.login')
                    .update("mobile_otp", otp)
                    .update('modified_at', moment())
                    .update('verification_status', false)
                    .where("phone_number", "=", input.phone_number)
                    .returning("*")


                const find_user_details = await knex('public.registration')
                    .where('phone_number', input.phone_number)
                    .select('*')

                if (find_user_details && find_user_details.length) {


                    //  await send_sms(input, otp)
                    return res
                        .status(200)
                        .json({
                            meta:
                            {
                                status: '1',
                                message: 'Otp Sent successfully ✅️'
                            },
                            data: {
                                "is_user_registered": 1,
                                otp,
                            }
                        })

                }


                // await send_sms(input, otp)
                return res
                    .status(200)
                    .json({
                        meta:
                        {
                            status: '1',
                            message: 'Otp Sent successfully ✅️'
                        },
                        data: {
                            "is_user_registered": 0,
                           
                           'otp':otp,
                        }
                    })

            } else {

                const add_phone_number_in_db = await knex('public.login')
                    .insert({ 'phone_number': input.phone_number })
                    .returning('*')

                const otp = await generateOTP();

                const add_otp_in_db = await knex('public.login')
                    .update("mobile_otp", otp)
                    .update('created_at', moment())
                    .update('modified_at', moment())
                    .update('verification_status', false)
                    .where("phone_number", "=", input.phone_number)
                    .returning("*")

                // await send_sms(input, otp);

                return res
                    .status(200)
                    .json({
                        meta:
                        {
                            status: 'OK',
                            message: 'Mobile number added And Send Otp ✅️'
                        },
                        data: {
                            "is_user_registered": 0,
                            
                          'otp': otp,
                        }
                    })
            }
        })
    } catch (err) {

        console.log(`${new Date() } , /api/otp/generate_otp -> ${err}`)

    
    }
}

// Function to generate OTP
const generateOTP = () => {

    // Declare a digits variable which stores all digits
    const digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < 6; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return OTP;
}

// create new contact
const send_sms = async (data, otp, res) => {

    // console.log(data);
    // console.log(otp)

    let phone_number = data.phone_number;
    try {
        var req = unirest("POST", "http://mobicomm.dove-sms.com/submitsms.jsp?");

        // req.headers({
        //     "authorization": "8db582baeaXX"
        // });

        req.form({
            "user": "HARISH",
            "senderid": "SCHOOL",
            "key": "8db582baeaXX",
            "message": `Your otp is ${otp}`,
            "language": "english",
            "accusage": "1",
            // "route": "p",
            "mobile": phone_number,
        });

        req.end(function (res) {
            if (res.error) throw new Error(res.error);

            console.log(res.body);
        });
        // res.json(resMsg);

    } catch (error) {
        // throw error;

        console.log(error);

    }
}


const schema = Joi.object({
    phone_number: Joi.string().regex(/^\d{3}\d{3}\d{4}$/).required()
})

// const decrypt = (text) => {
    
//     var decipher = crypto.createDecipher(algorithm, my_secret+text)
//     var dec = decipher.update(text, 'hex', 'utf8')
//     dec += decipher.final('utf8');
//     return dec;
// }

// otp generate api
router.put('/api/otp/generate_otp', otp_generate);

module.exports = router;
