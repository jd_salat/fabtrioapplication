
const knex = require('./knex');
const moment = require('moment-timezone');


const refund_amount = async (match_id) => {

    try {


        const get_user_participant_in_contest = await knex('public.user_contest')
            .where('match_id', match_id)
            .where('payment_status', 1)
            .select('c_id', 'v_id', 'match_id', 'amount_paid', 'username', 'user_team_id')

        console.log('get_user_participant_in_contest', get_user_participant_in_contest);

        for (let index = 0; index < get_user_participant_in_contest.length; index++) {
            const element = get_user_participant_in_contest[index];

            const contest_amount = parseFloat(element.amount_paid).toFixed(2)

            const wallet_details = await knex('public.user_wallet')
                .select('deposited_amount')
                .where('username', element.username)


            if (wallet_details && wallet_details.length > 0) {

                console.log('contest_amount', contest_amount)

                const final_deposited_amount = parseFloat(parseFloat(wallet_details[0].deposited_amount) + parseFloat(contest_amount)).toFixed(2)

                console.log('final_deposited_amount', final_deposited_amount)

                const updated_wallet = await knex('public.user_wallet')
                    .where('username', winner_user.username)
                    .update('deposited_amount', final_deposited_amount)
                    .update('modified_on', moment())

            }
            const get_match = await knex('public.match')
                .where('match_id', match_id)
                .select('title', 'date_start')

            const time_stamp = new Date().valueOf();
            const transaction_id = `FAB${time_stamp}`;


            const transaction_data = {
                transaction_id: transaction_id,
                transaction_type: 1,
                transaction_amount: contest_amount,
                transaction_date: moment().format('YYYY-MM-DD'),
                username: element.username,
                match_id: match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                transaction_message: `Refund contest entry fees for ${get_match[0].title} - ${moment(get_match[0].date_start).format('DD-MMM')} `,
                transaction_status: 1
            }

            // console.log(' tarntable', transaction_data)

            const save_transaction_details_in_db = await knex('public.transaction')
                .insert(transaction_data)
                .returning('*')


            const txn_id = `FABMASTER${time_stamp}`;

            const prepare_data_status_1 = {

                user_transaction_id: transaction_id,
                username: element.username,
                transaction_id: txn_id,
                amount: contest_amount,
                transaction_type: 2,
                payment_date: moment().format('YYYY-MM-DD'),
                match_id: match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                payment_status: 1
            }
            //   console.log('mastrtrnx', prepare_data_status_1)
            const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                .insert(prepare_data_status_1)
                .returning('*')



        }


    } catch (error) {

        console.error(error);
    }

}

module.exports = { refund_amount }; 