
//var group_id = 0;
// var groups = [];
async function generateGroups(users_data, total_entries, total_groups_to_create, max_participants_per_group, group_id, groups) {
  // console.log('group length -> ',groups.length)
    var group_array = [];
    var waiting_array = [];
    var all_user_array = [...users_data]
    // console.log(`total_entries->${total_entries}`)
    //console.log(`max_participants_per_group->${max_participants_per_group}`)

    // console.log('group_array' ,group_array)
    if (groups.length < total_groups_to_create) {
        for (const users of all_user_array) {

            // console.log('total_allocated_members', getAllocatedMemberCount(groups))
            // console.log('Every iteration group_array length ->', group_array.length)
            if (group_array.length > 0) {

                if (group_array.length === max_participants_per_group) {
                    group_id = group_id + 1;
                    //console.log("-----------------------------------------------------------------------------------------")
                    //  console.log(`group_array after clearing array-> for group_id->${group_id}`, JSON.stringify(group_array))
                    // insert group into databse
                    let group_to_insert = {
                        group_id: group_id,
                        group: [...group_array]
                    }
                    groups.push(group_to_insert)
                    group_array.splice(0, group_array.length) // empty group array after inserting into database

                    group_array.push(users) // push current user into group array
                    // console.log("-----------------------------------------------------------------------------------------")
                    //console.log('group_array after inserting into groups this length should be 1 ',group_array.length)
                    //all_user_array.push(...waiting_array) // push waiting array into all user array so that it can be reconsidered in next group.

                } else {
                    let is_user_eligible_for_this_group = await isUserEligible(group_array, users)
                    //   console.log('is_user_eligible_for_this_group -> ', is_user_eligible_for_this_group);
                    if (is_user_eligible_for_this_group === true) {
                        group_array.push(users);
                    } else {
                        waiting_array.push(users);
                        // console.log(`waiting_array inserted ->${JSON.stringify(waiting_array)}`)

                    }
                }
            } else {
                group_array.push(users);
            }
        }
    } else {
        waiting_array = [...users_data]
    }


    if (group_array.length > 0) { // last group to insert into database 
        // console.log('inside if after loop group_id->', group_id)
        // console.log('inside if after loop group_array.length->',group_array.length)
        group_id = group_id + 1;
        let group_to_insert = {
            group_id: group_id,
            group: [...group_array]
        }
        groups.push(group_to_insert)
        group_array.splice(0, group_array.length); //empty group array after inserting into database
    }
    if (waiting_array.length > 0) {
        if (groups.length === total_groups_to_create) {

            for (let group_index = 0; group_index < groups.length; group_index++) {
                let generated_group = groups[group_index].group
                if (generated_group.length === max_participants_per_group) {
                    continue;
                } else {
                    for (let index = 0; index < max_participants_per_group - generated_group.length; index++) { // only run loop for groups remaining entries to fill.
                        if (index < waiting_array.length) { // check if waiting array has members or not.
                            let is_user_eligible_in_second_condition = await isUserEligibleForSecondCondition(generated_group, waiting_array[index]);
                            if (is_user_eligible_in_second_condition === true) {
                                generated_group.push(waiting_array[index])
                                waiting_array.splice(index, 1)
                            }
                            else {
                                let is_user_eligible_in_third_condition = await isUserEligibleForThirdCondition(generated_group, waiting_array[index]);
                                if (is_user_eligible_in_third_condition === true) {
                                    generated_group.push(waiting_array[index])
                                    waiting_array.splice(index, 1)
                                }
                            }
                        } else {
                            break;
                        }
                    }
                    //  console.log('generated_group array length -> ', generated_group.length)
                    groups.splice(group_index, 1, { group_id: groups[group_index].group_id, group: generated_group })// replace group with latest elements.
                }

                if (group_index === groups.length - 1 && waiting_array.length > 0) {
                    return generateGroups(waiting_array, total_entries, total_groups_to_create, max_participants_per_group, group_id, groups)
                }
            }

        } else {
            return generateGroups(waiting_array, total_entries, total_groups_to_create, max_participants_per_group, group_id, groups)
        }
    }
    // console.log(JSON.stringify(groups))
    return groups;
}

function isUserEligible(group_array, users) {
    let is_first_player_present = false
    let is_second_player_present = false
    let is_third_player_present = false
    let is_trump_card_present = false
    let is_user_eligible_for_this_group = true;
    for (const group_member of group_array) {
        if (group_member[0].player_id === users[0].player_id || group_member[0].player_id === users[1].player_id || group_member[0].player_id === users[0].player_id) {
            is_first_player_present = true
        }
        if (group_member[1].player_id === users[0].player_id || group_member[1].player_id === users[1].player_id || group_member[1].player_id === users[0].player_id) {
            is_second_player_present = true
        }
        if (group_member[2].player_id === users[0].player_id || group_member[2].player_id === users[1].player_id || group_member[2].player_id === users[0].player_id) {
            is_third_player_present = true
        }
        if (group_member[0].trump_card === users[0].trump_card) {
            is_trump_card_present = true
        }
        if (group_member[0].username === users[0].username) {
            is_user_eligible_for_this_group = false;
            break;
        }
        if (is_first_player_present === true && is_second_player_present === true && is_third_player_present === true && is_trump_card_present === true) {
            is_user_eligible_for_this_group = false;
            break;
        } else {
            is_first_player_present = false;
            is_second_player_present = false;
            is_third_player_present = false;
            is_trump_card_present = false;
            is_user_eligible_for_this_group = true;
        }
    }

    return is_user_eligible_for_this_group;


}


function getAllocatedMemberCount(groups) {
    let count = 0;
    for (const group of groups) {
        count = count + group.group.length
    }
    return count;
}
function isUserEligibleForSecondCondition(group_array, users) {
    let is_user_eligible_for_this_group = true;
    for (const group_member of group_array) {
        if (group_member[0].username === users[0].username) {
            is_user_eligible_for_this_group = false;
            // break;
            return is_user_eligible_for_this_group;
        }
    }
    return is_user_eligible_for_this_group;
}

function isUserEligibleForThirdCondition(group_array, users) {
    let is_first_player_present = false
    let is_second_player_present = false
    let is_third_player_present = false
    let is_trump_card_present = false
    let is_user_eligible_for_this_group = true;
    for (const group_member of group_array) {
        if (group_member[0].player_id === users[0].player_id || group_member[0].player_id === users[1].player_id || group_member[0].player_id === users[0].player_id) {
            is_first_player_present = true
        }
        if (group_member[1].player_id === users[0].player_id || group_member[1].player_id === users[1].player_id || group_member[1].player_id === users[0].player_id) {
            is_second_player_present = true
        }
        if (group_member[2].player_id === users[0].player_id || group_member[2].player_id === users[1].player_id || group_member[2].player_id === users[0].player_id) {
            is_third_player_present = true
        }
        if (group_member[0].trump_card === users[0].trump_card) {
            is_trump_card_present = true
        }
        if (is_first_player_present === true && is_second_player_present === true && is_third_player_present === true && is_trump_card_present === true) {
            is_user_eligible_for_this_group = false;
            break;
        } else {
            is_first_player_present = false;
            is_second_player_present = false;
            is_third_player_present = false;
            is_trump_card_present = false;
            is_user_eligible_for_this_group = true;
        }
    }

    return is_user_eligible_for_this_group;


}

module.exports = generateGroups;