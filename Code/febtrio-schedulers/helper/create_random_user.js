const knex = require("./knex")
const moment = require('moment-timezone')


async function random_user(all_user_array, match_id, c_id, v_id) {

    //  console.log('all_user_array.lenth', all_user_array)

    const count_group_member = Math.ceil(all_user_array.length / 10)


    //  console.log(count_group_member);

    // console.log('count_group_member', count_group_member)
    let create_random_user_array = [];

    if (count_group_member === 0) {

        return create_random_user_array


    } else {

        const find_missing_user_count = (count_group_member * 10) - all_user_array.length

        //   console.log('find_missing_user_count', find_missing_user_count)


        for (let index = 0; index < find_missing_user_count; index++) {

            let user_details_array = []

            const get_random_user = await knex('public.random_user')
                .select('username')
                .orderBy('username', 'asc')

            // const shuffled_user_array = get_random_user.sort(() => 0.5 - Math.random());

            // const select_random_user = shuffled_user_array[Math.floor(Math.random() * shuffled_user_array.length)]

            //check alredy exting user team id if alredy present  to use this user_team_id
            let user_team_id;

            const select_random_user = get_random_user[index]
            // console.log('here');

            const get_user_team_id = await knex('public.user_team')
                .where('username', select_random_user.username)
                .where('match_id', match_id)
                .select('user_team_id', 'username', 'match_id', 'player_id', 'is_star_player')

            if (get_user_team_id && get_user_team_id.length) {


                user_team_id = get_user_team_id[0].user_team_id

                user_details_array = [...get_user_team_id]

            } else {


                // console.log('in else')
                const [{ last_value: latest_team_id }] = await knex('public.user_team_seq')
                    .select('last_value')

                user_team_id = parseInt(latest_team_id) + 1;



                const get_playing11_player = await knex('public.team_squad')
                    .where('match_id', match_id)
                    .where('playing11', true)
                    .select('player_id')


                // console.log('select_random_user', select_random_user)

                const shuffled_player_array = get_playing11_player.sort(() => 0.5 - Math.random());

                //  console.log('shuffled_player_array' ,shuffled_player_array.length)

                const trump_card_array = [1, 2, 3];

                const random_trump_card = trump_card_array[Math.floor(Math.random() * trump_card_array.length)]

                const user_team_player_1 = {
                    player_id: shuffled_player_array[0].player_id,
                    username: select_random_user.username,
                    user_team_id: user_team_id,
                    match_id: match_id,
                    is_star_player: 0,
                }

                await save_player_details_in_db(user_team_player_1);

                user_details_array.push(user_team_player_1);

                const user_team_player_2 = {
                    player_id: shuffled_player_array[1].player_id,
                    username: select_random_user.username,
                    user_team_id: user_team_id,
                    match_id: match_id,
                    is_star_player: 1,
                }

                await save_player_details_in_db(user_team_player_2);

                user_details_array.push(user_team_player_2);

                const user_team_player_3 = {
                    player_id: shuffled_player_array[2].player_id,
                    username: select_random_user.username,
                    user_team_id: user_team_id,
                    match_id: match_id,
                    is_star_player: 0,
                }

                await save_player_details_in_db(user_team_player_3);

                user_details_array.push(user_team_player_3);

                const trump_card_mapping = {
                    user_team_id: user_team_id,
                    trump_card: random_trump_card,
                }

                const save_trump_card_mapping = await knex('public.user_trump_card')
                    .insert(trump_card_mapping);

                const update_sequence = await knex.schema.withSchema('public').raw(`ALTER SEQUENCE user_team_seq RESTART WITH ${trump_card_mapping.user_team_id}`)

            }


            //get amount contest table

            const contest_amount = await knex('public.contest')
                .where('c_id', c_id)
                .where('v_id', v_id)
                .pluck('contest_amount')

            //   console.log('get_contest_amount', contest_amount)


            let transaction_time = new Date().valueOf();

            const transaction_id = `FAB${transaction_time}`;


            const transaction_data = {
                transaction_id: transaction_id,
                transaction_type: 1,
                transaction_amount: parseFloat(contest_amount),
                transaction_date: moment().format('MM/DD/YYYY'),
                username: select_random_user.username,
                match_id: match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                transaction_message: 2,
                transaction_status: 1
            }

            const save_transaction_details_in_db = await knex('public.transaction')
                .insert(transaction_data)
                .returning('*')



            const txn_id = `FABMASTER${transaction_time}`;

            const prepare_data_status_1 = {

                user_transaction_id: transaction_id,
                username: select_random_user.username,
                transaction_id: txn_id,
                amount: parseFloat(contest_amount),
                transaction_type: 2,
                payment_date: moment().format('MM/DD/YYYY'),
                match_id: match_id,
                created_on: moment().format(),
                modified_on: moment().format(),
                payment_status: 1
            }
            //   console.log('mastrtrnx', prepare_data_status_1)
            const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                .insert(prepare_data_status_1)
                .returning('*')



            const prepare_data_for_user_contest = {
                c_id: c_id,
                v_id: v_id,
                match_id: match_id,
                amount_paid: parseInt(contest_amount),
                user_team_id: user_team_id,
                username: select_random_user.username,
                total_points: 0,
                transaction_id: transaction_id,
                payment_status: 1,
            }


            // console.log('prepare_data_for_user_contest', prepare_data_for_user_contest)
            const user_contest = await save_user_contest_details_in_db(prepare_data_for_user_contest)

            //  console.log('user_contest' , user_contest)

            // console.log(user_details_array);

            user_details_array[0].user_contest_id = user_contest[0].user_contest_id
            user_details_array[1].user_contest_id = user_contest[0].user_contest_id
            user_details_array[2].user_contest_id = user_contest[0].user_contest_id

            const deposited_amount = await knex('public.user_wallet')
                .where('username', select_random_user.username)
                .pluck('deposited_amount');

            const update_user_wallet_amount = await knex('public.user_wallet')
                .where('username', select_random_user.username)
                .update('deposited_amount', parseFloat(parseFloat(deposited_amount) - parseFloat(contest_amount)).toFixed(2));


            create_random_user_array[index] = user_details_array;


        }


    }

  //  console.log('create_random_user_array length', create_random_user_array.length)


    return create_random_user_array;
}



const save_player_details_in_db = async (input) => {


    const result = await knex('public.user_team')
        .insert(input);

    return result;

}
const save_user_contest_details_in_db = async (data) => {


    const result = await knex('public.user_contest')
        .insert(data)
        .returning('*');

    return result;

}


module.exports = random_user