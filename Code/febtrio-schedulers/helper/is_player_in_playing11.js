
const knex = require('./knex');

const player_details = async (match_id) => {

    const player_is_not_in_playing11 = await knex('public.team_squad')
        .pluck('player_id')
        .where('playing11', false)
        .where('match_id', match_id)

    // console.log(player_is_not_in_playing11)

    const get_user_team_details = await knex('user_team as ut')
            .leftJoin('public.player as p' ,'p.player_id' ,'ut.player_id')
            .select('ut.username',
        'p.first_name',
        'p.last_name'
        )
        .whereIn('ut.player_id', player_is_not_in_playing11)
        .where('ut.match_id', match_id)

  //  console.log(get_user_team_details)


return get_user_team_details


}

module.exports = player_details;