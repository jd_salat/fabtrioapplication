// const knex = require('knex')({
//   client: 'pg',
//   version: '7.2',
//   connection: {
//     host : 'febtrios.ct1rcqji09cv.us-east-2.rds.amazonaws.com',
//     user : 'master',
//     password : 'postgres',
//     database : 'febtrio'

//   },
//   // connection: {
//   //   host : 'localhost',
//   //   user : 'postgres',
//   //   password : 'postgres',
//   //   database : 'febtrio'
//   // },
//   pool: {
//     min: 1,
//     max: 2,
//   },
//   ssl: true,
//   debug: false
// });
// //expose knex connection object;
// module.exports = knex;


const knex = require('knex')({

    client: 'postgresql',
    connection: process.env.DATABASE_URL,
    pool: {
      min: 2,
      max: 10
    },
    ssl : true,
    debug: false
  

  })


//expose knex connection object;
module.exports = knex;