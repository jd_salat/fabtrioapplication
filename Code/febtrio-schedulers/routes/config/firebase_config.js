var admin = require("firebase-admin");

var serviceAccount = require('./fabtrio-1803-firebase-adminsdk-rqhpe-599bee9965.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.URL
});

module.exports.admin = admin;