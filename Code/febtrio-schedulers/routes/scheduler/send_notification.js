const knex = require('../../helper/knex');
const { admin } = require('../config/firebase_config');
const express = require('express');


const send_notification_with_notification = async (phone, payload, notification_type, value_id) => {

    try {


        const options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24, // 1 day
        };

        [{ fcm_token }] = await knex('public.registration')
            .select('fcm_token')
            .where('phone_number', phone)

        console.log(fcm_token)

        var registrationToken = `${fcm_token}`



        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(response => {

                console.log('Notification sent successfully')

                add_notification_message(value_id, notification_type, phone, payload.notification.body)



            })
            .catch(error => {
                console.log(error);

                send_notification_with_notification(phone, payload)



            });






    } catch (err) {

        console.log(`${new Date()},send_notification_with_notification -> ${err}`)


    }
}

const add_notification_message = async (value_id, notification_type, username, notification_message) => {

    const [{ description }] = await knex('public.m_type_values')
        .select('description')
        .where('type_id', 13)
        .where('value_id', value_id)

    console.log(description)

    const prepared_data = {
        notification_message: notification_message,
        notification_type: notification_type,
        notification_image_url: description,
        username: username


    }

    const save_notification_in_db = await knex('public.notification')
        .insert(prepared_data)

}

const send_notification_with_data = async (phone, data) => {

    try {

        const payload = {
            data: {
                account: "Savings",
                balance: "$3020.25"
            }

        };

        const options = {
            priority: 'high',
            timeToLive: 60 * 60 * 24, // 1 day
        };

        [{ fcm_token }] = await knex('public.registration')
            .select('fcm_token')
            .where('phone_number', phone)

        console.log(fcm_token)

        var registrationToken = `${fcm_token}`



        admin.messaging().sendToDevice(registrationToken, payload, options)
            .then(async response => {

                //    console.log('Notification sent successfully')

                //     const [{ description }] = await knex('pubic.m_type_values')
                //     .select(description)
                //     .where('type_id', 13)
                //     .where('value_id', 2)

                // console.log(description)

                // const prepared_data = {
                //     notification_message: payload.notification.body,
                //     notification_type: 2,
                //     notification_image_url: description

                // }

                // const save_notification_in_db = await knex('public.notification')
                //     .insert(prepared_data)



            })
            .catch(error => {
                console.log(error);

                //     const [{ description }] = await knex('pubic.m_type_values')
                //     .select(description)
                //     .where('type_id', 13)
                //     .where('value_id', 3)

                // console.log(description)

                // const prepared_data = {
                //     notification_message: payload.notification.body,
                //     notification_type: 3,
                //     notification_image_url: description

                // }

                // const save_notification_in_db = await knex('public.notification')
                //     .insert(prepared_data)


            });




    } catch (err) {

        console.log(`${new Date()},send_notification_with_notification -> ${err}`)


    }
}

const send_notification_with_topic = async (payload) => {

    try {

        const topic = "match_update";


        admin.messaging().send(payload)
            .then(response => {

                console.log('Notification sent successfully')

            })
            .catch(error => {
                console.log(error);
            });




    } catch (err) {

        console.log(`${new Date()},send_notification_with_notification -> ${err}`)

    }
}



module.exports = {

    send_notification_with_notification,
    send_notification_with_data,
    send_notification_with_topic

}