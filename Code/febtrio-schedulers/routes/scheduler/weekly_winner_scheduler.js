const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');


var CronJob = require('cron').CronJob;

var job = new CronJob('0 0 * * SAT', async () => {

    await weekly_winner()

    console.log(new Date(), 'wikeekly_winner -You will see this message every saturday');


}, null, true, 'America/Los_Angeles');
job.start();



const weekly_winner = async () => {



    try {


        const delete_weekley_winner = await knex('public.weekly_winner')
        .delete()

            const user_winning_details = await knex('public.winners')
                .sum('total_points as total_points')
                .sum('winning_amount as total_amount')
                .orderBy('total_amount' , 'desc')
                .groupBy('username')
                .limit(10)
                .count('c_id as total_contest')
                .select('username')

             //   console.log('user by point ' , user_winning_details)

               

          

            for (const total of user_winning_details) {


             
            const weekley_winner_obj = {
                username: total.username,
                total_points: total.total_points,
                total_contest: total.total_contest,
                prize_money: total.total_amount,
                date: moment().format('YYYY-MM-DD')
            }

         //console.log('weekly winner obj' , weekley_winner_obj)

            const weekley_winners = await knex('public.weekly_winner')
                .insert(weekley_winner_obj);




            }

    


    }
    catch (err) {

        console.log( `${new Date()},/api/weekly_winner -> ${err}`)


    }

}

router.get('/api/weekly_winner', weekly_winner);

module.exports = router;