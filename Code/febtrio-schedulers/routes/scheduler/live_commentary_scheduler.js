const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const rp = require('request-promise-native');
const token = process.env.TOKEN;

var CronJob = require('cron').CronJob;

var job = new CronJob('*/3 * * * *', async () => {



    await live_commentary()

    console.log( new Date(),'live commentary scheduler -You will see this message every 3 minutes');


}, null, true, 'America/Los_Angeles');
job.start();



const live_commentary = async (req, res, next) => {



    // console.log(`req params ${JSON.stringify(req.query)}`

    const get_match = await knex('public.match')
        .where('status', 3)
        .select('match_id','inning_number')

    //console.log(get_match);

    for (let index = 0; index < get_match.length; index++) {
        const element = get_match[index];

        const match_id =  element.match_id

        const options = {
            uri: `https://rest.entitysport.com/v2/matches/${match_id}/innings/${element.inning_number}/commentary`,
            qs: {

                token: token,
            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        }


        rp(options).then(async (repos) => {

            //   console.log(repos.response)
            await save_details_in_db(repos.response, match_id);

            // console.log('function calles');



            return;
            //  res
            //     .status(200)
            //     .send({
            //         status: 'SUCCESS',
            //         response: repos.response
            //     });

        })
            .catch(function (err) {

                console.error(err.message); // API call failed...
                return
                // res
                //     .status(500)
                //     .send({ status: 'FAILURE' });

            });


    }

}

const save_details_in_db = async (data, match_id) => {

    


    try {

        const inning_number = data.inning.number;

       // console.log(inning_number)
        const commentaries = data.commentaries;

        for (let index = 0; index < commentaries.length; index++) {
            const element = commentaries[index];

         //   console.log(element);



            if (element.event != 'overend') {

                const get_commentries_detilas = await knex('public.ball_by_ball')
                    .where('match_id', match_id)
                    .where('over', element.over)
                    .where('ball', element.ball)
                    .where('inning_number', inning_number)
                    .select('*')

               // console.log(get_commentries_detilas);

                if (get_commentries_detilas.length === 0) {

                    const commentarie_obj = {
                        match_id: match_id,
                        event: element.event,
                        over: element.over,
                        ball: element.ball,
                        score: element.score,
                        commentary: element.commentary,
                        inning_number: inning_number,
                    }

                    const add_live_commentary = await knex('public.ball_by_ball')

                        .insert(commentarie_obj)

                   // console.log(add_live_commentary)

                }

            }
        }





    } catch (err) {

        console.log(`${new Date()},/api/get_match_info_for_live_match -> ${err}`)


    }

}

router.get('/api/live_commentary', live_commentary);

module.exports = router;