const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
var CronJob = require('cron').CronJob;
const player_details = require('../../helper/is_player_in_playing11')
const {send_notification_with_notification} = require('./send_notification');



var job = new CronJob('*/6 * * * *', async () => {


   await change_match_status_to_live_scheduler()

    console.log(new Date(),'change_match_status_to_live_scheduler - You will see this message every 10 minutes' );


}, null, true, 'America/Los_Angeles');

job.start();




const change_match_status_to_live_scheduler = async () => {

    try {
//console.log('here')

        const get_matches = await knex('public.match')
            .where('status', 1)
            .where('date_start' ,  moment().format('YYYY-MM-DD'))
            .select('match_id', 
            'timestamp_start',
            'is_non_player_notification_send')
    
// console.log('get_matches',get_matches)

          if(get_matches.length === 0){

            return
        }

      
        for (let index = 0; index < get_matches.length; index++) {
            const element = get_matches[index];

         //   console.log('create_group-------------------')
          //  const create_group = await seperate_Groups()

            const current_time = new Date()
         // console.log(current_time)    
            
            var myStart = element.timestamp_start
            var myEnd = current_time

        
          
          diff = getTimeDiff(myStart, myEnd)

          console.log(`${diff.hours()} Hour ${diff.minutes()} minutes`);

          if((diff.hours() === 0 && diff.minutes() <= 30 )){

           
                    const match_id = element.match_id

                    const get_playing11_player = await knex('public.team_squad')
                                                        .where('match_id' , match_id)
                                                        .where('playing11' , true)
                                                        .select('*')

                                    // console.log('get_playing11_player' , get_playing11_player.length)    
                                     

                    if(get_playing11_player.length != 0){
    
                    const player_id = await player_details(match_id)
    
                    for (const username of player_id) {

                        if(element.is_non_player_notification_send !== true){
    
                        const payload = {
                            notification: {
                                title: "Action Required",
                                body: `${username.first_name} ${username.last_name}  is not included in playing 11. You have 5 mins to replace ${username.first_name} ${username.last_name} with another player from the playing 11 list. If you don’t replace the player your current team formation will be considered for the contest.`
                                
                            },
    
                        }
    
                        const send_notification = await send_notification_with_notification(username.username , payload,1,1)
    
                      //  console.log('send_notification')
                        const non_player_notification_send = await knex('public.match')
                                                                .where('match_id' , match_id)
                                                                .update('is_non_player_notification_send', true)
                                                                .returning('is_non_player_notification_send')
                                                                
                        
                    }
                }
                } 
                       

          }
          
      //  console.log(`${diff.hours()} Hour ${diff.minutes()} minutes`);
            

       
    }

    } catch (err) {

        console.log(`${new Date()} /api/change_match_status_to_live_scheduler -> ${err}`)

    
    }





}




const getTimeDiff =  (start, end) => {

    return moment.duration(moment(start).diff(moment(end)));
  }




router.get('/api/change_match_status_to_live_scheduler', change_match_status_to_live_scheduler);

module.exports = router;