const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const count_total_fantasy_point = require('./count_total_fantasy_point');

const rp = require('request-promise-native');

const token = process.env.TOKEN;

var CronJob = require('cron').CronJob;


var job = new CronJob('*/2 * * * *', async () => {

   await get_player_stats_for_current_match()

    console.log(new Date(), 'fantasy point scheduler-You will see this message every 2 minutes');


}, null, true, 'America/Los_Angeles');
job.start();




const get_player_stats_for_current_match = async () => {

    // console.log(`req params ${JSON.stringify(req.query)}`

    const get_match = await knex('public.match')
        .where('status', 3)
        .select('match_id')

    console.log(get_match);

    for (let index = 0; index < get_match.length; index++) {
        const { match_id } = get_match[index];

        const options = {
            uri: `https://rest.entitysport.com/v2/matches/${match_id}/point`,
            qs: {

                token: token,
            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        }


        rp(options).then(async (repos) => {

            //   console.log(repos.response)
            await save_details_in_db(repos.response, match_id);

            console.log('function calles');

            await count_total_fantasy_point()

            return;
            //  res
            //     .status(200)
            //     .send({
            //         status: 'SUCCESS',
            //         response: repos.response
            //     });

        })
            .catch(function (err) {

                console.error(err.message); // API call failed...
                return
                // res
                //     .status(500)
                //     .send({ status: 'FAILURE' });

            });


    }
}

const save_details_in_db = async (data, match_id) => {



    try {

        const teama_id = data.teama.team_id;

        const teama_player_points = data.points.teama.playing11;

        //  console.log(teama_player_points)

        const teamb_id = data.teamb.team_id;

        const teamb_player_points = data.points.teamb.playing11;


        const teama_player_substitute_points = data.points.teama.substitute

        const teamb_player_substitute_points = data.points.teamb.substitute
        if (teama_player_points !== undefined) {

            for (let index = 0; index < teama_player_points.length; index++) {
                const element = teama_player_points[index];



                const teama_obj = {
                    "player_id": element.pid,
                    "team_id": teama_id,
                    "match_id": match_id,
                    "fantasy_points": element.point,
                    "created_on": moment(),
                    "modified_on": moment(),
                }

                const players_id = teama_obj.player_id
                //   console.log(players_id)

                // console.log(teama_obj)

                const get_teama_players_by_match_id_player_id = await knex('public.player_stat')
                    .where('match_id', match_id)
                    .where('player_id', players_id)
                    .select('*');



                if (get_teama_players_by_match_id_player_id.length === 0) {

                    const add_teama_details_in_db = await knex('public.player_stat')
                        .insert(teama_obj)
                        .returning('*');

                    //  console.log('------------')
                } else {

                    const replace_detalis = await knex('public.player_stat')
                        .where('match_id', match_id)
                        .where('player_id', players_id)
                        .update(teama_obj)
                        .returning('*')

                    // console.log('teama inside else')

                    // console.log(replace_detalis)
                }

            }
        }

        if (teama_player_substitute_points !== undefined) {
            for (let index = 0; index < teama_player_substitute_points.length; index++) {
                const element = teama_player_substitute_points[index];



                const teama_obj = {
                    "player_id": element.pid,
                    "team_id": teama_id,
                    "match_id": match_id,
                    "fantasy_points": element.point,
                    "created_on": moment(),
                    "modified_on": moment(),
                }

                const players_id = teama_obj.player_id
                //   console.log(players_id)

                // console.log(teama_obj)

                const get_teama_players_by_match_id_player_id = await knex('public.player_stat')
                    .where('match_id', match_id)
                    .where('player_id', players_id)
                    .select('*');



                if (get_teama_players_by_match_id_player_id.length === 0) {

                    const add_teama_details_in_db = await knex('public.player_stat')
                        .insert(teama_obj)
                        .returning('*');

                    //  console.log('------------')
                } else {

                    const replace_detalis = await knex('public.player_stat')
                        .where('match_id', match_id)
                        .where('player_id', players_id)
                        .update(teama_obj)
                        .returning('*')

                    // console.log('teama inside else')

                    // console.log(replace_detalis)
                }

            }
        }


        if (teamb_player_points !== undefined) {
            for (let index = 0; index < teamb_player_points.length; index++) {
                const element = teamb_player_points[index];




                const teamb_obj = {
                    "player_id": element.pid,
                    "team_id": teamb_id,
                    "match_id": match_id,
                    "fantasy_points": element.point,
                    "created_on": moment(),
                    "modified_on": moment(),
                }



                const get_teamb_players_by_match_id_player_id = await knex('public.player_stat')
                    .where('player_id', element.pid)
                    .where('match_id', match_id)
                    .select('*');

                if (get_teamb_players_by_match_id_player_id.length === 0) {
                    const add_details_in_db = await knex('public.player_stat')
                        .insert(teamb_obj)
                        .returning('*');

                    //  console.log('--------')
                } else {

                    const replace_detalis = await knex('public.player_stat')
                        .where('player_id', element.pid)
                        .where('match_id', match_id)
                        .update(teamb_obj)
                        .returning('*')

                    //  console.log('teamb inside else')
                }


            }

        }

        if (teamb_player_substitute_points !== undefined) {
            for (let index = 0; index < teamb_player_substitute_points.length; index++) {
                const element = teamb_player_substitute_points[index];


                const teamb_obj = {
                    "player_id": element.pid,
                    "team_id": teamb_id,
                    "match_id": match_id,
                    "fantasy_points": element.point,
                    "created_on": moment(),
                    "modified_on": moment(),
                }



                const get_teamb_players_by_match_id_player_id = await knex('public.player_stat')
                    .where('player_id', element.pid)
                    .where('match_id', match_id)
                    .select('*');

                if (get_teamb_players_by_match_id_player_id.length === 0) {
                    const add_details_in_db = await knex('public.player_stat')
                        .insert(teamb_obj)
                        .returning('*');

                    // console.log('--------')
                } else {

                    const replace_detalis = await knex('public.player_stat')
                        .where('player_id', element.pid)
                        .where('match_id', match_id)
                        .update(teamb_obj)
                        .returning('*')

                    //  console.log('teamb inside else')
                }


            }
        }


    } catch (err) {

        console.log(`${new Date()},/api/get_player_stats_for_current_match -> ${err}`)

    }

}



router.get('/api/get_player_stats_for_current_match', get_player_stats_for_current_match);

module.exports = router;