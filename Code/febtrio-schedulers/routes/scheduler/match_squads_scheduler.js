const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const rp = require('request-promise-native');
const token =  process.env.TOKEN;

var CronJob = require('cron').CronJob;
var job = new CronJob('*/5 * * * *', async () => {

    await fetch_match_squads_data()

    console.log(new Date() ,'match squads scheduler - You will see this message every 5 minutes');


}, null, true, 'America/New_York');
job.start();





const fetch_match_squads_data = async () => {

    const get_match  = await knex('public.match')
                .where('status' , 1)
                .select('match_id')
            
              //  console.log(get_match)

        for (let index = 0; index < get_match.length; index++) {
            const { match_id }= get_match[index];
               

    const options = {
        uri: `https://rest.entitysport.com/v2/matches/${match_id}/squads`,
        qs: {

            token: token,
        },
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    }


    rp(options).then(async (repos) => {


        await save_details_in_db(repos.response, match_id);

        return
        //  res
        //     .status(200)
        //     .send({
        //         status: 'SUCCESS',
        //         response: repos.response
        //     });

    })
        .catch(function (err) {

            console.error(err.message); // API call failed...
            return
            //  res
            //     .status(500)
            //     .send({ status: 'FAILURE' });

        });

}
}

const save_details_in_db = async (data, match_id) => {


    try {



        const teama_sqaud = data.teama.squads;

        const teamb_sqaud = data.teamb.squads;

        const players = data.players;



        for (let index = 0; index < teama_sqaud.length; index++) {
            const element = teama_sqaud[index];




            const teama_sqaud_obj = {
                "player_id": element.player_id,
                "role": element.role,
                "role_str": element.role_str,
                "playing11": element.playing11,
                "team_id": data.teama.team_id,
                "match_id": match_id,
                "created_on": moment(),
            }
            //
            // save team a detail 
            //
            // console.log(teama_sqaud_obj)

            const players_id = teama_sqaud_obj.player_id
            //    console.log(players_id)

            const get_teama_by_match_id = await knex('public.team_squad')
                .where('match_id', match_id)
                .where('player_id', players_id)
                .select('*');



            if (get_teama_by_match_id.length === 0) {

                const add_details_in_db = await knex('public.team_squad')
                    .insert(teama_sqaud_obj)
                    .returning('*');
               // console.log('----------------')

            } else {

                const replace_teama_sqaud_obj_detalis = await knex('public.team_squad')
                    .where('match_id', match_id)
                    .where('player_id', players_id)
                    .update(teama_sqaud_obj)
                    .returning('*')
               // console.log('inside else')
            }



        }


        for (let index = 0; index < teamb_sqaud.length; index++) {
            const element = teamb_sqaud[index];


            const teamb_sqaud_obj = {
                "player_id": element.player_id,
                "role": element.role,
                "role_str": element.role_str,
                "playing11": element.playing11,
                "team_id": data.teamb.team_id,
                "match_id": match_id,
                "created_on": moment(),
            }
            //
            //  save teamb detalis
            //


            const get_teamb_by_match_id = await knex('public.team_squad')
                .where('player_id', element.player_id)
                .where('match_id', match_id)
                .select('*');

            if (get_teamb_by_match_id.length === 0) {
                const add_details_in_db = await knex('public.team_squad')
                    .insert(teamb_sqaud_obj)
                    .returning('*');
            } else {

                const replace_teama_sqaud_obj_detalis = await knex('public.team_squad')
                    .where('match_id', match_id)
                    .where('player_id', element.player_id)
                    .update(teamb_sqaud_obj)
                    .returning('*')
            }

        }

        for (let index = 0; index < players.length; index++) {
            const element = players[index];

            const players_obj = {

                "player_id": element.pid,
                "title": element.title,
                "short_name": element.short_name,
                "first_name": element.first_name,
                "last_name": element.last_name,
                "middle_name": element.middle_name,
                "birthdate": element.birthdate !== "0000-00-00"?element.birthdate:moment().format("YYYY-MM-DD"),
                "birthplace": element.birthplace,
                "country": element.country,
                // "primary_team": [],
               //"thumb_url": element.thumb_url,
               // "logo_url": element.logo_url,
                "playing_role": element.playing_role,
                "batting_style": element.batting_style,
                "bowling_style": element.bowling_style,
                "fielding_position": element.fielding_position,
                "recent_match": element.recent_match,
                "recent_appearance": element.recent_appearance,
                "fantasy_player_rating": element.fantasy_player_rating,
                // "nationality": "Sri Lanka"
            }
            // console.log(players_obj.player_id)
            //
            // save players details

            const get_player = await knex('public.player')
                .where('player_id', players_obj.player_id)
                .select('*')


            if (get_player.length === 0) {

                const match = await knex('public.player')
                    .insert(players_obj)
                    .returning('*')

            } else {

                const replace_players_data = await knex('public.player')
                    .where('player_id', players_obj.player_id)
                    .update(players_obj)
                    .returning('*')

            }



        }

    } catch (err) {
        
        console.log(`${new Date()},/api/fetch_match_squads_data -> ${err}`)
      

}

}
router.get('/api/fetch_match_squads_data', fetch_match_squads_data);

module.exports = router;