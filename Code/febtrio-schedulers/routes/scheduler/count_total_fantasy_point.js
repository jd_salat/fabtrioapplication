const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone')
const rp = require('request-promise-native');
const token = process.env.TOKEN;

var CronJob = require('cron').CronJob;

var job = new CronJob('*/5 * * * *', async () => {

    await count_total_fantasy_point()

    console.log(new Date(), 'count_total_fantasy_point -You will see this message every 5 minutes');


}, null, true, 'America/Los_Angeles');
job.start();



const count_total_fantasy_point = async () => {

    try {


        //  console.log('here');

        const get_user_contest_details = await knex('public.user_contest_group as ucg')
            .leftJoin('public.match as m', 'm.match_id', 'ucg.match_id')
            .select(
                'ucg.user_contest_id',
                'ucg.username',
                'ucg.user_team_id',
                'ucg.match_id'
            )
            .where('m.status', 3)

        //console.log(get_user_contest_details)



        for (let index = 0; index < get_user_contest_details.length; index++) {
            const element = get_user_contest_details[index];

            const player_details = await knex('public.user_team as ut')
                .leftJoin('public.player_stat as ps', 'ps.player_id', 'ut.player_id')
                .where('ut.user_team_id', element.user_team_id)
                .where('ps.match_id', element.match_id)
                .select('ps.fantasy_points', 'ut.is_star_player')

            // console.log('players_details' , player_details)

            var total_points = 0;

            //  console.log(player_details);

            for (const player of player_details) {

                if (player.is_star_player === 1) {

                    total_points = total_points + (2 * parseFloat(player.fantasy_points));


                }
                else {

                    total_points = total_points + parseFloat(player.fantasy_points);


                }
               // console.log(`user_team_id -> ${element.user_team_id} fantasy_points -> ${player.fantasy_points} player.is_star_player -> ${player.is_star_player} is_star_player_total_points -> ${total_points}`)

            }

            // console.log(total_points)

            const update_total_points = await knex('public.user_contest_group')
                .where('user_contest_id', element.user_contest_id)
                .update('total_points', total_points)
                .update('modified_on', moment())
                .returning('*')


            //  console.log(update_total_points);


        }




    }
    catch (err) {

        console.log(`${new Date()} ,/api/count_total_fantasy_point_sheculer ->${err}`)

    }

}

router.get('/api/count_total_fantasy_point', count_total_fantasy_point);

module.exports = count_total_fantasy_point;