const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { winner_calculation } = require('../scheduler/winner_calculation');
var CronJob = require('cron').CronJob;




var job = new CronJob('*/30 * * * *', async () => {


    await award_distribution()

    console.log(new Date(), 'award distribution - You will see this message every 30 minutes');


}, null, true, 'America/Los_Angeles');

job.start();


const award_distribution = async () => {

    try {



        const  get_match_id = await knex('public.match')
            .where('is_award_distributed', false)
            .where('status', 5)
            .select('match_id')

       console.log(' awars distribution match', get_match_id)
        if (get_match_id.length > 0 ) {

            for (const match of get_match_id) {

                const find_mom = await count_mom_player_point(match.match_id)
                
            }

            
        }

    } catch (err) {

        console.log( `${new Date()} , award distribution -> ${err}`)

    }

}

const count_mom_player_point = async (match_id) => {


    const get_mom_player = await knex('public.match_stat')
        .where('match_id', match_id)
        .select('mom_player_id')

    //  console.log('get_mom_player', get_mom_player)

    if (get_mom_player.length && get_mom_player[0].mom_player_id !== null) {


        const get_man_of_the_match_in_trump_card = await knex('public.user_trump_card as utc')
            .leftJoin('public.user_team as ut', 'ut.user_team_id', 'utc.user_team_id')
            .select(
                'utc.user_team_id')
            .where('utc.trump_card', 3)
            .where('ut.match_id', match_id)
            .distinct('utc.user_team_id')

        // console.log('get_man_of_the_match_in_trump_card', get_man_of_the_match_in_trump_card)

        const get_mom_point = await knex('public.player_stat')
            .where('player_id ', get_mom_player[0].mom_player_id)
            .where('match_id', match_id)
            .select('fantasy_points')


        //   console.log('get_mom_point -> ', get_mom_point)


        for (let index = 0; index < get_man_of_the_match_in_trump_card.length; index++) {
            const element = get_man_of_the_match_in_trump_card[index];


            const get_user_contest_group_mom_total_point = await knex('public.user_contest_group as ucg')
                .select(
                    'ucg.total_points',
                    'ucg.user_team_id',
                    'ucg.username',
                    'ucg.c_id',
                    'ucg.v_id'

                )
                .where('ucg.match_id', match_id)
                .where('ucg.user_team_id', element.user_team_id)




            // console.log('get_user_contest_group_mom_total_point', get_user_contest_group_mom_total_point)

            for (let index = 0; index < get_user_contest_group_mom_total_point.length; index++) {
                const element = get_user_contest_group_mom_total_point[index];

                const total_points = parseFloat(element.total_points)

                //  console.log('total_points' , total_points)

                const mom_points = (total_points + parseFloat(get_mom_point[0].fantasy_points * 2)).toFixed(2)

                //  console.log('mom_points' , mom_points)

                const update_user_contest_group_bowler_total_points = await knex('public.user_contest_group')
                    .update('total_points', mom_points)
                    .where('username', element.username)
                    .where('user_team_id', element.user_team_id)

                // console.log(`username->${element.username}  points->${mom_points}  team_id->${element.user_team_id} c_id -> ${element.c_id} v_id -> ${element.v_id}`)


                //  console.log('update_user_contest_group_bowler_total_points', update_user_contest_group_bowler_total_points)

            }


            const winner = await winner_calculation(match_id)


        }
    }
}

module.exports = router;