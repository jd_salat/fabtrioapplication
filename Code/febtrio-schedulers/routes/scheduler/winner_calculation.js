const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
const getUserRank = require('../../helper/get_rank');



const winner_calculation = async (match_id) => {

    console.log(new Date(), 'call winner_calculation')


    try {



        const get_group_id = await knex('public.user_contest_group')
            .where('match_id', match_id)
            .distinct('group_id')
            .select('c_id', 'v_id', 'group_id')


       console.log('get_group_id',get_group_id)


        for (let index = 0; index < get_group_id.length; index++) {
            const element = get_group_id[index];
          console.log('*************************************************************************************************************************')
            console.log('group_id', element.group_id);

            const get_participant_in_group = await knex('public.user_contest_group')
                .select('username', 'total_points', 'user_team_id')
                .where('match_id', match_id)
                .where('group_id', element.group_id)
                .orderBy('total_points', 'desc')

             console.log('get_participant_in_group points' ,get_participant_in_group)

            const user_group_array = [];

            for (const user of get_participant_in_group) {
                const rank = await getUserRank(get_participant_in_group, user.username, user.total_points);
                user.rank = rank;
                user_group_array.push(user)
            }

             console.log('user_group_array',user_group_array);

            user_group_array.sort(function compare(user1, user2) {
                let comparision = 0;
                if (user1.rank > user2.rank) {

                    //  console.log('inif count rank' , user1.rank > user2.rank)
                    comparision = 1
                } else {

                    //  console.log('inelse')
                    comparision = -1
                }
                return comparision;
            })




            const contest_details = await knex('public.contest')
                .where('c_id', element.c_id)
                .where('v_id', element.v_id)
                .select('contest_amount', 'commission')

             console.log('contest_details' , contest_details)



            const variant_details = await knex('public.contest_variance')
                .select('no_of_winners_per_group', 'reward_distribution')
                .where('v_id', element.v_id)

                console.log('variant_details' , variant_details)




            const group_total_amount = parseFloat(contest_details[0].contest_amount) * get_participant_in_group.length

         console.log('group_total_amount', group_total_amount)

            const winner_contest_payment_amount = parseFloat(contest_details[0].contest_amount) * parseFloat(variant_details[0].no_of_winners_per_group)

          console.log('winner_contest_payment_amount', winner_contest_payment_amount)

            const count_commission_total_amount = parseFloat((group_total_amount * parseFloat(contest_details[0].commission)) / 100).toFixed(2)


           console.log('count_commission_total_amount', count_commission_total_amount)

            const award_amount = parseFloat(group_total_amount - winner_contest_payment_amount - count_commission_total_amount).toFixed(2)

          console.log('award_amount', award_amount)

            const no_of_winners = parseFloat(variant_details[0].no_of_winners_per_group);

            console.log('no_of_winnrs', no_of_winners)


            const winner_array = [];
            const looser_array = [];


            for (let index = 0; index < user_group_array.length; index++) {
                const group_member = user_group_array[index];

                

                if (index < no_of_winners) {

                //    console.log('index' , index)

                    // const winner = { username: element.username }

                    // console.log('inif winner ', winner)

                    winner_array.push(group_member);

                } else {

                    // const looser = { username: element.username }

                    //console.log('inelse -looser', looser)

                    looser_array.push(group_member);
                }



            }

         // console.log('winner_array', winner_array.length);


            for (let index = 0; index < winner_array.length; index++) {
                const winner_user = winner_array[index];

              console.log('winneruser', winner_array.length)

                const reward_distribution = variant_details[0].reward_distribution;

                const winning_amount = parseFloat(award_amount * parseFloat(reward_distribution[index]) / 100).toFixed(2);

               console.log('winning_amount',winning_amount);

                const amount_to_update = parseFloat(parseFloat(contest_details[0].contest_amount) + parseFloat(winning_amount)).toFixed(2);

              console.log('amount_to_update', amount_to_update)

                const wallet_details = await knex('public.user_wallet')
                    .select('deposited_amount')
                    .where('username', winner_user.username)

                 console.log('wallet_details', wallet_details)


                if (wallet_details && wallet_details.length > 0) {

                    const final_deposited_amount = parseFloat(parseFloat(wallet_details[0].deposited_amount) + parseFloat(amount_to_update)).toFixed(2)

                // console.log('final_deposited_amount', final_deposited_amount)
                    const updated_wallet = await knex('public.user_wallet')
                        .where('username', winner_user.username)
                        .update('deposited_amount', final_deposited_amount)
                        .update('modified_on', moment())

                 console.log(`inside  if -> username->${winner_user.username} final_amount -> ${final_deposited_amount}`)    


                } else {

                    const user_wallet_obj = {
                        "username": winner_user.username,
                        "bonus_amount": 0,
                        "deposited_amount": amount_to_update,
                        "winning_amount": 0,
                    }
                
                    const add_user_wallet = await knex('public.user_wallet')
                        .insert(user_wallet_obj)

                 console.log(`inside  else -> username->${winner_user.username} final_amount -> ${amount_to_update}`)    
                }


                const winners_obj = {
                    group_id: element.group_id,
                    v_id: element.v_id,
                    c_id: element.c_id,
                    username: winner_user.username,
                    winning_amount: amount_to_update,
                    match_id: match_id,
                    total_points: get_participant_in_group[0].total_points

                }

                // console.log(winners_obj)
                
                const add_winners = await knex('public.winners')
                    .insert(winners_obj)


                
                const update_winning_amount = await knex('public.user_contest_group')
                    .where('group_id',element.group_id)
                    .where('match_id', match_id)
                    .where('username', winner_user.username)
                    .where('user_team_id', winner_user.user_team_id)
                    .update('winning_amount', amount_to_update)
                    .update('modified_on' , moment())

                    const get_match = await knex('public.match')
                    .where('match_id' , match_id)
                    .select('title','date_start')

                    console.log('get_match_for_transction_message' , get_match)

                const time_stamp = new Date().valueOf();
                const transaction_id = `FAB${time_stamp}`;


                const transaction_data = {
                    transaction_id: transaction_id,
                    transaction_type: 1,
                    transaction_amount: amount_to_update,
                    transaction_date: moment().format('YYYY-MM-DD'),
                    username: winner_user.username,
                    match_id: match_id,
                    created_on: moment().format(),
                    modified_on: moment().format(),
                    transaction_message: `fabtrio reward amount ${get_match[0].title} - ${moment(get_match[0].date_start).format('DD-MMM')}`,
                    transaction_status: 1
                }

                // console.log(' tarntable', transaction_data)
                const save_transaction_details_in_db = await knex('public.transaction')
                    .insert(transaction_data)
                    .returning('*')


                const txn_id = `FABMASTER${time_stamp}`;

                const prepare_data_status_1 = {

                    user_transaction_id: transaction_id,
                    username: winner_user.username,
                    transaction_id: txn_id,
                    amount: amount_to_update,
                    transaction_type: 2,
                    payment_date: moment().format('YYYY-MM-DD'),
                    match_id: match_id,
                    created_on: moment().format(),
                    modified_on: moment().format(),
                    payment_status: 1
                }
                //   console.log('mastrtrnx', prepare_data_status_1)
               
                const save_transaction_status_1_details_in_db = await knex('public.master_payment_details')
                    .insert(prepare_data_status_1)
                    .returning('*')


            }


        }

        const update_model = {
            status: 2,
            status_str: 'Completed',
            is_award_distributed: true


        }

        const update_match_status = await knex('public.match')
            .where('match_id', match_id)
            .update(update_model)
           
        console.log('updated match_status')


    }



    catch (err) {

        console.log( `${new Date()},/api/winner_calculation -> ${err}`)
    }

}



//router.get('/api/winner_calculation', winner_calculation);

module.exports = { winner_calculation };
