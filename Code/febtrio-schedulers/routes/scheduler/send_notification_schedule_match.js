const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');
var CronJob = require('cron').CronJob;
const { send_notification_with_topic } = require('./send_notification');


var job = new CronJob('*/10 * * * *', async () => {

    const get_matches = await knex('public.match')
        .where('status', 1)
        .where('date_start' ,  moment(). format('YYYY-MM-DD'))
        .select('match_id',
            'timestamp_start',
        )

    //  console.log(get_matches)

    if (get_matches.length === 0) {

        return
    }

    const current_time = new Date();



    for (let index = 0; index < get_matches.length; index++) {
        const { timestamp_start, match_id } = get_matches[index];


        var myStart = timestamp_start
        var myEnd = current_time



        diff = getTimeDiff(myStart, myEnd)

        if ((diff.hours() === 2 && diff.minutes() >= 30) || (diff.hours() === 0 && diff.minutes() >= 30 && diff.minutes() <= 35)) {
            
            await send_notification_scheduler_match(match_id, diff)

        }

       // console.log(`${diff.hours()} Hour ${diff.minutes()} minutes`);


    }

    // console.log(get_matches)


    console.log(new Date() ,'send_notification_scheduler_match - You will see this message every 10 minutes');


}, null, true, 'America/Los_Angeles');

job.start();


const getTimeDiff = (start, end) => {

    return moment.duration(moment(start).diff(moment(end)));
}

const send_notification_scheduler_match = async (match_id, diff) => {

    try {


     //   console.log('function called')
        const get_matches = await knex('public.match')
            .where('status', 1)
            .where('match_id ', match_id)
            .where('date_start' ,  moment(). format('YYYY-MM-DD'))
            .select('match_id',
                'timestamp_start',
                'title')

      //  console.log('get_matches', get_matches)
      //  console.log('here');

        // const current_time = moment()

        // console.log(current_time)

        if (get_matches.length === 0) {

            return
        }

        for (let index = 0; index < get_matches.length; index++) {
            const team_name = get_matches[index];




           // console.log('----------', team_name.title)


            const payload = {
                topic: "match_updates",
                notification: {
                    title: `${team_name.title}`,
                    body: `${team_name.title} is all set to fight it out in a much awaited battle which starts in ${diff.hours()} Hour ${diff.minutes()} Minutes. Create your fabtrio team and participate in one or multiple contest to stand a change to win your price money.`
                },


            }

            const send_notification = await send_notification_with_topic(payload)



        }



    } catch (err) {

        console.log(`${new Date() },/api/send_notification_scheduler_match -> ${err}`)

       
    }





}


router.get('/api/send_notification_scheduler_match', send_notification_scheduler_match);

module.exports = router;