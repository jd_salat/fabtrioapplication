const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex')
const CronJob = require('cron').CronJob;
const moment = require('moment');
const generateGroups = require('../../helper/generate_group');
const random_user = require('../../helper/create_random_user');
const _ = require('lodash')


var job = new CronJob('*/10 * * * *', async () => {


  await create_user_contest_group()

  console.log(new Date(), 'Create user Contest Group - You will see this message every 10 minutes');


}, null, true, 'America/Los_Angeles');

job.start();


const create_user_contest_group = async () => {

  try {

    const match_details = await knex('public.match')
      .where('status', 1)
      .where('is_group_generated', false)
      .select(
        'match_id',
        'date_start',
        'timestamp_start')
      .where('date_start ', moment().format('YYYY-MM-DD'));

    // console.log('match_details' ,match_details)

    for (let index = 0; index < match_details.length; index++) {


      const element = match_details[index];

      const current_time = new Date()
      // console.log(current_time)    

      var myStart = element.timestamp_start
      var myEnd = current_time



      diff = getTimeDiff(myStart, myEnd)

      //  console.log(`matchid -> ${element.match_id} -> ${diff.hours()} Hour ${diff.minutes()} minutes`);

      if ((diff.hours() === 0 && diff.minutes() <= 15)) {

        const update_match_status = await knex('public.match')
          .where('match_id', element.match_id)
          .update('status', 3)
          .update('status_str', 'Live')
          .returning('*')


        var groups = []

        const get_match_details = await knex('public.match')
          .where('status', 3)
          .where('is_group_generated', false)
          .select(
            'match_id',
            'date_start')
          .where('date_start ', moment().format('YYYY-MM-DD'));

        // console.log('live matches for which groups are not generated', get_match_details)

        for (let index = 0; index < get_match_details.length; index++) {
          const { match_id } = get_match_details[index];

          console.log('group generation start for match_id', match_id)

          const get_variant_and_contest_id = await knex('public.contest_variance as cs')
            .leftJoin('public.contest as c', 'c.v_id', 'cs.v_id')
            .select('cs.v_id', 'c.c_id', 'cs.max_participants_per_group')

          for (let index = 0; index < get_variant_and_contest_id.length; index++) {
            const variant_contest_id = get_variant_and_contest_id[index];

            // console.log('*********************************************************************************************************')

            // console.log(`v_id-> ${variant_contest_id.v_id} c_id->${variant_contest_id.c_id}`);


            const get_user_contest_details = await knex('public.user_contest')
              .where('match_id ', match_id)
              .where('payment_status', 1)
              .where('c_id', variant_contest_id.c_id)
              .where('v_id', variant_contest_id.v_id)
              .select(
                'user_team_id',
                'username',
                'user_contest_id')

            //  console.log('get_user_contest_details', get_user_contest_details)
            //    console.log(`total_contest -> ${get_user_contest_details.length}`)
            let all_user_array = [];

            for (const user_details of get_user_contest_details) {

              // console.log(user_details.user_team_id)

              const player_details = await knex('public.user_team as ut')
                .leftJoin('public.user_trump_card as utc', 'utc.user_team_id', 'ut.user_team_id')
                .select(
                  'ut.player_id',
                  'utc.trump_card',
                  'ut.username',
                  'ut.user_team_id',
                  // 'uc.user_contest_id',
                )
                .where('ut.user_team_id ', user_details.user_team_id)
                .where('ut.match_id', match_id)

              // console.log( 'player_details', player_details)
              player_details[0].user_contest_id = user_details.user_contest_id;
              player_details[1].user_contest_id = user_details.user_contest_id;
              player_details[2].user_contest_id = user_details.user_contest_id;

              all_user_array.push(player_details)

            }

            //  console.log('before_all_group_array', all_user_array.length)


            const random_user_array = await random_user(all_user_array, match_id, variant_contest_id.c_id, variant_contest_id.v_id)

            // console.log(random_user_array);

            if (random_user_array && random_user_array.length) {

              //   console.log('here');
              all_user_array.push(...random_user_array);

            }

            //console.log(all_user_array);
            //   console.log('after_all_group_array', all_user_array.length);

            all_user_array = _.shuffle(all_user_array)

            // console.log('after_all_group_shufful_array', all_user_array.length);


            const [{ last_value: get_last_groupId }] = await knex('public.user_contest_group_seq')
              .select('last_value')

            groups = []

            const total_entries = all_user_array.length

            const total_groups_to_create = Math.ceil(total_entries / variant_contest_id.max_participants_per_group)

            groups = await generateGroups(all_user_array, total_entries, total_groups_to_create, parseInt(variant_contest_id.max_participants_per_group), parseInt(get_last_groupId), groups)
            //  console.log( 'groups',groups)
            //   console.log('groups', groups.length)


            for (const gorup of groups) {

              //  console.log('gorup' , gorup)
              const g_id = gorup.group_id;
              const group_members = gorup.group;


              for (const member of group_members) {
                //console.log('member' , member)/


                const prepared_data = {

                  group_id: g_id,
                  match_id: match_id,
                  user_contest_id: member[0].user_contest_id,
                  v_id: variant_contest_id.v_id,
                  c_id: variant_contest_id.c_id,
                  username: member[0].username,
                  match_date: get_match_details[0].date_start,
                  user_team_id: member[0].user_team_id,
                }

                //console.log('prepared_data',prepared_data)

                const save_groups = await knex('public.user_contest_group').insert(prepared_data);

              }
            }

            if (groups.length > 0) {
              const updated_sequence = groups[groups.length - 1].group_id;

              //console.log('updated_sequence', updated_sequence)

              const update_sequence =
                await knex.schema.withSchema('public').raw(`ALTER SEQUENCE user_contest_group_seq RESTART WITH ${updated_sequence}`)

            }
          }

          const update_status = await knex('public.match')
            .update('is_group_generated', true)
            .where('match_id', match_id)
        }


      }

    }
  } catch (err) {

    console.log(`${new Date()} ,/api/create_user_contest_group -> ${err}`)
  }


}

const getTimeDiff = (start, end) => {

  return moment.duration(moment(start).diff(moment(end)));
}

router.get('/api/create_user_contest_group', create_user_contest_group);

module.exports = create_user_contest_group