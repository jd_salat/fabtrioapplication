const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const moment = require('moment-timezone');

const rp = require('request-promise-native');

const token =  process.env.PAYMENT_TOKEN;
const merchantKey = process.env.MERCHANTKEY;
var CronJob = require('cron').CronJob;


var job = new CronJob('*/60 * * * *', async () => {


            await payment_status_check()
        

    console.log(new Date(), 'payment status check scheduler-You will see this message every 2 minutes');


}, null, true, 'America/Los_Angeles');
job.start();



const payment_status_check = async () => {

    // console.log(`req params ${JSON.stringify(req.query)}`
    const get_txn = await knex('public.transaction')
        .where('transaction_status', 2)
        .where('transaction_id', 'like', `%FABPG%`)
        .select(
            'transaction_status',
            'transaction_id',
            'transaction_amount',
            'username', 'match_id','created_on')
            .where('created_on' >= now())

    console.log(get_txn);

    

    for (let index = 0; index < get_txn.length; index++) {
        const element = get_txn[index];


        const options = {
            uri: `https://test.payumoney.com/payment/payment/chkMerchantTxnStatus?merchantKey=${merchantKey}&merchantTransactionIds=${element.transaction_id}`,
            qs: {

                token: token,
            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        }


        rp(options).then(async (repos) => {

        //console.log(repos)

           // await check_trancation_status(repos, element);

            // console.log('function calles');



            return;
            //  res
            //     .status(200)
            //     .send({
            //         status: 'SUCCESS',
            //         response: repos.response
            //     });

        })
            .catch(function (err) {

                console.error(err.message); // API call failed...
                return
                // res
                //     .status(500)
                //     .send({ status: 'FAILURE' });

            });


    }
}

const check_trancation_status = async (data, element) => {

    try {


        const deposited_amount = await knex('public.user_wallet')
            .select('deposited_amount')
            .where('username', element.username)

        console.log('deposited', deposited_amount)

        if (data.status === -1) {

            const update_payment_status = await knex('public.transaction')
                .update('transaction_status', 1)
                .where('transaction_id', element.transaction_id)
                .where('username', element.username)
                .returning('*')

            if (deposited_amount.length > 0) {

                const final_amount = (parseFloat(deposited_amount[0].deposited_amount) + parseFloat(element.amount)).toFixed(2)

                console.log('final_amount', final_amount)

                const upadate_amount = await knex('public.user_wallet')
                    .update('deposited_amount', final_amount)
                    .where('username', element.username)
                    .returning('*')

                console.log('update_amount', upadate_amount)

            } else {

                const prepared_data = {
                    username: element.phone,
                    bonus_amount: 0,
                    deposited_amount: parseFloat(element.amount),
                    winning_amount: 0
                }

                const add_amount = await knex('public.user_wallet')
                    .insert(prepared_data)

            }


        } else {

            const update_payment_status_for_failed = await knex('public.transaction')
                .update('transaction_status', 3)
                .where('transaction_id', element.transaction_id)
                .where('username', element.username)
                .returning('*')



        }

    } catch (err) {


        console.log(`${new Date()},/api/payment_status_check -> ${err}`)

        


    }


}


const getTimeDiff =  (start, end) => {

    return moment.duration(moment(start).diff(moment(end)));
  }

router.get('/api/payment_status_check', payment_status_check);

module.exports = router;