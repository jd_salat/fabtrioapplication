const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const moment = require('moment-timezone');
const rp = require('request-promise-native');
var CronJob = require('cron').CronJob;

const token = process.env.TOKEN;

const { winner_calculation } = require('../scheduler/winner_calculation');
const { where } = require('../../helper/knex');


var job = new CronJob('*/3 * * * *', async () => {


    await get_match_info_for_live_match()

    console.log(new Date(), 'live match scheduler - You will see this message every 3 minutes');


}, null, true, 'America/Los_Angeles');

job.start();




const get_match_info_for_live_match = async () => {


    const live_matches = await knex('public.match')
        .where('status', 3)
        .select('match_id')

    // console.log('live_matches' ,live_matches)
    if (live_matches.length === 0) {

        return
    }

    //  console.log(live_matches);

    for (let index = 0; index < live_matches.length; index++) {
        const { match_id } = live_matches[index];

        const options = {
            uri: `https://rest.entitysport.com/v2/matches/${match_id}/scorecard`,
            qs: {

                token: token,

            },
            headers: {
                'User-Agent': 'Request-Promise'
            },
            json: true // Automatically parses the JSON string in the response
        }


        rp(options).then(async (repos) => {

            // console.log(repos.response)

            await save_details_in_db(repos.response, match_id);



            return;

        })
            .catch(function (err) {

                console.error(err.message); // API call failed...
                return

            });


    }






}


const save_details_in_db = async (data, match_id) => {

    // console.log(data)


    try {

        if (data.verified === 'true') {

            //console.log(typeof(data.verified))

            console.log(new Date(), 'match is verified', data.verified)

            await update_match_status_in_db(data, match_id);

          //  const winner = await winner_calculation(match_id)



        }


        const update_game_state_inning_number = await knex('public.match')
            .where('match_id', data.match_id)
            .update('game_state', data.game_state)
            .update('game_state_str', data.game_state_str)
            .update('inning_number', data.latest_inning_number)

        // console.log('inning_number' , data.latest_inning_number)

        // console.log('update_game_state_inning_number',update_game_state_inning_number)





        const match_state_model = {
            match_id: data.match_id,
            match_date: moment(data.date_start).format('YYYY-MM-DD'),
            teama_id: data.teama.team_id,
            teamb_id: data.teamb.team_id,
            result: data.result,
            winning_team_id: data.winning_team_id,
            competition_name: data.competition.title,
            teama_scores: data.teama.scores,
            teama_overs: data.teama.overs,
            teamb_scores: data.teamb.scores,
            teamb_overs: data.teamb.overs
        }

        //  console.log('match_state_model', match_state_model)


        const get_match_data_from_db = await knex('public.match_stat')
            .where('match_id', match_id)
            .select('*')

        //   (get_match_data_from_db.length)

        if (get_match_data_from_db.length === 0) {
            // console.log('-----if --')

            const add_details_in_db = await knex('public.match_stat')
                .where('match_id', match_id)
                .insert(match_state_model);

            return add_details_in_db

        } else {
            //   console.log('---in else')

            match_state_model.modified_on = moment();

            const update_details_in_db = await knex('public.match_stat')
                .where('match_id', match_id)
                .update(match_state_model);

            return update_details_in_db;
        }




    } catch (err) {

        console.log(err)

        const log = await log_error(err, `${new Date()},/api/get_match_info_for_live_match`)



    }

}


const update_match_status_in_db = async (data, match_id) => {

    //  console.log(data)

    const update_model = {
        'status': data.status,
        'status_str': data.status_str,
        'status_note': data.status_note,
        'verified': data.verified,

    }

    console.log('updatemodel', update_model)

    const update_match_status = await knex('public.match')
        .where('match_id', match_id)
        .update(update_model)

    //console.log('mom ->', data.man_of_the_match.pid)
    const mom = data.man_of_the_match.pid

    const find_man_of_the_match = await knex('public.match_stat')
        .update('mom_player_id ', mom)

    await find_batsmen_and_bowler_of_the_match(data, match_id);


}

const find_batsmen_and_bowler_of_the_match = async (data, match_id) => {



    const batsmens_with_higher_points = await knex('public.player_stat as ps')
        .leftJoin('public.player as p ', 'p.player_id ', 'ps.player_id')
        .select('p.player_id', 'ps.fantasy_points')
        .where('ps.match_id ', match_id)
        .where('p.playing_role', 'bat')
        .orderBy('ps.fantasy_points', 'desc')
        .limit(1)

    // console.log('batsmens_with_higher_points', batsmens_with_higher_points)

    const bowler_with_higher_points = await knex('public.player_stat as ps')
        .leftJoin('public.player as p ', 'p.player_id ', 'ps.player_id')
        .select('p.player_id', 'ps.fantasy_points')
        .where('ps.match_id ', match_id)
        .where('p.playing_role', 'bowl')
        .orderBy('ps.fantasy_points', 'desc')
        .limit(1)

    //console.log('bowler_with_higher_points', bowler_with_higher_points)


    const update_in_db = await knex('public.match_stat')
        .where('match_id', match_id)
        .update('bow_om_player_id', bowler_with_higher_points[0].player_id)
        .update('bat_om_player_id', batsmens_with_higher_points[0].player_id)

    //.returning('*')

    // console.log('-----------updated_in_db-----------' ,update_in_db)



    const update_total_point = find_total_points_mom_player(data, batsmens_with_higher_points, bowler_with_higher_points, match_id)



}

const find_total_points_mom_player = async (data, batsmens_with_higher_points, bowler_with_higher_points, match_id) => {


    // console.log('batsmens_with_higher_points', batsmens_with_higher_points)
    // console.log('bowler_with_higher_points', bowler_with_higher_points)

    const get_best_batsman_in_trump_card = await knex('public.user_trump_card as utc')
        .leftJoin('public.user_team as ut', 'ut.user_team_id', 'utc.user_team_id')
        .select(
            'utc.user_team_id')
        .where('utc.trump_card', 1)
        .where('ut.match_id', match_id)
        .distinct('utc.user_team_id')

   // console.log('get_best_batsman_in_trump_card', get_best_batsman_in_trump_card)





    for (let index = 0; index < get_best_batsman_in_trump_card.length; index++) {
        const element = get_best_batsman_in_trump_card[index];

        const get_user_contest_group_batsman_total_point = await knex('public.user_contest_group as ucg')
            .select(
                'ucg.total_points',
                'ucg.user_team_id',
                'ucg.username'
            )
            .where('ucg.match_id', match_id)
            .where('ucg.user_team_id', element.user_team_id)

        //  console.log('get_user_contest_group_batsman_total_point', get_user_contest_group_batsman_total_point)



        for (let index = 0; index < get_user_contest_group_batsman_total_point.length; index++) {
            const element = get_user_contest_group_batsman_total_point[index];

            const total_points = parseFloat(element.total_points)
            const bestmen_points = (total_points + parseFloat(batsmens_with_higher_points[0].fantasy_points * 2)).toFixed(2)

            //  console.log('batsmanpoins' , bestmen_points)

            const update_user_contest_group_batsman_total_points = await knex('public.user_contest_group')
                .update('total_points', bestmen_points)
                .where('username', element.username)
                .where('user_team_id', element.user_team_id)

            // console.log(`username->${element.username}  points->${bestmen_points}  team_id->${element.user_team_id}`)

            //   console.log('update_user_contest_group_batsman_total_points', update_user_contest_group_batsman_total_points)



        }
    }


    const get_best_bowler_in_trump_card = await knex('public.user_trump_card as utc')
        .leftJoin('public.user_team as ut', 'ut.user_team_id', 'utc.user_team_id')
        .select(
            'utc.user_team_id')
        .where('utc.trump_card', 2)
        .where('ut.match_id', match_id)
        .distinct('utc.user_team_id')



    // console.log('get_best_bowler_in_trump_card' , get_best_bowler_in_trump_card )

    for (let index = 0; index < get_best_bowler_in_trump_card.length; index++) {
        const element = get_best_bowler_in_trump_card[index];



        const get_user_contest_group_bowler_total_point = await knex('public.user_contest_group as ucg')
            .select(
                'ucg.total_points',
                'ucg.user_team_id',
                'ucg.username',

            )
            .where('ucg.match_id', match_id)
            .where('ucg.user_team_id', element.user_team_id)

        // console.log('get_user_contest_group_bowler_total_point', get_user_contest_group_bowler_total_point)

        for (let index = 0; index < get_user_contest_group_bowler_total_point.length; index++) {
            const element = get_user_contest_group_bowler_total_point[index];

            const total_points = parseFloat(element.total_points)

            const bowler_points = (total_points + parseFloat(bowler_with_higher_points[0].fantasy_points * 2)).toFixed(2)

            const update_user_contest_group_bowler_total_points = await knex('public.user_contest_group')
                .update('total_points', bowler_points)
                .where('username', element.username)
                .where('user_team_id', element.user_team_id)

            //  console.log(`username->${element.username}  points->${bowler_points}  team_id->${element.user_team_id}`)


            //  console.log('update_user_contest_group_bowler_total_points', update_user_contest_group_bowler_total_points)

        }


    }


    const get_man_of_the_match_in_trump_card = await knex('public.user_trump_card as utc')
        .leftJoin('public.user_team as ut', 'ut.user_team_id', 'utc.user_team_id')
        .select(
            'utc.user_team_id')
        .where('utc.trump_card', 3)
        .where('ut.match_id', match_id)
        .distinct('utc.user_team_id')

   // console.log('get_man_of_the_match_in_trump_card', get_man_of_the_match_in_trump_card)

    const get_mom_point = await knex('public.player_stat')
        .select('fantasy_points')
        .where('player_id ', data.man_of_the_match.pid)

  //  console.log('get_mom_point -> ', get_mom_point)


    for (let index = 0; index < get_man_of_the_match_in_trump_card.length; index++) {
        const element = get_man_of_the_match_in_trump_card[index];


        const get_user_contest_group_mom_total_point = await knex('public.user_contest_group as ucg')
            .select(
                'ucg.total_points',
                'ucg.user_team_id',
                'ucg.username',
                'ucg.c_id',
                'ucg.v_id'

            )
            .where('ucg.match_id', match_id)
            .where('ucg.user_team_id', element.user_team_id)




        // console.log('get_user_contest_group_bowler_total_point', get_user_contest_group_bowler_total_point)

        for (let index = 0; index < get_user_contest_group_mom_total_point.length; index++) {
            const element = get_user_contest_group_mom_total_point[index];

            const total_points = parseFloat(element.total_points)

            const mom_points = (total_points + parseFloat(get_mom_point[0].fantasy_points * 2)).toFixed(2)

            const update_user_contest_group_bowler_total_points = await knex('public.user_contest_group')
                .update('total_points', mom_points)
                .where('username', element.username)
                .where('user_team_id', element.user_team_id)

           // console.log(`username->${element.username}  points->${mom_points}  team_id->${element.user_team_id} c_id -> ${element.c_id} v_id -> ${element.v_id}`)


            //  console.log('update_user_contest_group_bowler_total_points', update_user_contest_group_bowler_total_points)

        }


    }

}

router.get('/api/get_match_info_for_live_match', get_match_info_for_live_match);

module.exports = router;