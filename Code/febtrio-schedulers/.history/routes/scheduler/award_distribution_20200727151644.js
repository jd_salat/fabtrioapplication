const express = require('express');
const router = express.Router();
const knex = require('../../helper/knex');
const { log_error } = require('../config/config')
const moment = require('moment-timezone');
const rp = require('request-promise-native');
var CronJob = require('cron').CronJob;

const token = process.env.TOKEN;

const { winner_calculation } = require('../scheduler/winner_calculation');
const { where } = require('../../helper/knex');


var job = new CronJob('*/3 * * * *', async () => {


    await award_distribution()

    console.log(new Date(), 'live match scheduler - You will see this message every 3 minutes');


}, null, true, 'America/Los_Angeles');

job.start();


const award_distribution = async () =>{

    get_match = await knex('public.match')
                    .where('is_award_distributed' , false)
                    .select('match_id')



}

module.exports = router;