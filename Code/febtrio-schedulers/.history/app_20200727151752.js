const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const loggers = require('morgan');
const cors = require('cors');
const debug = require('debug')('temp-generator:server');
const http = require('http');
const dotenv = require('dotenv').config();

const app = express();

app.use(loggers('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

const body_parser = require('body-parser');

//middleware
app.use(body_parser.json());

// routes
//



//
//cronjob fetchdata
// 
// app.use('/v1', require('./routes/scheduler/match_list_scheduler'));
// app.use('/v1', require('./routes/scheduler/match_squads_scheduler'));
// app.use('/v1', require('./routes/scheduler/change_live_match_status_scheduler'));
// app.use('/v1', require('./routes/scheduler/weekly_winner_scheduler'));
// app.use('/v1', require('./routes/scheduler/send_notification_schedule_match'))

// app.use('/v1', require('./routes/scheduler/live_match_scheduler'));
// app.use('/v1', require('./routes/scheduler/match_fantasy_point_scheduler'));
// app.use('/v1', require('./routes/scheduler/live_commentary_scheduler'))


// app.use('/v1' , require('./routes/scheduler/create_user_contest_group'));

// app.use('/v1', require('./routes/scheduler/count_total_fantasy_point'));

app.use('/v1', require('./routes/scheduler/award_distribution'));

// catch 404 and forward to error
// handler
app.use(function (req, res, next) {
    const err = new Error('Not found');
    err.status = 404;
    next(404);
});

// error handler
app.use(function (err, req, res, next) {

    console.error(err);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req
        .app
        .get('env') === 'development'
        ? err
        : {};

    // render the error page
    res
        .json({ message: err.message })
        .status(err.status || 500);

});

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    // console.log('server server')
    console.log('Listening on ' + bind);
}