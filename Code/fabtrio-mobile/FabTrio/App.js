/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import { Text, View } from "react-native";
import SplashScreen from "./src/screens/SplashScreen";
import LoginScreen from "./src/screens/LoginScreen";
import HomeScreen from "./src/screens/HomeScreen";
import RegistrationScreen from "./src/screens/RegistrationScreen";
import WelcomeScreen from "./src/screens/WelcomeScreen";
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LinearGradient from "react-native-linear-gradient";
import {
  createDrawerNavigator, DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import MatchCenterScreen from './src/screens/MatchCenterScreen';
import QuickLinkScreen from './src/screens/QuickLinkScreen';
import MatchDetailsLiveScreen from './src/screens/MatchDetailsLiveScreen';
import MatchDetailsScreen from './src/screens/MatchDetailsScreen';
import UserTeamSelectionScreen from './src/screens/UserTeamSelectionScreen';
import UserSelectedTeamScreen from './src/screens/UserSelectedTeamScreen';
import SelectContestScreen from './src/screens/SelectContestScreen';
import MyContestScreen from './src/screens/MyContestScreen';
import AccountScreen from './src/screens/AccountScreen';
import BottomBarComponent from './src/components/BottomBarComponent';
import NotificationScreen from './src/screens/NotificationScreen';
import ScreenString from './src/strings/en.json';
import { BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, WHITE_COLOR, PARTICIPANT_COUNTTEXT_COLOR, TAB_HIGHLITER_COLOR, REGISTRATION_SUB_TITLE_COLOR } from './src/utils/ColorConstants';
import { FRANKLIN_BOOK } from './src/utils/FontUtils';
import FontAwesome5 from 'react-native-vector-icons/MaterialCommunityIcons';
import messaging from '@react-native-firebase/messaging';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

async function registerAppWithFCM() {
  await messaging().registerForRemoteNotifications();
}

async function requestPermission() {
  const granted = messaging().requestPermission();
 
  if (granted) {
    console.log('User granted messaging permissions!');
  } else {
    console.log('User declined messaging permissions :(');
  }
}
function App() {
  registerAppWithFCM()
  requestPermission()
  const unsubscribe = messaging().onMessage(async remoteMessage => {
    console.log('FCM Message Data:', remoteMessage.data);
  });
  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage.data);
  });
  messaging().getToken().then(string=>{
    console.log("Token : "+string)
  }).catch(error=>{
    console.log("Error: "+error)
  })
  
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Splash" component={SplashScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Login" component={LoginScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Registration" component={RegistrationScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Welcome" component={WelcomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Dashboard" component={MyDrawer}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="MatchDetails" component={MatchDetailsScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="MatchDetailsLive" component={MatchDetailsLiveScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="TeamSelection" component={UserTeamSelectionScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="SelectedTeam" component={UserSelectedTeamScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="SelectContest" component={SelectContestScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="Notification" component={NotificationScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>

    </NavigationContainer>
  );
}

function MyTabNavigator() {
  return (
    <Tab.Navigator tabBar={props => <BottomBarComponent {...props} />}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Games" component={MatchCenterScreen} />
      <Tab.Screen name="Contests" component={MyContestScreen} />
      <Tab.Screen name="Account" component={AccountScreen} />
    </Tab.Navigator>
  );
}


function MyDrawer() {
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="Home" component={MyTabNavigator} />
    </Drawer.Navigator>
  );
}

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <View style={{ alignSelf: 'stretch', height: 150, marginTop: -10, backgroundColor: '#cdcdcd', alignItems: 'center', marginBottom: 30 }}>
        <LinearGradient
          style={{ height: 150, width: '100%', opacity: 1 }}
          start={{ x: 0, y: 0 }}
          end={{ x: 1.5, y: 0 }}
          location={[0, 0.9]}
          colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
        </LinearGradient>
        <View style={{ marginTop: -35, height: 70, width: 70, borderRadius: 35, backgroundColor: '#cdfcdf', justifyContent: 'center', alignItems: 'center' }}>
          <LinearGradient
            style={{ height: 70, width: 70, borderRadius: 35, justifyContent: 'center', alignItems: 'center', opacity: 1 }}

            colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
            <Text style={{ color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 20 }}>{"MJ"}</Text>
          </LinearGradient>

        </View>
      </View>

      <DrawerItem
        icon={({ focused, color, size }) => {
          return <FontAwesome5 name={'account-circle'}
            size={size}
            color={PARTICIPANT_COUNTTEXT_COLOR}
            opacity={1}
          />
        }}
        label={({ focused, color }) => { return <Text style={{ color, marginLeft:-20,fontFamily:FRANKLIN_BOOK  }}>{focused ? ScreenString.leftMenu.myProfile : ScreenString.leftMenu.myProfile}</Text> }}
        onPress={() => {props.navigation.navigate('Account')}} />
      <DrawerItem
        icon={({ focused, color, size }) => {
          return <FontAwesome5 name={'credit-card'}
            size={size}
            color={PARTICIPANT_COUNTTEXT_COLOR}
            opacity={1}
          />
        }}
        
        label={({ focused, color }) => { return <Text style={{ color, marginLeft:-20,fontFamily:FRANKLIN_BOOK  }}>{focused ? ScreenString.leftMenu.accountDetails : ScreenString.leftMenu.accountDetails}</Text> }} onPress={() => alert('Link to help')} />
      <DrawerItem 
      icon={({ focused, color, size }) => {
        return <FontAwesome5 name={'wallet'}
          size={size}
          color={PARTICIPANT_COUNTTEXT_COLOR}
          opacity={1}
        />
      }}
      label={({ focused, color }) => { return <Text style={{ color, marginLeft:-20, fontFamily:FRANKLIN_BOOK  }}>{focused ? ScreenString.leftMenu.accountBalance : ScreenString.leftMenu.accountBalance}</Text> }} onPress={() => alert('Link to help')} />
      <DrawerItem label={({ focused, color }) => { return <Text style={{ color, marginLeft:35, fontFamily:FRANKLIN_BOOK }}>{focused ? ScreenString.leftMenu.aboutFabTrio : ScreenString.leftMenu.aboutFabTrio}</Text> }} onPress={() => alert('Link to help')} />
      <DrawerItem label={({ focused, color }) => { return <Text style={{ color, marginLeft:35,fontFamily:FRANKLIN_BOOK  }}>{focused ? ScreenString.leftMenu.aboutFabTrio : ScreenString.leftMenu.terms}</Text> }} onPress={() => alert('Link to help')} />
      <DrawerItem label={({ focused, color }) => { return <Text style={{ color, marginLeft:35, fontFamily:FRANKLIN_BOOK  }}>{focused ? ScreenString.leftMenu.aboutFabTrio : ScreenString.leftMenu.privacy}</Text> }} onPress={() => alert('Link to help')} />

      <View style={{alignSelf:'stretch', alignItems:'center', marginTop:10}}>
        <Text style={{color:REGISTRATION_SUB_TITLE_COLOR, fontFamily:FRANKLIN_BOOK, fontSize:15}}>{ScreenString.leftMenu.version}</Text>
      </View>
    </DrawerContentScrollView>
  );
}
export default App;