export const FRANKLIN_DEMI = 'FranklinGothic-Demi';
export const FRANKLIN_MEDIUM = 'FranklinGothic-Medium';
export const FRANKLIN_BOOK = 'FranklinGothic-Book';
export const FRANKLIN_MEDIUM_COND = 'FranklinGothic-MediumCond';
export const UBUNTU_BOLD= 'Ubuntu-Bold'
export const UBUNTU= 'Ubuntu'
