import format from 'date-fns/format'
export function isTokenValid(lastLoginTime) {
    let VALID_TOKEN_TIME = 3600 * 1000;
    if ((getCurrentTime() - lastLoginTime) >= VALID_TOKEN_TIME) {
        return false;
    } else {
        return true;
    }
}

export function getCurrentTime() {
    return Date.now();
}

export function getDateInFormat(dateString, dateFormat) {
    console.log(dateString)
    if (dateString !== undefined) {
        var date = new Date(Date.parse(dateString));
        return format(date, dateFormat);
    }
    return dateString
}

export function getCurrentYear() {
    return new Date().getFullYear();
}

export function getYearFilterData(startYear, endYear) {
    const year = []
    const diff = startYear - endYear;
    for (var i = 0; i <= diff; i++) {
        year[i] = startYear;
        startYear = startYear - 1
    }
    return year
}

export function isAgeValid(selectedDate) {
    const birthYear = new Date(Date.parse(selectedDate)).getFullYear();
    const currentYear = getCurrentYear()
    const age = currentYear - birthYear;
    return age >= 18 ? true : false
}
export function secondsToHms(futureTime) {
    const currentDate = new Date().getTime();

    const diff = (futureTime - currentDate) / 1000;
    var d = Number(diff);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? "h" : "h") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? "m" : "m") : "";
    var hours = hDisplay.length === 2 ? "0" + hDisplay : hDisplay
    var minutes = mDisplay.length === 2 ? "0" + mDisplay : mDisplay;

    return hours + " " + minutes;
}


export function getAccountDateFormat(date){
    return format(new Date(date), "dd MMM yyyy")
}