import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    USERNAME_STORAGE_KEY,
    ACCESS_TOKEN_STORAGE_KEY,
    LAST_LOGIN_TIME_STORAGE_KEY,
    REFRESH_TOKEN_STORAGE_KEY,
    IS_REGISTERED_STORAGE_KEY,
} from '../utils/Constants';

export function saveUsername(username) {
    AsyncStorage.setItem(USERNAME_STORAGE_KEY, username);
};
export async function fetchUsername() {
    return await AsyncStorage.getItem(USERNAME_STORAGE_KEY);
};
export function saveAccessToken(accessToken) {
    AsyncStorage.setItem(ACCESS_TOKEN_STORAGE_KEY, accessToken);
};
export async function fetchAccessToken() {
    return await AsyncStorage.getItem(ACCESS_TOKEN_STORAGE_KEY);
};
export function saveRefreshToken(refreshToken) {
    AsyncStorage.setItem(REFRESH_TOKEN_STORAGE_KEY, refreshToken);
};
export async function fetchRefreshToken() {
    return await AsyncStorage.getItem(REFRESH_TOKEN_STORAGE_KEY);
};
export function saveLastLoginTime(lastLoginTime) {
    AsyncStorage.setItem(LAST_LOGIN_TIME_STORAGE_KEY, JSON.stringify(lastLoginTime));
};
export async function fetchLastLoginTime() {
    return await AsyncStorage.getItem(LAST_LOGIN_TIME_STORAGE_KEY);
};
export function saveIsRegistered(flag) {
    AsyncStorage.setItem(IS_REGISTERED_STORAGE_KEY, JSON.stringify(flag));
};
export async function fetchIsRegistered() {
    return await AsyncStorage.getItem(IS_REGISTERED_STORAGE_KEY);
};