
export function isEmpty(text) {
    if (text !== '') {
        return false;
    } else {
        return true;
    }
}

export function isValidAccountNumber(accountNumber) {
    if (isNaN(accountNumber)) {
        return false;
    } else {
        if (accountNumber.toString().length === 15) {
            return true;
        } else {
            return false;
        }
    }
}

export function isPasswordValid(password) {
    var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    if (!isEmpty(password)) {
        if (strongRegex.test(password)) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}