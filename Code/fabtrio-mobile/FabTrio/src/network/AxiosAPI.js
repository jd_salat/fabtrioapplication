import axios from 'axios';
import { BASE_URL } from './AppUri';
const APIClient = axios.create({
    baseURL: BASE_URL,
    headers: {
        Accept: 'application/json'
    },
    timeout: 10000
    });
export default APIClient;
