export const BASE_URL = "http://febtrio-env-1.mzkpryhiky.us-east-2.elasticbeanstalk.com/v1/api/";

export const GET_OTP = "otp/generate_otp";
export const VERIFY_OTP = "verify_otp"
export const GET_APP_VERSION = "application_version";
export const REFRESH_TOKEN = "oauth/access_token?grant_type=refresh_token&refresh_token=";
export const REGISTRATION = "registration";
export const GET_STATES = "get_state";
export const GET_WINNER_COUNT = "winner_count";
export const GET_MATCHES = "get_match";
export const GET_CLAIMS_SEARCH = "claim/search";
export const GET_CLAIM_DETAILS = "claim/show/";
export const CLAIM_ATTACHMENT = "claim/showAttachment/";
export const CONTACT_US = "contactUs/send";
export const REIMBURSEMENT_YEARS = "/attestation/reimbursementYears";
export const REIMBURSEMENT_DATES = "/attestation/reimbursementDatesByYear/";
export const REIMBURSEMENT_ADVISE = "attestation/reimbursementAdvice/";
export const ATTESTAION_ADVISE = "attestation/general/";
