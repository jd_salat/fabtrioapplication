import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList, ScrollView } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK, FRANKLIN_MEDIUM_COND } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, TAB_HIGHLITER_COLOR, TRANSACTION_ID_TITLE_COLOR, TRANSACTION_ID_VALUE_COLOR
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import Snackbar from 'react-native-snackbar';
import { secondsToHms, getAccountDateFormat } from '../utils/TimeUtils.js';
import AccountHistory from '../network/accountsummarymock.json';

class AccountScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            isBadgeVisible: false,
            accountSummary: []
        }
    }

    componentDidMount() {
        this.setState({
            accountSummary: AccountHistory.response
        })
    }

    renderItem = ({ item, index }) => {

        const transactionDate = getAccountDateFormat(item.transaction_date).split(" ")
        console.log(transactionDate)
        return (
            <View key={index} style={[styles.listItemContainerStyle, styles.shadow]}>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.2 }}>
                    <Image style={{ height: 40, width: 40 }}
                        resizeMode='contain'
                        source={{ uri: item.transaction_type_url }}
                    />
                </View>
                <Text style={styles.dateTextStyle}>
                    {transactionDate[0]}{"\n"}
                    <Text style={{ fontSize: 15 }}>{transactionDate[1]}
                        {"\n"}{transactionDate[2]}</Text>
                </Text>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.6, flexDirection: 'column' }}>
                    <Text style={{ color: REGISTRATION_SUB_TITLE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 15 }}>
                        <Text style={{ color: item.transaction_type ===1?  BUTTON_GRADIENT_START_COLOR:CONGRATS_COLOR, fontFamily: FRANKLIN_MEDIUM }}>{item.transaction_amount}</Text> {item.transaction_message}</Text>
                    <View style={{ flexDirection: 'row', marginTop: 10 }}>
                        <View style={styles.textContainerStyle}>
                            <Text style={styles.transactionTitleStyle}>{ScreenString.accountScreen.transactionId}</Text>
                            <Text style={styles.valueTextStyle}>{item.transaction_id}</Text>
                        </View>
                        <View style={[styles.textContainerStyle, { alignItems: 'center' }]}>
                            <Text style={styles.transactionTitleStyle}>{ScreenString.accountScreen.match}</Text>
                            <Text style={styles.valueTextStyle}>{item.match_short_name}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )

    }


    handleRightIconPress = () => { }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: -1, right: -10, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='stretch'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ flexDirection: 'row', height: 70, marginTop: 10 }}>
                        <Text style={[styles.titleTextStyle,]}>{ScreenString.accountScreen.title}</Text>
                        <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                            <FontAwesome5 name={'bell'}
                                size={20}
                                color={WHITE_COLOR}
                                opacity={1}
                                onPress={() => { this.handleRightIconPress(); }} />
                            {this.state.isBadgeVisible &&
                                <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                            }
                        </View>
                    </View>
                    <FlatList
                        style={{ margin: 19, marginTop: 0 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.accountSummary}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

export default AccountScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR

    },
    imageStyle: {
        width: '100%',
        height: '35%',
    },

    titleTextStyle: {
        fontSize: 18,
        fontFamily: FRANKLIN_BOOK,
        color: WHITE_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    listItemContainerStyle: {
        flexDirection: 'row',
        borderRadius: 4,
        backgroundColor: WHITE_COLOR,
        margin: 1,
        marginTop: 10,
        padding: 10
    },
    dateTextStyle: {
        flex: 0.2,
        textAlignVertical: 'center',
        fontSize: 25,
        fontFamily: FRANKLIN_MEDIUM,
        color: REGISTRATION_SUB_TITLE_COLOR,
        textAlign: 'center'
    },
    transactionTitleStyle: {
        color: TRANSACTION_ID_TITLE_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9
    },
    valueTextStyle: {
        color: TRANSACTION_ID_VALUE_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9
    },
    textContainerStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        flex: 1
    }

})