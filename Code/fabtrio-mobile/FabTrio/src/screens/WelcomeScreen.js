import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import { PRIMARY_COLOR, WELCOME_NAME_COLOR, LOGIN_INSTRUCTION_COLOR, WHITE_COLOR, LOGIN_MOBILE_NO_COLOR, BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR } from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import FloatingLabel from 'react-native-floating-labels';
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_WINNER_COUNT } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import ModalComponent from "../components/ModalComponent";
import Snackbar from 'react-native-snackbar';
import DateTimePicker from '@react-native-community/datetimepicker';
import { getDateInFormat, getCurrentTime, isAgeValid } from '../utils/TimeUtils.js';
import { FORMAT_DD_MM_YYYY } from '../utils/Constants.js'
import moment from 'moment';
import { isEmpty } from '../utils/TextInputUtils.js';

function WelcomeScreen({ route, navigation }) {
    const { firstName } = route.params
    const { lastName } = route.params
    const [isBadgeVisible, setIsBadgeVisible] = useState(true);
    const [winnerCount, setWinnerCount] = useState(0)
    const [loading, toggelLoading] = useState(true)

    useEffect(() => {
        APIClient({
            method: 'GET',
            url: GET_WINNER_COUNT
        }).then(response => {
            console.log(response.data);
            toggelLoading(false)
            const data = response.data.data;
            setWinnerCount(1000000000000)
        }).catch(error => {
            toggelLoading(false)
            setWinnerCount(100000)
            setTimeout(() => {
                Snackbar.show({
                    text: ScreenString.login.errorFetch,
                    duration: Snackbar.LENGTH_SHORT,
                });
            }, 1000);

        })
    })

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
            <Loader
                loading={loading} />
            <ImageBackground
                style={[styles.imgBackground, { justifyContent: 'flex-end' }]}
                resizeMode='cover'
                source={require('../../assets/images/ic_splash_background.png')} >
                <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', height: 70, marginTop: 20 }}>
                            <Text style={[styles.titleTextStyle,]}>{ScreenString.splash.title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { }} />
                                {isBadgeVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                                }
                            </View>
                        </View>

                    </View>
                    <View
                        style={styles.registationContainerStyle}>
                        <View style={[styles.registationContainerStyle, {
                            backgroundColor: WHITE_COLOR,
                            borderRadius: 4, flex: 0
                        }]}>
                            <Text style={styles.enterMobileNoTextStyle}>{ScreenString.welcome.hello}</Text>
                            <Text style={[styles.enterMobileNoTextStyle, { marginTop: 0, color: WELCOME_NAME_COLOR }]}>{firstName + " " + lastName}</Text>
                            <Text style={styles.congratsTextStyle}>{ScreenString.welcome.congrats}</Text>
                            <Text style={styles.instructionTextStyle}>{ScreenString.welcome.instruction_part_1} {winnerCount} {ScreenString.welcome.instruction_part_2}</Text>
                            <Text style={styles.subInstructionTextStyle}>{ScreenString.welcome.subInstruction}</Text>
                            <View style={{ marginLeft: 20, marginEnd: 20, marginTop: 17, flexDirection: 'row', alignSelf: 'stretch' }}>
                                <View style={{ alignItems: 'center' }}>
                                    <View style={styles.circleStyle}>
                                        <Text style={styles.circleTextStyle}>{1}</Text>
                                    </View>
                                    <Text style={styles.stepsTextStyle}>{ScreenString.welcome.select}</Text>
                                    <Text style={[styles.stepsTextStyle, { fontSize: 10, marginTop:0, fontWeight:'bold' }]}>{ScreenString.welcome.match}</Text>
                                </View>
                                <View style={{ alignItems: 'center', marginLeft: 20 }}>
                                    <View style={styles.circleStyle}>
                                        <Text style={styles.circleTextStyle}>{2}</Text>
                                    </View>
                                    <Text style={styles.stepsTextStyle}>{ScreenString.welcome.select}</Text>
                                    <Text style={[styles.stepsTextStyle, { fontSize: 10, marginTop:0, fontWeight:'bold' }]}>{ScreenString.welcome.team}</Text>
                                </View>
                                <View style={{ alignItems: 'center', marginLeft: 20 }}>
                                    <View style={styles.circleStyle}>
                                        <Text style={styles.circleTextStyle}>{3}</Text>
                                    </View>
                                    <Text style={styles.stepsTextStyle}>{ScreenString.welcome.select}</Text>
                                    <Text style={[styles.stepsTextStyle, { fontSize: 10, marginTop:0, fontWeight:'bold' }]}>{ScreenString.welcome.contest}</Text>
                                </View>
                                <View style={{ alignItems: 'center', marginLeft: 20 }}>
                                    <View style={[styles.circleStyle, { backgroundColor: CONGRATS_COLOR }]}>
                                        <Text style={styles.circleTextStyle}>{4}</Text>
                                    </View>
                                    <Text style={styles.stepsTextStyle}>{ScreenString.welcome.allSet}</Text>
                                    <Text style={[styles.stepsTextStyle, { fontSize: 10, marginTop:0, fontWeight:'bold' }]}>{ScreenString.welcome.play}</Text>
                                </View>
                            </View>
                            <LinearGradient
                                style={{ height: 40, borderRadius: 20, width: 120, margin: 20, opacity: 1 }}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1.5, y: 0 }}
                                location={[0, 0.9]}
                                colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                <TouchableOpacity activeOpacity={1}
                                    onPress={() => {
                                        navigation.replace('Dashboard');
                                    }}
                                    style={[styles.loginButtonStyle]}>
                                    <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{ScreenString.welcome.btnText}</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </ImageBackground>
        </SafeAreaView>
    )

}

export default WelcomeScreen
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgBackground: {
        width: '100%',
        height: '100%',

    },
    listContainer: {
        flex: 1,
        backgroundColor: 'transparent'

    },
    titleTextStyle: {
        fontSize: 36,
        fontFamily: FRANKLIN_DEMI,
        color: PRIMARY_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    subtitleTextStyle: {
        fontSize: 15,
        marginTop: 0,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_BOOK,
        color: LOGIN_INSTRUCTION_COLOR
    },
    registationContainerStyle: {
        justifyContent: 'center',
        flex: 99,
        alignSelf: 'stretch',
        margin: 10
    }, enterMobileNoTextStyle: {
        fontSize: 24,
        marginTop: 20,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_MEDIUM,
        color: LOGIN_MOBILE_NO_COLOR
    },
    editTextTitleStyle: {
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 14,
        color: REGISTRATION_SUB_TITLE_COLOR,
        marginStart: 20,
        marginEnd: 20,
        marginTop: 20,

    },

    labelInput: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontSize: 13,
        fontFamily: FRANKLIN_BOOK
    },
    formInput: {
        borderBottomWidth: 1,
        marginLeft: 20,
        marginRight: 20,
        borderColor: REGISTRATION_SUB_TITLE_COLOR,
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        fontFamily: FRANKLIN_BOOK
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: BLACK_COLOR,
        fontFamily: FRANKLIN_BOOK,
        borderColor: LOGIN_INSTRUCTION_COLOR,
        fontSize: 16,
    },

    underlineStyleHighLighted: {
        borderColor: LOGIN_INSTRUCTION_COLOR
    },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    pickerModalStyle: {
        height: 210,
        backgroundColor: WHITE_COLOR,
    },
    pickerBackStyle: {
        height: 50,
        borderRadius: 4,
        borderBottomColor: REGISTRATION_SUB_TITLE_COLOR,
        borderBottomWidth: 1,
        marginStart: 20,
        marginRight: 20,
        marginTop: 0,
        backgroundColor: 'transparent',
        flexDirection: 'row'
    },
    subjectTitleStyle: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 13,
    },
    pickerTitleStyle: {
        marginStart: 30,
        marginRight: 20,
        marginTop: 20,
    },
    congratsTextStyle: {
        marginStart: 20,
        marginEnd: 20,
        marginTop: 26,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 30, color: CONGRATS_COLOR
    },
    instructionTextStyle: {
        color: WELCOME_NAME_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 15,
        marginStart: 20,
        marginEnd: 20,
        marginTop: 17
    },
    subInstructionTextStyle: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK,
        marginEnd: 20,
        marginStart: 20,
        marginTop: 37
    },
    circleStyle: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: BUTTON_GRADIENT_START_COLOR,
        justifyContent: 'center',
        alignItems: 'center'
    },
    circleTextStyle: {
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 16,
        color: WHITE_COLOR
    },
    stepsTextStyle: {
        marginTop:5,
        color: REGISTRATION_SUB_TITLE_COLOR,
        fontSize: 8,
        fontFamily: FRANKLIN_BOOK
    }
})