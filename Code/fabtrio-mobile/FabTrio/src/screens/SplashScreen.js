import React, { Component } from 'react'
import { StyleSheet, View, SafeAreaView, Text, ImageBackground, Image, StatusBar, Linking, Platform } from "react-native";
import { fetchIsRegistered, fetchRefreshToken, saveLastLoginTime, saveAccessToken, saveRefreshToken, fetchLastLoginTime, } from '../utils/AsyncStorageUtils';
import SplashString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM } from '../utils/FontUtils';
import { PRIMARY_COLOR, WHITE_COLOR } from '../utils/ColorConstants';
import { GET_APP_VERSION } from '../network/AppUri';
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { isTokenValid, getCurrentTime } from '../utils/TimeUtils';
import { APP_VERSION } from '../utils/Constants';
import Snackbar from 'react-native-snackbar';
import Dialog from "react-native-dialog";

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            dialogVisible: false
        }

    }

    showDialog = () => {
        this.setState({ dialogVisible: true });
    };

    handleCancel = () => {
        this.setState({ dialogVisible: false });
    };

    handleUpdate = () => {
        this.setState({ dialogVisible: false });
        setTimeout(() => {
            Platform.OS === 'android' ?
                Linking.openURL(SplashString.splash.androidURL) :
                Linking.openURL(SplashString.splash.iOSURL)
        }, 100)
    };
    componentDidMount() {
        this.fetchApplicationVersion();
    }

    fetchApplicationVersion = () => {
        this.setState({ loading: true })
        APIClient({
            method: 'GET',
            url: GET_APP_VERSION
        }).then(response => {
            this.setState({
                loading: false
            })
            const data = response.data.data;
            console.log(data)
            if (data.app_version[0].version === APP_VERSION) {
                this.goToNextScreen();

            } else {
                this.showDialog()
            }
        }).catch(error => {
            this.setState({ loading: false });
            setTimeout(() => {
                this.showSnackbar(SplashString.login.errorFetch);
            }, 1000);

        })
    }

    renderDialog = () => {
        return (
            <View>
                <Dialog.Container visible={this.state.dialogVisible}>
                    <Dialog.Title>{SplashString.splash.updateTitle}</Dialog.Title>
                    <Dialog.Description>
                        {SplashString.splash.updateDescription}
                    </Dialog.Description>
                    <Dialog.Button label={SplashString.splash.update} onPress={this.handleUpdate} />
                </Dialog.Container>
            </View>
        )
    }

    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    goToNextScreen = async () => {
        const isRegisterd = await fetchIsRegistered() === "true" ? true : false
        if (isRegisterd) {
            if (isTokenValid(await fetchLastLoginTime())) {
                this.props.navigation.replace('Dashboard');
            } else {
                this.refreshToken()
            }

        } else {
           this.props.navigation.replace('Welcome', {
                firstName: "Javed",
                lastName: "Salat"
            });
          //  this.props.navigation.replace('Registration');
        }
    }
    refreshToken = async () => {
        this.setState({ loading: true })
        const refreshToken = await fetchRefreshToken();
        const subUrl = REFRESH_TOKEN + refreshToken;
        APIClient({
            method: 'POST',
            url: subUrl
        }).then(response => {
            const data = response.data;
            this.saveTokens(data.token, data.refresh_token);
            saveLastLoginTime(getCurrentTime());
            this.goToNextScreen();
            this.setState({ loading: false })
        }).catch(error => {
            this.setState({ loading: false })
            setTimeout(() => {
                this.showSnackbar(t('splash:errorAuthenticationFailed'));
            }, 1000);
        });
    }

    saveTokens = (accessToken, refreshToken) => {
        saveAccessToken(accessToken);
        saveRefreshToken(refreshToken);
        //   saveIsRegistered(true);
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground
                    style={[styles.imgBackground, { justifyContent: 'flex-end' }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_splash_background.png')}
                >
                    <ImageBackground
                        style={[styles.imgBackground, { height: '70%', opacity: 0.6, alignItems: 'center' }]}
                        resizeMode='cover'
                        source={require('../../assets/images/ic_back_image.png')}>
                        <Image
                            style={[styles.jourseyStyle]}
                            resizeMode='cover'
                            source={require('../../assets/images/ic_joursey_splash.png')}
                        />
                        <Text style={styles.titleTextStyle}>{SplashString.splash.title}</Text>
                        <Text style={styles.subtitleTextStyle}>{SplashString.splash.subtitle}</Text>
                        {
                            this.renderDialog()
                        }
                    </ImageBackground>

                </ImageBackground>
            </SafeAreaView>
        )
    }
}

export default SplashScreen
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgBackground: {
        width: '100%',
        height: '100%',

    },

    jourseyStyle: {
        width: 100,
        height: 115,
    },

    titleTextStyle: {
        fontSize: 36,
        margin: 20,
        marginBottom: 0,
        fontFamily: FRANKLIN_DEMI,
        color: PRIMARY_COLOR
    },
    subtitleTextStyle: {
        fontSize: 15,
        marginTop: 12,
        fontFamily: FRANKLIN_MEDIUM,
        color: WHITE_COLOR
    }
})