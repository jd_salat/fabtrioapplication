import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList, ScrollView } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, DIVIDER_BACKGROUND, WELCOME_NAME_COLOR
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import Snackbar from 'react-native-snackbar';
import ViewMoreText from 'react-native-view-more-text';


import VariantMockData from '../network/variantmock.json'

var title = "";
var match_id = 0;
class SelectContestScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            contestData: [],
            payableAmount: 0,
            selectedContest: []
        }
    }

    componentDidMount() {
        this.setState({
            contestData: VariantMockData.response
        });
    }
    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    renderViewMore(onPress) {
        return (
            <Text style={styles.viewMoreStyle} onPress={onPress}>{ScreenString.contestSelection.viewDetails}</Text>
        )
    }
    renderViewLess(onPress) {
        return (
            <Text style={styles.viewMoreStyle} onPress={onPress}>{ScreenString.contestSelection.showLess}</Text>
        )
    }

    renderVariant = (item, index) => {
        return (
            <View key={index} style={{ flexDirection: 'column', flex: 1 }}>
                <View style={styles.variantTitleContainerStyle}>
                    <View style={{ flex: 0.4 }}>
                        <View style={styles.titleDividerStyle} />
                    </View>
                    <Text style={styles.variantTitleStyle}>{item.variant_name}</Text>
                    <View style={{ flex: 0.4 }}>
                        <View style={styles.titleDividerStyle} />
                    </View>
                </View>
                <ScrollView horizontal={true} style={{ height: 80, marginTop: 20, marginStart: 1, marginEnd: 0, marginBottom: 0, flex: 0.8 }}>
                    {
                        item.contest.map((contest, i) => { return this.renderContest(contest, i, item.variant_id) })
                    }
                </ScrollView>
                <View>
                    <ViewMoreText
                        numberOfLines={1}
                        renderViewMore={this.renderViewMore}
                        renderViewLess={this.renderViewLess}
                        textStyle={styles.viewMoreContainerStyle}>
                        <Text>
                            {item.variant_description}
                        </Text>
                    </ViewMoreText>
                </View>
            </View>
        )
    }

    renderContest = (item, index, variant_id) => {
        const marginLeft = index === 0 ? 0 : 5;
        var borderColor = DIVIDER_BACKGROUND;
        var fontColor = REGISTRATION_SUB_TITLE_COLOR;

        this.state.selectedContest.map((selected, i) => {
            if (selected.variant_id === variant_id && selected.contest.contest_id === item.contest_id) {
                borderColor = LOGIN_MOBILE_NO_COLOR;
                fontColor = LOGIN_MOBILE_NO_COLOR;
            }
        })
        return (
            <TouchableOpacity key={index} style={[styles.contestCircleStyle, { borderColor: borderColor, marginLeft: marginLeft, }]}
                onPress={() => {
                    this.handleContestSelection(item, variant_id)
                }}
                activeOpacity={1}>
                <Text style={[styles.contestTitleStyle, { color: fontColor }]}>{ScreenString.contestSelection.rupee}</Text>
                <Text style={[styles.contestTitleStyle, { color: fontColor }]} >{item.contest_amount}</Text>
            </TouchableOpacity>
        )
    }

    handleContestSelection = (contest, variant_id) => {
        var amountToPay = this.state.payableAmount;
        if (this.state.selectedContest.length !== 0) {
            var isAlreadySelected = false
            var selectedIndex = -1
            for (var arrayIndex = 0; arrayIndex < this.state.selectedContest.length; arrayIndex++) {
                if (this.state.selectedContest[arrayIndex].variant_id === variant_id && this.state.selectedContest[arrayIndex].contest.contest_id === contest.contest_id) {
                    isAlreadySelected = true;
                    selectedIndex = arrayIndex;
                    break;
                } else {
                    isAlreadySelected = false
                    selectedIndex = -1
                }
            }
            if (isAlreadySelected === true) {
                amountToPay = amountToPay - contest.contest_amount;
                this.state.selectedContest.splice(selectedIndex, 1)
            } else {
                amountToPay = amountToPay + contest.contest_amount;
                this.state.selectedContest.push({ variant_id: variant_id, contest: contest })
            }

        } else {
            amountToPay = amountToPay + contest.contest_amount;
            this.state.selectedContest.push({ variant_id: variant_id, contest: contest })
        }

        this.setState({ payableAmount: amountToPay })
    }


    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }


    handlePaymentClick = () => {
        if (this.state.payableAmount !== 0) {

        } else {
            this.showSnackbar(ScreenString.contestSelection.errorContestSelection);
        }
    }
    render() {
        match_id = this.props.route.params.matchId;
        title = this.props.route.params.matchTitle;
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleRightIconPress(); }} />
                                {this.state.isDotVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                                }
                            </View>

                        </View>
                    </View>
                    <View style={[styles.containerStyle, styles.shadow]}>
                        <LinearGradient
                            style={{ height: 40, borderTopLeftRadius: 4, borderTopRightRadius: 4, opacity: 1 }}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1.5, y: 0 }}
                            location={[0, 0.9]}
                            colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                            <View style={styles.stepHeaderViewStyle}>
                                <Text style={styles.stepTitleStyle}>{ScreenString.contestSelection.step3Title}</Text>
                                <Text style={styles.continueStyle}
                                    onPress={() => { this.handlePaymentClick() }}
                                >{ScreenString.contestSelection.pay}  {this.state.payableAmount}</Text>
                            </View>
                        </LinearGradient>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={[styles.containerStyle, styles.shadow, { marginTop: 0, borderTopRightRadius: 0, borderTopLeftRadius: 0 }]}>
                            <Text style={styles.instructionTextStyle}>{ScreenString.contestSelection.instruction}</Text>
                            <ScrollView style={{ marginStart: 10, marginEnd: 10, marginTop: 15 }}>
                                {
                                    this.state.contestData.map(this.renderVariant)
                                }
                            </ScrollView>
                        </View>

                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default SelectContestScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,

    },
    imageStyle: {
        width: '100%',
        height: '40%',
    },
    stepTitleStyle: {
        flex: 1,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 15,
        paddingStart: 10,
        color: WHITE_COLOR
    },
    continueStyle: {
        flex: 0.5,
        paddingEnd: 10,
        height: 40,
        textAlignVertical: 'center',
        textAlign: 'right',
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 15,
        color: WHITE_COLOR
    },
    containerStyle: {
        marginTop: 10,
        marginEnd: 20,
        marginStart: 20,
        backgroundColor: WHITE_COLOR,
        borderRadius: 4
    },

    stepHeaderViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        borderTopEndRadius: 4,
        borderTopStartRadius: 4,
        height: 40
    },
    instructionTextStyle: {
        marginTop: 15,
        marginStart: 10,
        marginEnd: 10,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 12
    },
    variantTitleStyle: {
        fontSize: 12,
        fontFamily: FRANKLIN_MEDIUM,
        color: WELCOME_NAME_COLOR,
        textAlign: 'center', flex: 0.3,
        textAlignVertical: 'auto'
    },
    titleDividerStyle: {
        height: 1,
        alignSelf: "stretch",
        backgroundColor: DIVIDER_BACKGROUND,
        opacity: 0.5
    },
    variantTitleContainerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewMoreStyle: {
        marginTop: 10,
        marginBottom: 10,
        color: LOGIN_MOBILE_NO_COLOR,
        fontSize: 10,
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center'
    },
    viewMoreContainerStyle: {
        textAlign: 'center',
        fontFamily: FRANKLIN_BOOK,
        fontSize: 10,
        color: REGISTRATION_SUB_TITLE_COLOR,
        marginTop: 10
    },
    contestCircleStyle: {
        height: 60,
        width: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1
    },
    contestTitleStyle: { fontSize: 15, fontFamily: FRANKLIN_MEDIUM }



})