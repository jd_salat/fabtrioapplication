import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import {  FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import {PRIMARY_COLOR, WHITE_COLOR,REGISTRATION_SUB_TITLE_COLOR} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Loader from '../components/Loading';
import { getDateInFormat } from '../utils/TimeUtils.js';
import Database from "../utils/DatabaseUtils";

class NotificationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            isBadgeVisible: false,
            accountSummary: [],
            notificationData: []
        }

    }

    async componentDidMount() {
        const db = new Database()
        this.setState({
            loading: true
        })
        //  db.addNotification({
        //     notification_type: 1,
        //     notification_message: 'Some message regarding the errors that occured with respect to transactions etc.',
        //     notification_image_url: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1584274664847.png',
        //     notification_date: '2019-02-22'
        // }).then((result)=>{
        //     console.log(result)
        // }).catch(error=>{
        //     console.log(error)
        // })
        // const notification = getAllNotifications()
        // console.log(notification)
        db.listNotification().then((data) => {
            this.setState({
                notificationData: data,
                loading: false
            })
        }).catch(error => {
            console.log(error)
        })


    }

    renderItem = ({ item, index }) => {

        const transactionDate = getDateInFormat(item.notification_date, "dd MMM")

        return (
            <View key={index} style={[styles.listItemContainerStyle, styles.shadow]}>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.2 }}>
                    <Image style={{ height: 40, width: 40 }}
                        resizeMode='contain'
                        source={{ uri: item.notification_image_url }}
                    />
                </View>

                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 0.8, flexDirection: 'column' }}>
                    <Text style={{ textAlign: 'justify', color: REGISTRATION_SUB_TITLE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 15 }}>
                        {item.notification_message}</Text>

                    <View style={[styles.textContainerStyle, { alignItems: 'flex-end', justifyContent: 'flex-end', alignSelf: 'stretch' }]}>
                        <Text style={styles.valueTextStyle}>{transactionDate}</Text>
                    </View>
                </View>
            </View>
        )

    }


    handleLeftIconPress = () => { this.props.navigation.pop() }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: -1, right: -10, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='stretch'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{ScreenString.notificationScreen.title}</Text>
                        </View>
                    </View>
                    <FlatList
                        style={{ margin: 19, marginTop: 0, marginBottom: 5 }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.notificationData}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

            </SafeAreaView>
        )
    }
}

export default NotificationScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR

    },
    imageStyle: {
        width: '100%',
        height: '35%',
    },

    listItemContainerStyle: {
        flexDirection: 'row',
        borderRadius: 4,
        backgroundColor: WHITE_COLOR,
        margin: 1,
        marginTop: 10,
        padding: 10
    },
    valueTextStyle: {
        color: REGISTRATION_SUB_TITLE_COLOR,
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 10
    },
    textContainerStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-end',
        flex: 1
    }

})