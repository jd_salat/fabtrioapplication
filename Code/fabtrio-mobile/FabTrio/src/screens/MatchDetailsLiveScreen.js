import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList, ScrollView } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK, FRANKLIN_MEDIUM_COND } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, TAB_HIGHLITER_COLOR
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import FloatingLabel from 'react-native-floating-labels';
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import Snackbar from 'react-native-snackbar';
import DateTimePicker from '@react-native-community/datetimepicker';
import { getDateInFormat, getCurrentTime, isAgeValid } from '../utils/TimeUtils.js';
import { FORMAT_DD_MM_YYYY, SCHEDULE_MATCH_STATUS } from '../utils/Constants.js'
import moment from 'moment';
import { isEmpty } from '../utils/TextInputUtils.js';
import ProgressCircle from 'react-native-progress-circle';
import UserTeam from '../network/userteammock.json';
import GroupData from '../network/leaderboardmockdata.json'
var starImage= require('../../assets/images/ic_star.png');


var matchDetail = {}

class MatchDetailsLiveScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            teamDetails: [],
            myTeamVisible: true,
            leaderboardVisible: false,
            trumpCardData: {},
            totalPoints: 0,
            currentStanding: 0,
            groupParticipantData: [],
        }
    }

    componentDidMount() {
        this.setState({
            teamDetails: UserTeam.response.team_player_detail,
            trumpCardData: UserTeam.response.trump_card,
            totalPoints: UserTeam.response.total_points,
            currentStanding: UserTeam.response.current_standing,
            groupParticipantData: GroupData.response.group_participants
        })
    }

    renderUpcomingMatches = (item) => {
        console.log(item)
        return (
            <View style={[styles.shadow, { backgroundColor: WHITE_COLOR, borderRadius: 4, marginTop: 0, height: '30%', flex: 1 }]}
                activeOpacity={1}>
                <View style={{ flexDirection: 'row', padding: 10 }}>
                    <View style={[styles.columnViewStyle, { height: 80 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-start' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1583143603776.png' }} />
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center', height: 80 }}>

                        <Text style={styles.scoreTextStyle}>{item.teama.scores}</Text>
                        <Text style={[styles.matchTitleStyle, { textAlign: 'left', marginLeft: 10 }]}>{item.teama.overs} {ScreenString.liveMatchData.overs}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end', justifyContent: 'center', height: 80 }}>

                        <Text style={styles.scoreTextStyle}>{item.teamb.scores}</Text>
                        <Text style={[styles.matchTitleStyle, { marginLeft: 15 }]}>{item.teamb.overs} {ScreenString.liveMatchData.overs}</Text>
                    </View>
                    <View style={[styles.columnViewStyle, { height: 80 }]}>
                        <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1583143808026.png' }} />
                        </View>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', flex: 1, height: 50 }}>
                    <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd' }} />
                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', marginStart: 10, marginEnd: 10 }}>
                        <Text style={{ fontFamily: FRANKLIN_BOOK, fontSize: 12, textAlign: 'left', textAlignVertical: 'center' }}>{ScreenString.liveMatchData.timeline}</Text>
                        <ScrollView>
                        </ScrollView>
                    </View>
                </View>
            </View>
        )
    }

    getTimelineView = () => {

    }

    renderTeamView = () => {

        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>{

                    this.state.teamDetails.map((item, index) => {
                        var marginLeft = index === 0 ? 0 : 15
                        const isStarPlayer = item.is_start_player === 1?true:false
                        return (
                            <View key={index} style={{ flexDirection: 'column', marginLeft: marginLeft , alignItems:'center'}}>

                                <ProgressCircle
                                    percent={item.popularity}
                                    radius={40}
                                    borderWidth={2}
                                    color={CONGRATS_COLOR}
                                    shadowColor="#cdcdcd"
                                    bgColor="#fff">
                                    <Image style={[styles.teamImageStyle]}
                                        resizeMode='contain'
                                        source={{ uri: item.thumb_url }} />
                                </ProgressCircle>{isStarPlayer &&
                                <Image style={{ height: 20, width: 20, position: 'absolute', top: 65 }}
                                    resizeMode='contain'
                                    source={starImage} />}
                                <Text style={styles.playerNameStyle}>{item.Player_name}</Text>
                                <Text style={[styles.popularityTextStyle]}>{item.popularity}{"%"}</Text>
                                <Text style={styles.pointTextStyle}>{item.total_points} {ScreenString.liveMatchData.points}</Text>
                            </View >
                        )
                    })
                }
                </View >
                <Text
                    style={styles.trumpCardStyle}>
                    <Text style={{ color: REGISTRATION_SUB_TITLE_COLOR, fontFamily: FRANKLIN_BOOK }}>{ScreenString.liveMatchData.trumpCard}{":"} </Text>
                    {this.state.trumpCardData.trump_card_name}
                </Text>
                <Text style={styles.pointTextStyle}>{ScreenString.liveMatchData.trumpInstruction}</Text>
                <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd', marginTop: 18 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={styles.footerViewStyle}>
                        <Text style={styles.totalPointsStyle}>{ScreenString.liveMatchData.totalPoint}</Text>
                        <Text style={{ color: PARTICIPANT_COUNTTEXT_COLOR, fontFamily: FRANKLIN_DEMI, fontSize: 30, fontWeight: 'bold' }}>{this.state.totalPoints}</Text>
                    </View>
                    <View style={{ width: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd' }} />
                    <View style={styles.footerViewStyle}>
                        <Text style={styles.totalPointsStyle}>{ScreenString.liveMatchData.currentStanding}</Text>
                        <Text style={{ color: BUTTON_GRADIENT_START_COLOR, fontFamily: FRANKLIN_DEMI, fontSize: 30, fontWeight: 'bold' }}>{this.state.currentStanding}</Text>
                    </View>
                </View>
            </View>
        )
    }
    renderParticipnats = ({ item, index }) => {
        return (
            <View style={{ flexDirection: 'column', justifyContent: 'center', height: 50, alignSelf: 'stretch' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', height: 50, alignSelf: 'stretch' }}>
                    <Text style={[styles.listHeaderTitle, { flex: 0.3, fontSize: 9, textAlign: 'left', marginStart: 10 }]}>{item.participant_name}</Text>
                    <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'flex-start', flexDirection: 'row' }}>{
                        item.participant_team.map((url, index) => {
                            return (
                                <View style={{ marginLeft: 3 }}>
                                    <ProgressCircle
                                        percent={url.popularity}
                                        radius={15}
                                        borderWidth={1}
                                        color={CONGRATS_COLOR}
                                        shadowColor="#cdcdcd"
                                        bgColor="#fff">
                                        <Image style={{ height: 30, width: 30 }}
                                            resizeMode='contain'
                                            source={{ uri: url.thumb_url }} />
                                    </ProgressCircle>
                                </View>)

                        })
                    }
                    </View>
                    <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center', marginStart: 10 }}>
                        <Image style={{ height: 30, width: 30 }}
                            resizeMode='contain'
                            source={{ uri: item.trump_card_url }} />
                    </View>


                    <Text style={{ flex: 0.15, fontFamily: FRANKLIN_MEDIUM_COND, fontSize: 12, color: PARTICIPANT_COUNTTEXT_COLOR, textAlign: 'center', marginStart: 3 }}>{item.total_points}</Text>
                    <Text style={{ flex: 0.15, fontFamily: FRANKLIN_MEDIUM_COND, fontSize: 12, color: CONGRATS_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{(index + 1)}</Text>
                </View>
                <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd', marginTop: 0 }} />
            </View>
        );
    }
    renderLeaderBoard = () => {
        return (
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', height: 50, alignSelf: 'stretch' }}>
                    <Text style={[styles.listHeaderTitle, { flex: 0.3, textAlign: 'left', marginStart: 10, fontSize: 8 }]}>{ScreenString.liveMatchData.participantName}</Text>
                    <Text style={[styles.listHeaderTitle, { flex: 0.3 }]}>{ScreenString.liveMatchData.team}</Text>
                    <Text style={[styles.listHeaderTitle, { marginStart: 3 }]}>{ScreenString.liveMatchData.trumpCard}</Text>
                    <Text style={styles.listHeaderTitle}>{ScreenString.liveMatchData.points}</Text>
                    <Text style={styles.listHeaderTitle}>{ScreenString.liveMatchData.rank}</Text>
                </View>
                <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd', marginTop: 0 }} />
                <FlatList
                    style={{}}
                    showsVerticalScrollIndicator={false}
                    data={this.state.groupParticipantData}
                    renderItem={this.renderParticipnats}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }


    handleMyTeamPress = () => {
        this.setState({
            myTeamVisible: true,
            leaderboardVisible: false
        })
    }
    handleLeaderboardPress = () => {
        this.setState({
            myTeamVisible: false,
            leaderboardVisible: true
        })
    }
    render() {
        matchDetail = this.props.route.params.matchDetail
        const title = matchDetail.teama.short_name + " VS " + matchDetail.teamb.short_name;
        console.log(matchDetail.teama.short_name)

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleRightIconPress(); }} />
                                {this.state.isDotVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                                }
                            </View>

                        </View>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={{ margin: 20, marginBottom: 5 }}>{
                            this.renderUpcomingMatches(matchDetail)
                        }
                            <View style={[styles.mainContainerStyle, styles.shadow]}>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity style={styles.tabButtonStyle}
                                        activeOpacity={1}
                                        onPress={() => { this.handleMyTeamPress() }}>
                                        <Text style={[styles.tabTitleStyle, { fontWeight: this.state.myTeamVisible === true ? 'bold' : 'normal' }]}>{ScreenString.liveMatchData.myTeam}</Text>
                                        <View style={{ height: 2, backgroundColor: this.state.myTeamVisible === true ? LOGIN_MOBILE_NO_COLOR : TAB_HIGHLITER_COLOR, alignSelf: 'stretch' }} />
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.tabButtonStyle}
                                        activeOpacity={1}
                                        onPress={() => { this.handleLeaderboardPress() }}>
                                        <Text style={[styles.tabTitleStyle, { fontWeight: this.state.leaderboardVisible === true ? 'bold' : 'normal' }]}>{ScreenString.liveMatchData.leaderboard}</Text>
                                        <View style={{ height: 2, backgroundColor: this.state.leaderboardVisible === true ? LOGIN_MOBILE_NO_COLOR : TAB_HIGHLITER_COLOR, alignSelf: 'stretch' }} />
                                    </TouchableOpacity>
                                </View>
                                {this.state.myTeamVisible &&
                                    this.renderTeamView()
                                }
                                {this.state.leaderboardVisible &&
                                    this.renderLeaderBoard()
                                }
                            </View>

                        </View>
                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default MatchDetailsLiveScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,

    },
    listContainer: {
        flex: 1,
        backgroundColor: 'transparent'

    },
    imageStyle: {
        width: '100%',
        height: '35%',
    },
    teamImageStyle: {
        height: 80,
        width: 60,
    },
    headToHeadCountTextStyle: {
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        textAlign: 'center'
    },
    matchTitleStyle: {
        marginTop: 0,
        textAlign: 'center',

        alignSelf: 'stretch',
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    scoreTextStyle: {
        fontSize: 20,
        textAlign: 'center',
        color: BLACK_COLOR,

        fontFamily: FRANKLIN_DEMI
    },
    vsTextStyle: {
        color: VS_TITLE_COLOR,
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        marginTop: 15
    },
    timeLeftTextStyle: {
        color: TIME_LEFT_TITLE_COLOR,
        fontFamily: FRANKLIN_DEMI,
        fontSize: 12,
        marginTop: 5
    },
    columnViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    last5TitleStyle: {
        color: BLACK_COLOR,
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK,
        marginTop: 15
    },
    participationTitleTyle: { color: LOGIN_MOBILE_NO_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 15, marginTop: 20, textAlign: 'center', alignSelf: 'stretch' },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    mainContainerStyle: {
        flex: 1,
        backgroundColor: WHITE_COLOR,
        borderRadius: 4,
        marginTop: 5
    },
    tabButtonStyle: {
        flexDirection: 'column',
        flex: 1,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }, tabTitleStyle: {
        height: 40,
        textAlignVertical: 'center',
        fontSize: 13,
        fontFamily: FRANKLIN_BOOK,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    playerNameStyle: {
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK,
        color: LOGIN_INSTRUCTION_COLOR,
        textAlign: 'center',
        marginTop: 5,
        textTransform: 'uppercase'
    },
    popularityTextStyle: {
        color: PARTICIPANT_COUNTTEXT_COLOR,
        fontFamily: FRANKLIN_DEMI,
        fontWeight: 'bold',
        fontSize: 22,
        textAlign: 'center',
        marginTop: 0
    },
    pointTextStyle: {
        fontSize: 10,
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        color: REGISTRATION_SUB_TITLE_COLOR,
        marginTop: 0
    },
    trumpCardStyle: {
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 12,
        color: LOGIN_MOBILE_NO_COLOR,
        textAlign: 'center',
        textTransform: 'uppercase',
        marginTop: 20
    },
    footerViewStyle: {
        marginTop: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    totalPointsStyle: {
        color: REGISTRATION_SUB_TITLE_COLOR,
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK
    },
    listHeaderTitle: {
        textTransform: 'uppercase',
        color: LOGIN_INSTRUCTION_COLOR,
        flex: 0.15,
        textAlign: 'center',
        fontSize: 8,
        fontFamily: FRANKLIN_BOOK
    }

})