import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, TextInput } from "react-native";
import SplashString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import { PRIMARY_COLOR, LOGIN_INSTRUCTION_COLOR, WHITE_COLOR, LOGIN_MOBILE_NO_COLOR, BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR } from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import APIClient from '../network/AxiosAPI';
import { isEmpty } from '../utils/TextInputUtils';
import Snackbar from 'react-native-snackbar';
import { saveUsername, saveLastLoginTime, saveAccessToken, saveRefreshToken } from '../utils/AsyncStorageUtils';
import Loader from '../components/Loading';
import { GET_OTP, VERIFY_OTP } from '../network/AppUri.js';
import { getCurrentTime } from '../utils/TimeUtils';

class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isBadgeVisible: true,
            otpCode: '',
            loading: false,
            isOTPVisible: false,
            isSubmitBtnVisible: false,
            isGetOTPBtnVisible: true,
            username: '',
            usernameEditable: true
        }
    }

    goToNextScreen = (isRegistered) => {
        if (isRegistered === 0) {
            this.props.navigation.replace('Registration');
        } else {
            this.props.navigation.replace('Home');
        }

    }

    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }
    getOTP = () => {
        const { username } = this.state;
        console.log(username);
        if (!isEmpty(username)) {
            this.setState({ loading: true })
            const loginRequest = {
                "phone_number": username,
                "email": ''
            }
            APIClient({
                method: 'PUT',
                url: GET_OTP,
                data: loginRequest
            }).then(response => {
                console.log(response.data)
                this.setState({
                    loading: false,
                    isOTPVisible: true,
                    isSubmitBtnVisible: true,
                    isGetOTPBtnVisible: false,
                    usernameEditable: false
                })

            }).catch(error => {
                console.log(error);
                this.setState({ loading: false });
                setTimeout(() => {
                    this.showSnackbar(SplashString.login.errorFetch);
                }, 1000);
            })
        } else {
            this.showSnackbar(SplashString.login.errorUsername)
        }
    }

    handleUsernameText = (text) => {
        console.log(text)
        this.setState({ username: text })
    }

    verifyOTP = () => {
        const { username, otpCode } = this.state;
        console.log(username.length);
        if (!isEmpty(otpCode)) {
            this.setState({ loading: true })
            const loginRequest = {
                "phone_number": username,
                "otp": otpCode
            }
            APIClient({
                method: 'POST',
                url: VERIFY_OTP,
                data: loginRequest
            }).then(response => {
                const data = response.data.data;
                console.log(data);
                saveUsername(username);
                this.saveTokens(data.token, data.refresh_token);
                saveLastLoginTime(getCurrentTime());
                this.setState({
                    loading: false,
                })
                //isRegistered check here
                this.goToNextScreen(data.is_user_registered);

            }).catch(error => {
                if (error.response.status === 400) {
                    this.setState({ loading: false, otpCode:'' });
                    this.setState({ loading: false });
                    setTimeout(() => {
                        this.showSnackbar(SplashString.login.errorOTP);
                    }, 1000);
                } else {
                    this.setState({ loading: false, otpCode:'' });
                    setTimeout(() => {
                        this.showSnackbar(SplashString.login.errorFetch);
                    }, 1000);
                }
            })
        } else {
            this.showSnackbar(SplashString.login.errorOTP)
        }
    }
    saveTokens = (accessToken, refreshToken) => {
        saveAccessToken(accessToken);
        saveRefreshToken(refreshToken);
    }
    handleRightIconPress = () => { }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground
                    style={[styles.imgBackground, { justifyContent: 'flex-end' }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_splash_background.png')}
                ><KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={{ height: '30%' }}>
                            <View style={{ flexDirection: 'row', height: 70, marginTop: 20 }}>
                                <Text style={[styles.titleTextStyle,]}>{SplashString.splash.title}</Text>
                                <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                                    <FontAwesome5 name={'bell'}
                                        size={20}
                                        color={WHITE_COLOR}
                                        opacity={1}
                                        onPress={() => { this.handleRightIconPress(); }} />
                                    {this.state.isBadgeVisible &&
                                        <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                                    }
                                </View>

                            </View>
                            <Text style={styles.subtitleTextStyle}>{SplashString.login.instruction}</Text>
                        </View>
                        <ImageBackground
                            style={[styles.imgBackground, { height: '70%', opacity: 0.6 }]}
                            resizeMode='cover'
                            source={require('../../assets/images/ic_back_image.png')}>

                            <Text style={styles.enterMobileNoTextStyle}>{SplashString.login.enterMobileNo}</Text>
                            <Text style={styles.editTextTitleStyle}>{SplashString.login.editTextTitle}</Text>
                            <View style={{ margin: 20, marginBottom: 10, flexDirection: 'row', }}>
                                <Text style={{ fontFamily: FRANKLIN_BOOK, fontSize: 20, flex: 0.2, textAlignVertical: 'center' }}>{SplashString.login.code}</Text>
                                <TextInput
                                    placeholder={SplashString.login.mobileNo}
                                    placeholderTextColor={LOGIN_INSTRUCTION_COLOR}
                                    underlineColorAndroid='transparent'
                                    maxLength={10}
                                    editable={this.state.usernameEditable}
                                    keyboardType='phone-pad'
                                    multiline={false}
                                    value ={this.state.username}
                                    returnKeyType='done'
                                    onChangeText={(text) => { this.handleUsernameText(text) }}
                                    style={{ borderBottomWidth: 1, fontFamily: FRANKLIN_BOOK, fontSize: 28, flex: 0.8, color:BLACK_COLOR }}
                                />
                            </View>
                            {this.state.isOTPVisible &&
                                <OTPInputView
                                    style={{ width: '80%', height: 50, marginStart: 20, marginEnd: 20 }}
                                    pinCount={6}
                                    code={this.state.otpCode}
                                    autoFocusOnLoad={true}
                                    codeInputFieldStyle={styles.underlineStyleBase}
                                    codeInputHighlightStyle={styles.underlineStyleHighLighted}
                                    onCodeChanged={(code => {
                                        this.setState({ otpCode: code })
                                    })}
                                    
                                />
                            }
                            {this.state.isGetOTPBtnVisible &&
                                <LinearGradient
                                    style={{ height: 40, borderRadius: 20, width: 120, margin: 20, opacity: 1 }}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1.5, y: 0 }}
                                    location={[0, 0.9]}
                                    colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                    <TouchableOpacity activeOpacity={1}
                                        onPress={() => { this.getOTP() }}
                                        style={[styles.loginButtonStyle]}>
                                        <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{SplashString.login.sendOTP}</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            }
                            {this.state.isSubmitBtnVisible &&
                                <LinearGradient
                                    style={{ height: 40, borderRadius: 20, width: 120, margin: 20, opacity: 1 }}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1.5, y: 0 }}
                                    location={[0, 0.9]}
                                    colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                    <TouchableOpacity activeOpacity={1}
                                        onPress={() => { this.verifyOTP() }}
                                        style={[styles.loginButtonStyle]}>
                                        <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{SplashString.login.submit}</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            }
                        </ImageBackground>
                    </KeyboardAwareScrollView>
                </ImageBackground>
            </SafeAreaView>
        )
    }
}

export default LoginScreen
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgBackground: {
        width: '100%',
        height: '100%',

    },
    listContainer: {
        flex: 1,
        backgroundColor: 'transparent'

    },
    jourseyStyle: {
        width: 100,
        height: 115,
    },

    titleTextStyle: {
        fontSize: 36,
        fontFamily: FRANKLIN_DEMI,
        color: PRIMARY_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    subtitleTextStyle: {
        fontSize: 15,
        marginTop: 0,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_BOOK,
        color: LOGIN_INSTRUCTION_COLOR
    },
    enterMobileNoTextStyle: {
        fontSize: 24,
        marginTop: 20,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_MEDIUM,
        color: LOGIN_MOBILE_NO_COLOR
    },
    editTextTitleStyle: {
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 14,
        color: LOGIN_INSTRUCTION_COLOR,
        marginStart: 20,
        marginEnd: 20,
        marginTop: 20,
    },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    borderStyleHighLighted: {
        borderColor: LOGIN_INSTRUCTION_COLOR
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: BLACK_COLOR,
        fontFamily: FRANKLIN_BOOK,
        borderColor: LOGIN_INSTRUCTION_COLOR,
        fontSize: 16,
    },

    underlineStyleHighLighted: {
        borderColor: LOGIN_INSTRUCTION_COLOR
    },
})