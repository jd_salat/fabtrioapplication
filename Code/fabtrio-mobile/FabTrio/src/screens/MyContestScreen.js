import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList, ScrollView } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK, FRANKLIN_MEDIUM_COND } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, TAB_HIGHLITER_COLOR
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import Snackbar from 'react-native-snackbar';
import { secondsToHms } from '../utils/TimeUtils.js';
import ProgressCircle from 'react-native-progress-circle';
import UpcomingContest from '../network/mycontestlistmock.json';
import PastContest from '../network/mycontestlistmock.json';

class MyContestScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            isBadgeVisible: false,
            upcomingContestVisible: true,
            pastContestVisible: false,
            upcomingContestData: [],
            pastContestData: []
        }
    }

    componentDidMount() {
        this.setState({
            upcomingContestData: UpcomingContest.response,
            pastContestData: PastContest.response
        })
    }
    renderEmptyListView = () => {
        return (
            <View style={[styles.emptyListViewStyle, styles.shadow]}>
                <Image style={{ width: 120, height: 120 }}
                    resizeMode='contain'
                    source={require('../../assets/images/ic_empty_contest.png')} />
                <Text style={{ fontFamily: FRANKLIN_MEDIUM_COND, fontSize: 20, color: BLACK_COLOR, textAlign: 'center', margin: 30 }}>{ScreenString.myContest.noContestError}</Text>
                <LinearGradient
                    style={{ height: 40, borderRadius: 20, width: 180, margin: 20, opacity: 1 }}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1.5, y: 0 }}
                    location={[0, 0.9]}
                    colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                    <TouchableOpacity activeOpacity={1}
                        onPress={() => {
                            //navigation.replace('Dashboard');
                        }}
                        style={[styles.loginButtonStyle]}>
                        <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{ScreenString.myContest.participate}</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </View>)
    }

    renderParticipnats = (item, variant_id, variant_name, match_id, cIndex) => {
       
        return (
            <TouchableOpacity
                key={cIndex}
                activeOpacity={1}
                onPress={()=>{
                    console.log("contest id"+item.contest_id);
                    console.log("variant id :"+variant_id);
                    console.log("match id :"+match_id);
                    console.log("team_id :"+item.team.user_team_id)
                }}
                style={{ flexDirection: 'column', justifyContent: 'center', height: 50, alignSelf: 'stretch' }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', height: 50, alignSelf: 'stretch' }}>
                    <Text style={[styles.listHeaderTitle, { flex: 0.3, fontSize: 9, textAlign: 'left', marginStart: 10 }]}>{variant_name}</Text>
                    <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'flex-start', flexDirection: 'row' }}>{
                        item.team.players.map((url, index) => {
                            return (
                                <View key={index} style={{ marginLeft: 3 }}>
                                    <ProgressCircle
                                        percent={url.popularity}
                                        radius={15}
                                        borderWidth={1}
                                        color={CONGRATS_COLOR}
                                        shadowColor="#cdcdcd"
                                        bgColor="#fff">
                                        <Image style={{ height: 30, width: 30 }}
                                            resizeMode='contain'
                                            source={{ uri: url.player_url }} />
                                    </ProgressCircle>
                                </View>)
                        })
                    }
                    </View>
                    <View style={{ flex: 0.15, justifyContent: 'center', alignItems: 'center', marginStart: 10 }}>
                        <Image style={{ height: 30, width: 30 }}
                            resizeMode='contain'
                            source={{ uri: item.team.trump_card_url }} />
                    </View>


                    <Text style={{ flex: 0.15, fontFamily: FRANKLIN_MEDIUM_COND, fontSize: 12, color: PARTICIPANT_COUNTTEXT_COLOR, textAlign: 'center', marginStart: 3 }}>{item.contest_amount}</Text>
                    <Text style={{ flex: 0.15, fontFamily: FRANKLIN_MEDIUM_COND, fontSize: 12, color: CONGRATS_COLOR, textAlign: 'center', fontWeight: 'bold' }}>{item.rank}</Text>
                </View>
                <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: '#cdcdcd', marginTop: 0 }} />
            </TouchableOpacity>
        );
    }
    renderContestItems = ({ item, index }) => {
        console.log(item.match_id)
        return (
            <TouchableOpacity style={[styles.shadow, { backgroundColor: WHITE_COLOR, borderRadius: 4, margin: 1, marginTop: 20, }]}
                activeOpacity={1}>
                <View style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teama.thumb_url }} />
                        </View>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={styles.matchTitleStyle}>{item.competition_name}</Text>
                        <Text style={styles.vsTextStyle}>{ScreenString.upcomingMatch.vs}</Text>
                        <Text style={styles.timeLeftTextStyle}>{secondsToHms(item.timestamp_start)} {ScreenString.upcomingMatch.left}</Text>
                    </View>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teamb.thumb_url }} />
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', height: 50, alignSelf: 'stretch' }}>
                        <Text style={[styles.listHeaderTitle, { flex: 0.3, textAlign: 'left', marginStart: 10, fontSize: 8 }]}>{ScreenString.myContest.contestType}</Text>
                        <Text style={[styles.listHeaderTitle, { flex: 0.3 }]}>{ScreenString.liveMatchData.team}</Text>
                        <Text style={[styles.listHeaderTitle, { marginStart: 3 }]}>{ScreenString.liveMatchData.trumpCard}</Text>
                        <Text style={styles.listHeaderTitle}>{ScreenString.myContest.entryFee}</Text>
                        <Text style={styles.listHeaderTitle}>{ScreenString.liveMatchData.rank}</Text>
                    </View>
                    <View style={{ height: 1, alignSelf: 'stretch', opacity: 0.5, backgroundColor: TAB_HIGHLITER_COLOR, marginTop: 0 }} />
                    <View>
                        {
                            item.user_contest.map((variant, vIndex) => {
                                return (
                                    <View key={vIndex}>
                                        {
                                            variant.participating_contest.map((contest, cIndex) => {
                                                return this.renderParticipnats(contest, variant.variant_id, variant.variant_name, item.match_id, cIndex)
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderUpcomingContest = () => {
        if (this.state.upcomingContestData.length === 0) {
            return (
                this.renderEmptyListView()
            )
        } else {
            return (
                <FlatList
                    style={{ margin: 19, marginTop: 0 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.pastContestData}
                    renderItem={this.renderContestItems}
                    keyExtractor={(item, index) => index.toString()}
                />
            )
        }
    }
    renderPastContest = () => {
        if (this.state.pastContestData.length === 0) {
            return (
                this.renderEmptyListView()
            )
        } else {
            return (
                <FlatList
                    style={{ margin: 19, marginTop: 0 }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.pastContestData}
                    renderItem={this.renderContestItems}
                    keyExtractor={(item, index) => index.toString()}
                />
            )
        }
    }

    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }


    handleMyTeamPress = () => {
        this.setState({
            upcomingContestVisible: true,
            pastContestVisible: false
        })
    }
    handleLeaderboardPress = () => {
        this.setState({
            upcomingContestVisible: false,
            pastContestVisible: true
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: -1, right: -10, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='stretch'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ flexDirection: 'row', height: 70, marginTop: 10 }}>
                        <Text style={[styles.titleTextStyle,]}>{ScreenString.myContest.title}</Text>
                        <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                            <FontAwesome5 name={'bell'}
                                size={20}
                                color={WHITE_COLOR}
                                opacity={1}
                                onPress={() => { this.handleRightIconPress(); }} />
                            {this.state.isBadgeVisible &&
                                <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                            }
                        </View>
                    </View>
                    <View style={[styles.mainContainerStyle, styles.shadow]}>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity style={[styles.tabButtonStyle]}
                                activeOpacity={1}
                                onPress={() => { this.handleMyTeamPress() }}>
                                <Text style={[styles.tabTitleStyle, { fontWeight: this.state.upcomingContestVisible === true ? 'bold' : 'normal' }]}>{ScreenString.myContest.liveContest}</Text>
                                <View style={{ height: 2, backgroundColor: this.state.upcomingContestVisible === true ? TIME_LEFT_TITLE_COLOR : TAB_HIGHLITER_COLOR, alignSelf: 'stretch' }} />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.tabButtonStyle}
                                activeOpacity={1}
                                onPress={() => { this.handleLeaderboardPress() }}>
                                <Text style={[styles.tabTitleStyle, { fontWeight: this.state.pastContestVisible === true ? 'bold' : 'normal' }]}>{ScreenString.myContest.past}</Text>
                                <View style={{ height: 2, backgroundColor: this.state.pastContestVisible === true ? TIME_LEFT_TITLE_COLOR : TAB_HIGHLITER_COLOR, alignSelf: 'stretch' }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>


                        {this.state.upcomingContestVisible &&
                            this.renderUpcomingContest()
                        }
                        {this.state.pastContestVisible &&
                            this.renderPastContest()
                        }
                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default MyContestScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,
        backgroundColor: WHITE_COLOR

    },
    listContainer: {
        flex: 1,


    },
    imageStyle: {
        width: '100%',
        height: '35%',
    },
    teamImageStyle: {
        height: 80,
        width: 60,
    },
    titleTextStyle: {
        fontSize: 18,
        fontFamily: FRANKLIN_BOOK,
        color: WHITE_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    emptyListViewStyle: {
        flex: 1,
        borderRadius: 4,
        alignSelf: 'stretch',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: WHITE_COLOR,
        margin: 20
    },
    matchTitleStyle: {
        marginTop: 0,
        textAlign: 'center',

        alignSelf: 'stretch',
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    vsTextStyle: {
        color: VS_TITLE_COLOR,
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        marginTop: 15
    },
    timeLeftTextStyle: {
        color: TIME_LEFT_TITLE_COLOR,
        fontFamily: FRANKLIN_DEMI,
        fontSize: 12,
        marginTop: 5
    },
    columnViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start'
    },
    
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    mainContainerStyle: {
        backgroundColor: WHITE_COLOR,
        borderTopStartRadius: 4,
        borderTopEndRadius: 4,
        marginStart: 20,
        marginEnd: 20
    },
    tabButtonStyle: {
        flexDirection: 'column',
        flex: 1,
        height: 50,
        justifyContent: 'flex-end',
        alignItems: 'center'
    }, tabTitleStyle: {
        height: 40,
        textAlignVertical: 'center',
        fontSize: 13,
        fontFamily: FRANKLIN_BOOK,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    listHeaderTitle: {
        textTransform: 'uppercase',
        color: LOGIN_INSTRUCTION_COLOR,
        flex: 0.15,
        textAlign: 'center',
        fontSize: 8,
        fontFamily: FRANKLIN_BOOK
    }
})