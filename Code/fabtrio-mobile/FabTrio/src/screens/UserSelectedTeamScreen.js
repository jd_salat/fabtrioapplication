import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, DIVIDER_BACKGROUND
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import Snackbar from 'react-native-snackbar';
var starImage= require('../../assets/images/ic_star.png');

import PlayerMockData from '../network/playermockdata.json'
var selectedPlayers = [];
var title = "";
var match_id = 0;
class UserSelectedTeamScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            selectedStarPlayer: undefined,
            selectedTrumpCard: undefined,
            trumpCardData: []
        }
    }

    componentDidMount() {
        this.setState({
            trumpCardData:
                [{ value: 1, name: ScreenString.userTeamSelection.bbowl },
                { value: 2, name: ScreenString.userTeamSelection.bbat },
                { value: 3, name: ScreenString.userTeamSelection.pom }]
        });
    }
    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }




    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }

    renderTrumpCardView() {
        return this.state.trumpCardData.map((item, index) => {
            const marginStart = index === 0 ? 0 : 15;
            const boderColor = item.value === this.state.selectedTrumpCard ? LOGIN_MOBILE_NO_COLOR : 'transparent'
            return (
                <TouchableOpacity activeOpacity={1} key={index} style={[styles.containerViewStyle, { marginStart: marginStart }]}
                    onPress={() => {
                        this.setState({ selectedTrumpCard: item.value })
                    }}
                >
                    <View style={[{
                        borderRadius: 50,
                        borderWidth: 1,
                        borderColor: boderColor,
                        width: 80, height: 80,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }]}>
                        <Image style={[styles.teamImageStyle]}
                            resizeMode='center'
                            source={{ uri: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1583143603776.png' }} />
                    </View>
                    <Text style={styles.playerNameStyle}>{item.name}</Text>

                </TouchableOpacity>

            )
        })
    }

    renderSelectedPlayerView() {
        return selectedPlayers.map((item, index) => {
            const marginStart = index === 0 ? 0 : 15;
            const boderColor = item.player_id === this.state.selectedStarPlayer ? LOGIN_MOBILE_NO_COLOR : 'transparent'
            const isStarPlayer = item.player_id === this.state.selectedStarPlayer?true:false
            return (

                <TouchableOpacity activeOpacity={1} key={index} style={[styles.containerViewStyle, { marginStart: marginStart }]}
                    onPress={() => {
                        this.setState({ selectedStarPlayer: item.player_id })
                    }}>
                    <View style={[{
                        borderRadius: 50,
                        borderWidth: 1,
                        borderColor: boderColor,
                        width: 80, height: 80,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }]}>
                        <Image style={[styles.teamImageStyle]}
                            resizeMode='center'
                            source={{ uri: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1583143603776.png' }} />
                    </View>
                    {isStarPlayer && 
                        <Image style={{ height: 20, width: 20, position:'absolute', top:65}}
                        resizeMode='contain'
                        source={starImage} />
                    }
                    <Text style={styles.playerNameStyle}>{item.title}</Text>

                </TouchableOpacity>

            )
        })
    }


    render() {
        match_id = this.props.route.params.matchId;
        title = this.props.route.params.matchTitle;
        selectedPlayers = this.props.route.params.selectedPlayers;
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleRightIconPress(); }} />
                                {this.state.isDotVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                                }
                            </View>

                        </View>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={[styles.containerStyle, styles.shadow]}>
                            <LinearGradient
                                style={{ height: 40, borderTopLeftRadius: 4, borderTopRightRadius: 4, opacity: 1 }}
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1.5, y: 0 }}
                                location={[0, 0.9]}
                                colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                <View style={styles.stepHeaderViewStyle}>
                                    <Text style={styles.stepTitleStyle}>{ScreenString.userTeamSelection.step2Title}</Text>
                                    <Text style={styles.continueStyle}
                                        onPress={() => {
                                            if (this.state.selectedStarPlayer !== undefined && this.state.selectedTrumpCard !== undefined) {
                                                this.props.navigation.replace('SelectContest'
                                                    , { matchId: match_id, matchTitle: title }
                                                )
                                            } else {
                                                this.showSnackbar(ScreenString.userTeamSelection.errorStarTrump)
                                            }

                                        }}
                                    >{ScreenString.userTeamSelection.continue}</Text>
                                </View>
                            </LinearGradient>
                            <Text style={styles.instructionTextStyle}>{ScreenString.userTeamSelection.combinationInstruction}</Text>
                            <Text style={styles.participationTitleTyle}>{ScreenString.userTeamSelection.selectStartPlayer}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center' }}>
                                {
                                    this.renderSelectedPlayerView()
                                }
                            </View>
                            <Text style={styles.participationTitleTyle}>{ScreenString.userTeamSelection.selectTrumpCard}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center', marginBottom: 20 }}>
                                {
                                    this.renderTrumpCardView()
                                }
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default UserSelectedTeamScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,

    },
    imageStyle: {
        width: '100%',
        height: '40%',
    },
    stepTitleStyle: {
        flex: 1,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 15,
        paddingStart: 10,
        color: WHITE_COLOR
    },
    continueStyle: {
        flex: 0.5,
        paddingEnd: 10,
        height: 40,
        textAlignVertical: 'center',
        textAlign: 'right',
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 15,
        color: WHITE_COLOR
    },

    teamImageStyle: {

        height: 60, width: 60,


    },
    playerNameStyle: {
        textTransform: 'uppercase',
        marginTop: 5,
        color: LOGIN_INSTRUCTION_COLOR,
        fontSize: 10,
        fontFamily: FRANKLIN_BOOK
    },
    containerStyle: {
        marginTop: 10,
        marginEnd: 20,
        marginStart: 20,
        backgroundColor: WHITE_COLOR,
        borderRadius: 4
    },

    stepHeaderViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        borderTopEndRadius: 4,
        borderTopStartRadius: 4,
        height: 40
    },
    instructionTextStyle: {
        marginTop: 15,
        marginStart: 10,
        marginEnd: 10,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 15
    },
    participationTitleTyle: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 12,
        marginTop: 30,
        textAlign: 'center',
        alignSelf: 'stretch'
    },
    containerViewStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },

})