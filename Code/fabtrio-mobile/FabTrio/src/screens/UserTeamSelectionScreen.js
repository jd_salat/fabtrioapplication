import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR, DIVIDER_BACKGROUND
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import Snackbar from 'react-native-snackbar';

import PlayerMockData from '../network/playermockdata.json'

var match_id = 0;
var title = undefined
class UserTeamSelectionScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            teamASquadData: [],
            teamBSquadData: [],
            selectedPlayers: [],
            playersData: []
        }
    }

    componentDidMount() {
        this.setState({
            teamASquadData: PlayerMockData.response.teama.squads,
            teamBSquadData: PlayerMockData.response.teamb.squads,
            playersData: [...PlayerMockData.response.teama.squads, ...PlayerMockData.response.teamb.squads,]
        });

    }

    getHeadToHeadCount = (item, team) => {
        var count = 0
        if (team === 1) {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teama.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        } else {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teamb.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        }
        return count;
    }
    getLastFiveResultView = (item, index) => {
        const marginLeft = index === 0 ? 0 : 5
        const backColor = item.match_point > 10 ? CONGRATS_COLOR : BUTTON_GRADIENT_START_COLOR
        return (
            <View key={index} style={[styles.lastFiveViewStyle, {
                marginLeft: marginLeft,
                backgroundColor: backColor,
            }]} >
                <Text style={styles.pointTextStyle}>{item.match_point}</Text>
            </View>
        )
    }
    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }
    onAddButtonClick = (item) => {
        console.log(this.state.selectedPlayers.length);

        if (this.state.selectedPlayers.length !== 0) {
            var isAlreadySelected = false
            var selectedIndex = -1
            for (var arrayIndex = 0; arrayIndex < this.state.selectedPlayers.length; arrayIndex++) {
                console.log(item.player_id)
                console.log(this.state.selectedPlayers[arrayIndex].player_id)
                if (item.player_id === this.state.selectedPlayers[arrayIndex].player_id) {
                    isAlreadySelected = true;
                    selectedIndex = arrayIndex;
                    break;
                } else {
                    isAlreadySelected = false
                    selectedIndex = -1
                }
            }
            if (isAlreadySelected === true) {
                this.state.selectedPlayers.splice(selectedIndex, 1)
            } else {
                if (this.state.selectedPlayers.length < 3) {
                    this.state.selectedPlayers.push(item)
                } else {
                    this.showSnackbar(ScreenString.userTeamSelection.errorPlayerSelection)
                }
            }

        } else {
            this.state.selectedPlayers.push(item)
        }

        this.setState({})




    }

    renderPlayersSelectionView = ({ item, index }) => {
        var btnBackColor = CONGRATS_COLOR
        var iconName = 'plus'
        this.state.selectedPlayers.map((player, selectedIndex) => {
            if (player.player_id === item.player_id) {
                iconName = 'minus',
                    btnBackColor = BUTTON_GRADIENT_START_COLOR
            }
        })


        return (
            <View key={index} style={{ flexDirection: 'column' }}>
                <View key={index} style={{ flexDirection: 'row', paddingTop: 15, paddingBottom: 15 }}>
                    <View style={[styles.containerViewStyle, { flex: 0.2 }]}>
                        <TouchableOpacity style={[styles.addButtonStyle, { borderColor: btnBackColor, }]}
                            onPress={() => {
                                this.onAddButtonClick(item)
                            }}
                        >
                            <FontAwesome5 name={iconName}
                                size={10}
                                background={WHITE_COLOR}
                                color={btnBackColor}
                                opacity={1}
                            >
                            </FontAwesome5>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.containerViewStyle, { flex: 0.3, paddingTop: 5, paddingBottom: 5 }]}>

                        <Image style={[styles.teamImageStyle]}
                            resizeMode='contain'
                            source={{ uri: 'https://fabtrio.s3.ap-south-1.amazonaws.com/team/1583143603776.png' }} />

                    </View>
                    <View style={[styles.rightViewStyle]}>

                        <Text style={styles.playerNameStyle}>{item.title}</Text>
                        <Text style={styles.recentMatchTextStyle}>{ScreenString.userTeamSelection.points}</Text>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'flex-start' }}>
                            {
                                item.last_five_match_stats.map((item, index) => {
                                    return this.getLastFiveResultView(item, index)
                                })
                            }
                        </View>
                    </View>
                </View>
                <View style={{ height: 1, alignSelf: "stretch", backgroundColor: DIVIDER_BACKGROUND, opacity: 0.5 }} />
            </View>
        )
    }

    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }


    render() {
        match_id = this.props.route.params.matchId;
        title = this.props.route.params.matchTitle;
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleRightIconPress(); }} />
                                {this.state.isDotVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                                }
                            </View>

                        </View>
                    </View>
                    <View style={{ marginTop: 10, marginEnd: 20, marginStart: 20, backgroundColor: WHITE_COLOR, borderRadius: 4 }}>
                        <LinearGradient
                            style={{ height: 40, borderTopLeftRadius: 4, borderTopRightRadius: 4, opacity: 1 }}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1.5, y: 0 }}
                            location={[0, 0.9]}
                            colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                            <View style={styles.stepHeaderViewStyle}>
                                <Text style={styles.stepTitleStyle}>{ScreenString.userTeamSelection.step1Title}</Text>
                                <Text style={styles.continueStyle}
                                    onPress={() => {
                                        if (this.state.selectedPlayers.length === 3) {


                                            this.props.navigation.replace('SelectedTeam',
                                                {
                                                    matchId: match_id,
                                                    selectedPlayers: this.state.selectedPlayers,
                                                    matchTitle: title
                                                }
                                            )
                                        } else {
                                            this.showSnackbar(ScreenString.userTeamSelection.errorPlayerSelection)
                                        }
                                    }
                                    }
                                >{ScreenString.userTeamSelection.continue}</Text>
                            </View>
                        </LinearGradient>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={{ marginTop: 0, marginEnd: 20, marginStart: 20, backgroundColor: WHITE_COLOR, borderBottomLeftRadius: 4, borderBottomRightRadius: 4 }}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={this.state.playersData}
                                renderItem={this.renderPlayersSelectionView}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default UserTeamSelectionScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,

    },
    imageStyle: {
        width: '100%',
        height: '40%',
    },
    stepTitleStyle: {
        flex: 1,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 15,
        paddingStart: 10,
        color: WHITE_COLOR
    },
    continueStyle: {
        flex: 0.5,
        paddingEnd: 10,
        height: 40,
        textAlignVertical: 'center',
        textAlign: 'right',
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 15,
        color: WHITE_COLOR
    },
    stepHeaderViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        borderTopEndRadius: 4,
        borderTopStartRadius: 4,
        height: 40
    },
    addButtonStyle: {
        borderRadius: 12,
        height: 24,
        justifyContent: 'center',
        alignItems: 'center',
        width: 24,
        borderWidth: 1,
        backgroundColor: WHITE_COLOR
    },
    containerViewStyle: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    teamImageStyle: {
        flex: 1,
        minHeight: 100, minWidth: 100
    },
    rightViewStyle: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        flex: 0.7,
        marginLeft: 10,
        marginRight: 5
    },
    playerNameStyle: {
        color: LOGIN_INSTRUCTION_COLOR,
        fontSize: 18,
        fontFamily: FRANKLIN_MEDIUM
    },
    recentMatchTextStyle: {
        marginTop: 5,
        color: LOGIN_INSTRUCTION_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 10
    },
    lastFiveViewStyle: {
        height: 30,
        width: 30,
        marginTop: 5,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    pointTextStyle: {
        fontSize: 12,
        fontFamily: FRANKLIN_MEDIUM,
        color: WHITE_COLOR
    }

})