import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_BOOK } from '../utils/FontUtils';
import { PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR, BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR } from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import { fetchAccessToken, } from '../utils/AsyncStorageUtils';
import Snackbar from 'react-native-snackbar';
import { SCHEDULE_MATCH_STATUS, LIVE_MATCH_STATUS, COMPLETED_MATCH_STATUS } from '../utils/Constants.js'
import UpcomingMatchData from '../network/matchmockdata.json';
import { secondsToHms } from '../utils/TimeUtils.js';
class MatchCenterScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isBadgeVisible: true,
            loading: false,
            upcomingMatchData: UpcomingMatchData,

        }
    }
    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }
    componentDidMount() {
        this.setState({
            upcomingMatchData: UpcomingMatchData
        })
    }

    fetchStateData = async () => {
        this.setState({ loading: true })
        const accessToken = await fetchAccessToken();
        const subURL = "?status=" + SCHEDULE_MATCH_STATUS;
        APIClient({
            method: 'GET',
            url: GET_MATCHES,
            headers: {
                "Authorization": `${accessToken}`,
                'Content-Type': 'application/json'
            },
        }).then(response => {
            const data = response.data.data;
            this.setState({
                loading: false,
                upcomingMatchData: data.all_state
            })
        }).catch(error => {
            console.log(error)
            this.setState({ loading: false });
            setTimeout(() => {
                this.showSnackbar(ScreenString.login.errorFetch);
            }, 1000);

        })
    }
    
    renderUpcomingMatches = ({ item, index }) => {

        console.log("*************************************************")
    
        console.log("Remaining Time " + secondsToHms(item.timestamp_start));
        return (
            <TouchableOpacity key={index} style={{ backgroundColor: WHITE_COLOR, borderRadius: 4, marginTop: 9 }}
                activeOpacity={1}
                onPress={() => {
                    if (item.status === SCHEDULE_MATCH_STATUS) {
                        this.props.navigation.navigate('MatchDetails', {
                            matchDetail: item
                        })
                    } else if (item.status === LIVE_MATCH_STATUS) {
                        this.props.navigation.navigate('MatchDetailsLive', {
                            matchDetail: item
                        })
                    } else if (item.status === COMPLETED_MATCH_STATUS) {
                        this.props.navigation.navigate('MatchDetails', {
                            matchDetail: item
                        })
                    }
                }}>
                <View style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.imageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teama.logo_url }} />
                        </View>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center' }}>
                            {
                                item.teama.last_five_result.map((item, index) => {
                                    return this.getLastFiveResultView(item, index)
                                })
                            }
                        </View>
                        <Text style={styles.headToHeadCountTextStyle}>{
                            this.getHeadToHeadCount(item, 1)
                        }</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={styles.matchTitleStyle}>{item.competition_name}</Text>
                        <Text style={styles.vsTextStyle}>{ScreenString.upcomingMatch.vs}</Text>
                        <Text style={styles.timeLeftTextStyle}>{secondsToHms(item.timestamp_start)} {ScreenString.upcomingMatch.left}</Text>
                        <Text style={styles.last5TitleStyle}>{ScreenString.upcomingMatch.last5}</Text>
                    </View>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.imageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teamb.logo_url }} />
                        </View>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center' }}>
                            {
                                item.teamb.last_five_result.map((item, index) => {
                                    return this.getLastFiveResultView(item, index)
                                })
                            }
                        </View>
                        <Text style={styles.headToHeadCountTextStyle}>{
                            this.getHeadToHeadCount(item, 2)
                        }</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    getHeadToHeadCount = (item, team) => {
        var count = 0
        if (team === 1) {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teama.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        } else {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teamb.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        }
        return count;
    }
    getLastFiveResultView = (item, index) => {
        const backColor = item.hasWon === true ? CONGRATS_COLOR : BUTTON_GRADIENT_START_COLOR
        return (
            <View key={index} style={{ backgroundColor: backColor, height: 6, width: 6, margin: 2, marginTop: 5, borderRadius: 3 }} />
        )
    }
    handleRightIconPress = () => {
        this.props.navigation.navigate('Notification')
     }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />

                <ImageBackground
                    style={[styles.imgBackground, { justifyContent: 'flex-end' }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_splash_background.png')}>
                    <View style={styles.listContainer} >
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', height: 70, marginTop: 20 }}>
                                <Text style={[styles.titleTextStyle,]}>{ScreenString.splash.title}</Text>
                                <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                                    <FontAwesome5 name={'bell'}
                                        size={20}
                                        color={WHITE_COLOR}
                                        opacity={1}
                                        onPress={() => { this.handleRightIconPress(); }} />
                                    {this.state.isBadgeVisible &&
                                        <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                                    }
                                </View>
                            </View>
                            <View style={{ alignSelf: 'stretch', flex: 1, marginStart: 20, marginEnd: 20, flexDirection: 'column' }}>
                                <View style={{ alignSelf: 'stretch', flexDirection: 'row' }}>
                                    <Text style={{ color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 15, marginTop: 15, flex: 1 }}>{ScreenString.upcomingMatch.upcomingMatches}</Text>
                                    <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                        <LinearGradient
                                            style={{ height: 40, borderRadius: 20, width: 120, opacity: 1 }}
                                            start={{ x: 0, y: 0 }}
                                            end={{ x: 1.5, y: 0 }}
                                            location={[0, 0.9]}
                                            colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                            <TouchableOpacity activeOpacity={1}
                                                onPress={() => { }}
                                                style={[styles.loginButtonStyle]}>
                                                <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{ScreenString.upcomingMatch.viewAll}</Text>
                                            </TouchableOpacity>
                                        </LinearGradient>
                                    </View>
                                </View>
                                <FlatList
                                    style={{ marginTop: 20, marginStart: 1, flex: 0.8 }}
                                    showsVerticalScrollIndicator={false}
                                    data={this.state.upcomingMatchData}
                                    renderItem={this.renderUpcomingMatches}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </View>
                    </View>
                </ImageBackground>


            </SafeAreaView>
        )
    }
}

export default MatchCenterScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1
    },
    imgBackground: {
        width: '100%',
        height: '100%',

    },
    imageStyle: {
        height: 100,
        width: 80,
    },
    listContainer: {
        flex: 1,
        backgroundColor: 'transparent'

    },
    titleTextStyle: {
        fontSize: 36,
        fontFamily: FRANKLIN_DEMI,
        color: PRIMARY_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    headToHeadCountTextStyle: {
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        textAlign: 'center'
    },
    matchTitleStyle: {
        marginTop: 20,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    vsTextStyle: {
        color: VS_TITLE_COLOR,
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        marginTop: 15
    },
    timeLeftTextStyle: {
        color: TIME_LEFT_TITLE_COLOR,
        fontFamily: FRANKLIN_DEMI,
        fontSize: 12,
        marginTop: 5
    },
    columnViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    last5TitleStyle: {
        color: BLACK_COLOR,
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK,
        marginTop: 15
    }

})