import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import {
    PRIMARY_COLOR, VS_TITLE_COLOR, WHITE_COLOR, TIME_LEFT_TITLE_COLOR,
    BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, PARTICIPANT_COUNTTEXT_COLOR,
    REGISTRATION_SUB_TITLE_COLOR, CONGRATS_COLOR, LOGIN_MOBILE_NO_COLOR, LOGIN_INSTRUCTION_COLOR
} from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_MATCHES } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import { secondsToHms } from '../utils/TimeUtils.js';
import ParticipantCount from '../network/participantscountmock.json'
var matchDetail = {}

class MatchDetailsScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isDotVisible: false,
            participantCountData: []
        }
    }

    componentDidMount() {
        this.setState({ participantCountData: ParticipantCount.response })
    }

    getHeadToHeadCount = (item, team) => {
        var count = 0
        if (team === 1) {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teama.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        } else {
            item.last_five_match_stats.map((winnigResult, index) => {
                if (item.teamb.team_id === winnigResult.winning_team_id) {
                    count++;
                }
            })
        }
        return count;
    }
    getLastFiveResultView = (item, index) => {
        const backColor = item.hasWon === true ? CONGRATS_COLOR : BUTTON_GRADIENT_START_COLOR
        return (
            <View key={index} style={{ backgroundColor: backColor, height: 10, width: 10, margin: 2, borderRadius: 5 }} />
        )
    }
    renderUpcomingMatches = (item) => {
        return (
            <TouchableOpacity style={[styles.shadow, { backgroundColor: WHITE_COLOR, borderRadius: 4, marginTop: 0, height: '35%' }]}
                activeOpacity={1}>
                <View style={{ flex: 1, flexDirection: 'row', padding: 10 }}>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teama.logo_url }} />
                        </View>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center' }}>
                            {
                                item.teama.last_five_result.map((item, index) => {
                                    return this.getLastFiveResultView(item, index)
                                })
                            }
                        </View>
                        <Text style={styles.headToHeadCountTextStyle}>{
                            this.getHeadToHeadCount(item, 1)
                        }</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                        <Text style={styles.matchTitleStyle}>{item.competition_name}</Text>
                        <Text style={styles.vsTextStyle}>{ScreenString.upcomingMatch.vs}</Text>
                        <Text style={styles.timeLeftTextStyle}>{secondsToHms(item.timestamp_start)} {ScreenString.upcomingMatch.left}</Text>
                        <Text style={styles.last5TitleStyle}>{ScreenString.upcomingMatch.last5}</Text>
                    </View>
                    <View style={styles.columnViewStyle}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={[styles.teamImageStyle]}
                                resizeMode='contain'
                                source={{ uri: item.teamb.logo_url }} />
                        </View>
                        <View style={{ flexDirection: 'row', alignSelf: 'stretch', justifyContent: 'center' }}>
                            {
                                item.teamb.last_five_result.map((item, index) => {
                                    return this.getLastFiveResultView(item, index)
                                })
                            }
                        </View>
                        <Text style={styles.headToHeadCountTextStyle}>{
                            this.getHeadToHeadCount(item, 2)
                        }</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
    handleRightIconPress = () => { }
    handleLeftIconPress = () => { this.props.navigation.pop() }

    renderParticipantCountView = () => {

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                {
                    this.state.participantCountData.map((item, index) => {
                        var textColor = CONGRATS_COLOR
                        var marginLeft = 20
                        if (index == 0) {
                            marginLeft = 0
                        }
                        if (index === 1) {
                            textColor = PARTICIPANT_COUNTTEXT_COLOR
                        } else if (index == 2) {
                            textColor = TIME_LEFT_TITLE_COLOR
                        }
                        return (<View key={index} style={{ justifyContent: 'center', alignItems: 'center', marginLeft: marginLeft }}>
                            <Text style={{ fontSize: 18, fontFamily: FRANKLIN_MEDIUM, color: LOGIN_INSTRUCTION_COLOR }}>{item.v_name}</Text>
                            <Text style={{ fontSize: 24, fontFamily: FRANKLIN_MEDIUM, color: textColor, marginTop: 0 }}>{item.participants}</Text>
                        </View>)
                    })
                }
            </View>
        )
    }

    render() {
        matchDetail = this.props.route.params.matchDetail
        const title = matchDetail.teama.short_name + " VS " + matchDetail.teamb.short_name;
        console.log(matchDetail.teama.short_name)

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />
                <ImageBackground style={[styles.imageStyle, { position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, zIndex: 0 }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_match_detail_back.png')} >
                </ImageBackground>
                <View style={{ flex: 1, alignSelf: 'stretch' }}>
                    <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
                        <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                            <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                                <FontAwesome5 name={'angle-left'}
                                    size={22}
                                    background={WHITE_COLOR}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleLeftIconPress(); }} >
                                </FontAwesome5>

                            </View>
                            <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                            <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                                <FontAwesome5 name={'bell'}
                                    size={20}
                                    color={WHITE_COLOR}
                                    opacity={1}
                                    onPress={() => { this.handleRightIconPress(); }} />
                                {this.state.isDotVisible &&
                                    <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                                }
                            </View>

                        </View>
                    </View>
                    <KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={{ flex: 1, margin: 20 }}>{
                            this.renderUpcomingMatches(matchDetail)
                        }
                            <View style={[styles.shadow, { backgroundColor: WHITE_COLOR, flex: 1, marginTop: 10, borderRadius: 4, alignItems: 'center' }]}>
                                <Text style={styles.participationTitleTyle}>{ScreenString.matchDetail.participantsTitle}</Text>
                                {
                                    this.renderParticipantCountView()
                                }
                                <Text style={styles.participationTitleTyle}>{ScreenString.matchDetail.yourTeam}</Text>
                                <Image style={[styles.teamImageStyle]}
                                    resizeMode='contain'
                                    source={require("../../assets/images/ic_jersey.png")} />

                                <LinearGradient
                                    style={{ height: 40, borderRadius: 20, paddingHorizontal: 20, margin: 20, opacity: 1 }}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1.5, y: 0 }}
                                    location={[0, 0.9]}
                                    colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                    <TouchableOpacity activeOpacity={1}
                                        onPress={() => {
                                            this.props.navigation.replace('TeamSelection', {
                                                matchId: matchDetail.match_id,
                                                matchTitle: title
                                            })
                                        }}
                                        style={[styles.loginButtonStyle]}>
                                        <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{ScreenString.matchDetail.createTeam}</Text>
                                    </TouchableOpacity>
                                </LinearGradient>

                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>

            </SafeAreaView>
        )
    }
}

export default MatchDetailsScreen
function elevationShadowStyle(elevation) {
    return {
        elevation,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 0.5 * elevation },
        shadowOpacity: 0.3,
        shadowRadius: 0.8 * elevation
    };
}
const styles = StyleSheet.create({
    shadow: elevationShadowStyle(2),
    container: {
        flex: 1,

    },
    imageStyle: {
        width: '100%',
        height: '40%',
    },
    teamImageStyle: {
        height: 100,
        width: 80,
    },
    headToHeadCountTextStyle: {
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        textAlign: 'center'
    },
    matchTitleStyle: {
        marginTop: 20,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 9,
        color: REGISTRATION_SUB_TITLE_COLOR
    },
    vsTextStyle: {
        color: VS_TITLE_COLOR,
        fontSize: 24,
        fontFamily: FRANKLIN_DEMI,
        marginTop: 15
    },
    timeLeftTextStyle: {
        color: TIME_LEFT_TITLE_COLOR,
        fontFamily: FRANKLIN_DEMI,
        fontSize: 12,
        marginTop: 5
    },
    columnViewStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    last5TitleStyle: {
        color: BLACK_COLOR,
        fontSize: 12,
        fontFamily: FRANKLIN_BOOK,
        marginTop: 15
    },
    participationTitleTyle: { color: LOGIN_MOBILE_NO_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 15, marginTop: 20, textAlign: 'center', alignSelf: 'stretch' },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },

})