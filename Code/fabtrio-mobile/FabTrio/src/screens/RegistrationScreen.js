import React, { Component } from 'react'
import { StyleSheet, View, StatusBar, SafeAreaView, Text, ImageBackground, Image, TouchableOpacity, FlatList } from "react-native";
import ScreenString from '../strings/en.json';
import { FRANKLIN_DEMI, FRANKLIN_MEDIUM, FRANKLIN_BOOK } from '../utils/FontUtils';
import { PRIMARY_COLOR, LOGIN_INSTRUCTION_COLOR, WHITE_COLOR,TAB_HIGHLITER_COLOR, LOGIN_MOBILE_NO_COLOR, BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR, BLACK_COLOR, REGISTRATION_SUB_TITLE_COLOR } from '../utils/ColorConstants';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import FloatingLabel from 'react-native-floating-labels';
import Loader from '../components/Loading';
import APIClient from '../network/AxiosAPI';
import { GET_STATES, REGISTRATION } from '../network/AppUri';
import { saveIsRegistered, fetchAccessToken, fetchUsername, } from '../utils/AsyncStorageUtils';
import ModalComponent from "../components/ModalComponent";
import Snackbar from 'react-native-snackbar';
import DateTimePicker from '@react-native-community/datetimepicker';
import { getDateInFormat, getCurrentTime, isAgeValid } from '../utils/TimeUtils.js';
import moment from 'moment';
import { isEmpty } from '../utils/TextInputUtils.js';



class RegistrationScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isBadgeVisible: true,
            otpCode: '',
            isOTPVisible: true,
            isSubmitBtnVisible: true,
            isGetOTPBtnVisible: false,
            stateData: [],
            loading: false,
            date: new Date(getCurrentTime()),
            showDatePicker: false,
            selectState: undefined,
            validAge: false,
            firstName: undefined,
            lastName: undefined,
            emailId: undefined,
            selectedDate: undefined
        }
    }


    handleFirstName = (firstName) => {
        this.setState({ firstName })
    }


    handleLastName = (lastName) => {
        this.setState({ lastName })
    }


    handleEmail = (emailId) => {
        this.setState({ emailId })
    }
    toggleDatePicker = () => {

        this.setState({ showDatePicker: true });
    }

    handleDateChange = (event, changedDate) => {
        if (event.type === "set") {
            this.setState({
                date: changedDate,
                showDatePicker: false,
                validAge: isAgeValid(changedDate),
                selectedDate: moment.utc(changedDate).format("YYYY-MM-DD")
            })
        }
    }
    showSnackbar = (msg) => {
        Snackbar.show({
            text: msg,
            duration: Snackbar.LENGTH_SHORT,
        });
    }
    componentDidMount() {
        this.fetchStateData();
    }
    fetchStateData = async () => {
        this.setState({ loading: true })
        const accessToken = await fetchAccessToken();
        APIClient({
            method: 'GET',
            url: GET_STATES,
            headers: {
                "Authorization": `Bearer ${accessToken}`,
                'Content-Type': 'application/json'
            },
        }).then(response => {
            const data = response.data.data;
            this.setState({
                loading: false,
                stateData: data.all_state
            })
        }).catch(error => {
            console.log(error)
            this.setState({ loading: false });
            setTimeout(() => {
                this.showSnackbar(ScreenString.login.errorFetch);
            }, 1000);

        })
    }

    registerUser = async () => {
        const { firstName, lastName, selectedDate, validAge, emailId, selectState } = this.state;
        const username = await fetchUsername();
        console.log(username)
        const accessToken = await fetchAccessToken();
        if (!isEmpty(firstName) && firstName !== undefined && !isEmpty(lastName) && lastName !== undefined) {
            console.log(emailId)
            if (selectedDate !== undefined && validAge === true) {
                if (selectState !== undefined) {
                    this.setState({ loading: true })
                    const loginRequest = {
                        "phone_number": username,
                        "email": emailId === undefined ? '' : emailId,
                        "first_name": firstName,
                        "last_name": lastName,
                        "state_id": selectState.id,
                        "dob": selectedDate,
                        "is_kyc_done": "false"
                    }
                    console.log(loginRequest)
                    console.log(accessToken)
                    APIClient({
                        method: 'POST',
                        url: REGISTRATION,
                        data: loginRequest,
                        headers: {
                            "Authorization": `${accessToken}`,
                            'Content-Type': 'application/json'
                        },
                    }).then(response => {
                        const data = response.data.data;
                        console.log(data);
                        this.setState({
                            loading: false,
                        })
                        saveIsRegistered(true)
                        //isRegistered check here
                        this.goToNextScreen();

                    }).catch(error => {
                        console.log(error.response);
                        if (error.response.status === 424) {
                            this.setState({ loading: false });
                            setTimeout(() => {
                                this.showSnackbar(ScreenString.registration.mobileNoError);
                            }, 1000);
                        } else {
                            this.setState({ loading: false });
                            setTimeout(() => {
                                this.showSnackbar(ScreenString.registration.errorSubmit);
                            }, 1000);
                        }
                    })
                } else {
                    this.showSnackbar(ScreenString.registration.errorState)
                }
            } else {
                this.showSnackbar(ScreenString.registration.errorDate)
            }
        } else {
            this.showSnackbar(ScreenString.registration.errorName)
        }
    }
    onStateModalOpen = () => {

    }

    handleRightIconPress = () => { }

    onStateModalClose = () => {
    }

    onPickerPress = () => {
        this.refs.statePickerModal.openModal();
    }

    goToNextScreen = () => {
        this.props.navigation.navigate('Welcome', {
            KEY_FIRST_NAME: this.state.firstName,
            KEY_LAST_NAME: this.state.lastName
        });
    }
    _renderItem = ({ item, index }) => {
        return (
            <View style={{ height: 40, justifyContent: 'center' }}>
                <TouchableOpacity style={{ flex: 1, flexDirection: 'column', }} activeOpacity={1} onPress={() => {
                    this.setState({
                        subjectValue: item.value,
                        subject: item.label
                    });
                    this.handleStateSelection(item);
                }}>
                    <Text style={[styles.subjectTitleStyle, { flex: 1, textAlign: 'left', textAlignVertical: 'center', color:BLACK_COLOR }]}>{item.state_name}</Text>
                    <View style={{ height: 1, flexDirection: 'row', alignSelf: 'stretch', backgroundColor: PRIMARY_COLOR, opacity: 0.1 }} />
                </TouchableOpacity>
            </View>
        );
    }

    handleStateSelection = (item) => {
        this.refs.statePickerModal.closeModal();
        this.setState({
            selectState: item
        })

    }
    onPickerPress = () => {
        this.refs.statePickerModal.openModal();
    }
    renderPickerModal = () => {

        return (
            <View style={{ alignSelf: 'stretch' }}>
                <Text style={[styles.subjectTitleStyle, { textAlign: 'center' }]}>{ScreenString.registration.selectState}</Text>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.state.stateData}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    };
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor={PRIMARY_COLOR} />
                <Loader
                    loading={this.state.loading} />

                <ImageBackground
                    style={[styles.imgBackground, { justifyContent: 'flex-end' }]}
                    resizeMode='cover'
                    source={require('../../assets/images/ic_splash_background.png')}
                ><KeyboardAwareScrollView style={styles.listContainer} contentContainerStyle={{ flexGrow: 1 }} enableOnAndroid={true}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', height: 70, marginTop: 20 }}>
                                <Text style={[styles.titleTextStyle,]}>{ScreenString.splash.title}</Text>
                                <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end', justifyContent: 'center' }}>
                                    <FontAwesome5 name={'bell'}
                                        size={20}
                                        color={WHITE_COLOR}
                                        opacity={1}
                                        onPress={() => { this.handleRightIconPress(); }} />
                                    {this.state.isBadgeVisible &&
                                        <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'center', top: 25, right: 3 }} ></ View>
                                    }
                                </View>
                            </View>

                        </View>
                        <View
                            style={styles.registationContainerStyle}>
                            <Text style={styles.enterMobileNoTextStyle}>{ScreenString.registration.title}</Text>
                            <Text style={styles.editTextTitleStyle}>{ScreenString.registration.subtitle}</Text>

                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                onChangeText={(text) => { this.handleFirstName(text) }}
                                inputStyle={styles.input}
                                style={styles.formInput}>{ScreenString.registration.firstName}</FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                onChangeText={(text) => { this.handleLastName(text) }}
                                style={styles.formInput}>{ScreenString.registration.lastName}</FloatingLabel>
                            <FloatingLabel
                                labelStyle={styles.labelInput}
                                inputStyle={styles.input}
                                keyboardType='email-address'
                                onChangeText={(text) => { this.handleEmail(text) }}
                                style={styles.formInput}>{ScreenString.registration.email}<Text style={{color:TAB_HIGHLITER_COLOR}}>{ScreenString.registration.optional}</Text></FloatingLabel>

                                <Text style={[styles.subjectTitleStyle, styles.pickerTitleStyle]}>{ScreenString.registration.dob}<Text style={{color:TAB_HIGHLITER_COLOR}}>{ScreenString.registration.ageInstruction}</Text></Text>
                            <View style={styles.pickerBackStyle}>
                                <TouchableOpacity style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }} onPress={() => { this.toggleDatePicker(); }} activeOpacity={1}>
                                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: 0 }}>
                                        <Text style={[styles.subjectTitleStyle, {color:BLACK_COLOR}]}>{moment.utc(this.state.date).format("DD/MM/YYYY")}</Text>
                                    </View>
                                    <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                                        <FontAwesome5 name='calendar-alt'
                                            size={18}
                                            color={LOGIN_INSTRUCTION_COLOR}
                                            opacity={1}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <Text style={[styles.subjectTitleStyle, styles.pickerTitleStyle]}>{ScreenString.registration.state}</Text>
                            <View style={styles.pickerBackStyle}>
                                <TouchableOpacity style={{ flex: 1, justifyContent: 'center', flexDirection: 'row' }} onPress={() => { this.onPickerPress(); }} activeOpacity={1}>
                                    <View style={{ flex: 0.8, justifyContent: 'center', alignItems: 'flex-start', paddingHorizontal: 0 }}>
                                        <Text style={[styles.subjectTitleStyle, {color:BLACK_COLOR}]}>{this.state.selectState === undefined ? ScreenString.registration.selectState : this.state.selectState.state_name}</Text>
                                    </View>
                                    <View style={{ flex: 0.2, justifyContent: 'center', alignItems: 'center' }}>
                                        <FontAwesome5 name='angle-down'
                                            size={18}
                                            color={LOGIN_INSTRUCTION_COLOR}
                                            opacity={1}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            {this.state.isSubmitBtnVisible &&
                                <LinearGradient
                                    style={{ height: 40, borderRadius: 20, width: 120, margin: 20, opacity: 1 }}
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1.5, y: 0 }}
                                    location={[0, 0.9]}
                                    colors={[BUTTON_GRADIENT_START_COLOR, BUTTON_GRADIENT_END_COLOR]}>
                                    <TouchableOpacity activeOpacity={1}
                                        onPress={() => { this.registerUser() }}
                                        style={[styles.loginButtonStyle]}>
                                        <Text style={[styles.buttonTextStyle, { fontSize: 16 }]}>{ScreenString.login.submit}</Text>
                                    </TouchableOpacity>
                                </LinearGradient>
                            }
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
                <ModalComponent
                    ref={"statePickerModal"}
                    renderContent={this.renderPickerModal}
                    modalStyle={styles.pickerModalStyle}
                    onClose={this.onStateModalClose}
                    onOpen={this.onStateModalOpen} />
                {this.state.showDatePicker && (
                    <DateTimePicker
                        timeZoneOffsetInMinutes={0}
                        value={this.state.date}
                        mode={'date'}
                        is24Hour={false}
                        display="default"
                        maximumDate={new Date()}
                        onChange={(event, changedDate) => { this.handleDateChange(event, changedDate) }}
                    />
                )}
            </SafeAreaView>
        )
    }
}

export default RegistrationScreen
const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    imgBackground: {
        width: '100%',
        height: '100%',

    },
    listContainer: {
        flex: 1,
        backgroundColor: 'transparent'

    },
    titleTextStyle: {
        fontSize: 36,
        fontFamily: FRANKLIN_DEMI,
        color: PRIMARY_COLOR,
        textAlignVertical: 'center',
        flex: 1,
        marginLeft: 20
    },
    subtitleTextStyle: {
        fontSize: 15,
        marginTop: 0,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_BOOK,
        color: LOGIN_INSTRUCTION_COLOR
    },
    registationContainerStyle: {
        backgroundColor: WHITE_COLOR,
        borderRadius: 4,
        flex: 99,
        alignSelf: 'stretch',
        margin: 20
    }, enterMobileNoTextStyle: {
        fontSize: 24,
        marginTop: 20,
        marginStart: 20,
        marginEnd: 20,
        fontFamily: FRANKLIN_MEDIUM,
        color: LOGIN_MOBILE_NO_COLOR
    },
    editTextTitleStyle: {
        fontFamily: FRANKLIN_MEDIUM,
        fontSize: 14,
        color: REGISTRATION_SUB_TITLE_COLOR,
        marginStart: 20,
        marginEnd: 20,
        marginTop: 20,

    },

    labelInput: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontSize: 13,
        fontFamily: FRANKLIN_BOOK,
        paddingLeft:0
    },
    formInput: {
        borderBottomWidth: 1,
        marginLeft: 20,
        marginRight: 20,
        borderColor: REGISTRATION_SUB_TITLE_COLOR,
    },
    input: {
        borderWidth: 0,
        fontSize: 16,
        paddingLeft:0,
        fontFamily: FRANKLIN_BOOK
    },
    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: BLACK_COLOR,
        fontFamily: FRANKLIN_BOOK,
        borderColor: LOGIN_INSTRUCTION_COLOR,
        fontSize: 16,
    },

    underlineStyleHighLighted: {
        borderColor: LOGIN_INSTRUCTION_COLOR
    },
    loginButtonStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: 52,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4
    },
    buttonTextStyle: {
        fontFamily: FRANKLIN_BOOK,
        textAlign: 'center',
        fontSize: 12,
        color: WHITE_COLOR
    },
    pickerModalStyle: {
        height: '70%',
        backgroundColor: WHITE_COLOR,
    },
    pickerBackStyle: {
        height: 50,
        borderRadius: 4,
        borderBottomColor: REGISTRATION_SUB_TITLE_COLOR,
        borderBottomWidth: 1,
        marginStart: 20,
        marginRight: 20,
        marginTop: 0,
        backgroundColor: 'transparent',
        flexDirection: 'row'
    },
    subjectTitleStyle: {
        color: LOGIN_MOBILE_NO_COLOR,
        fontFamily: FRANKLIN_BOOK,
        fontSize: 13,
    },
    pickerTitleStyle: {
        marginStart: 20,
        marginRight: 20,
        marginTop: 20,
    }
})