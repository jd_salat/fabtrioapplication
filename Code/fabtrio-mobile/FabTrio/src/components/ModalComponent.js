import React from 'react';
import { ViewPropTypes, StyleSheet, ScrollView, View, Dimensions } from 'react-native';
import Modal from 'react-native-modalbox';
import PropTypes from 'prop-types';
var screen = Dimensions.get('window');
export default class ModalComponent extends React.Component {
    constructor(props) {
        super(props);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    static propTypes = {
        onClose: PropTypes.func,
        onOpen: PropTypes.func,
        renderContent: PropTypes.func.isRequired,
        modalStyle: ViewPropTypes.style,
    }

    static defaultProps = {
        onClose: () => { },
        onOpen: () => { },
        modalStyle: null

    }

    closeModal() {
        this.refs.modal.close();
    }

    openModal() {
        this.refs.modal.open();
    }

    onClose = () => {
        const { onClose } = this.props;
        onClose();
    }

    onOpen = () => {
        const { onOpen } = this.props;
        onOpen();
    }

    onClosingState(state) {
        console.log('the open/close of the swipeToClose just changed');
    }
    render() {

        const { modalStyle } = this.props;
        const { renderContent } = this.props;
        return (
            <Modal style={[styles.modal, modalStyle]}
                position={"bottom"}
                ref={"modal"}
                swipeArea={0}
                swipeToClose={false}
                onClosed={this.onClose}
                onOpened={this.onOpen}
                onClosingState={this.onClosingState}>
                <View style={styles.viewStyle}>
                    {renderContent()}
                </View>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    modal: {
        width: screen.width,
        padding: 20,
        backgroundColor: '#f7f5eee8'
    },
    viewStyle: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    }

});