import React from "react";
import { View, Text } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { FRANKLIN_BOOK, POPPINS_REGULAR } from '../utils/FontUtils';
import { PRIMARY_COLOR, WHITE_COLOR } from "../utils/ColorConstants";
export default class HeaderWithBackbutton extends React.Component {
    constructor(props) {
        super(props);
    }

    handleLeftIconPress = () => {
        const { leftIconPress } = this.props;
        leftIconPress();
    }
    handleRightIconPress = () => {
        const { rightIconPress } = this.props;
        rightIconPress();
    }

    render() {
        const { title, isBadgeVisible } = this.props
        return (
            <View style={{ alignSelf: 'stretch', height: 50, flexDirection: 'column', alignItems: 'center' }}>
            <View style={{ alignSelf: 'stretch', backgroundColor: '#FFFFFF00', height: 50, flexDirection: 'row', alignItems: 'center' }}>
                <View activeOpacity={1} style={{ flex: 0.2, marginStart: 10, }}>

                    <FontAwesome5 name={'angle-left'}
                        size={22}
                        background={WHITE_COLOR}
                        color={WHITE_COLOR}
                        opacity={1}
                        onPress={() => { this.handleLeftIconPress(); }} >
                    </FontAwesome5>

                </View>
                <Text style={{ flex: 0.6, textAlign: 'center', color: WHITE_COLOR, fontFamily: FRANKLIN_BOOK, fontSize: 14 }}>{title}</Text>
                <View activeOpacity={1} style={{ marginEnd: 20, flex: 0.2, alignItems: 'flex-end' }}>
                    <FontAwesome5 name={'bell'}
                        size={20}
                        color={WHITE_COLOR}
                        opacity={1}
                        onPress={() => { this.handleRightIconPress(); }} />
                    {isBadgeVisible &&
                        <View style={{ backgroundColor: 'red', height: 5, width: 5, borderRadius: 2.5, position: 'absolute', justifyContent: 'flex-end', top: 3, right: 2 }} ></ View>
                    }
                </View>

            </View>
        </View>
        );
    }
}