
import React, { Component } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/MaterialCommunityIcons';
import { REGISTRATION_SUB_TITLE_COLOR, PRIMARY_COLOR } from "../utils/ColorConstants";
import { FRANKLIN_BOOK } from "../utils/FontUtils";
export default function BottomBarComponent({ state, descriptors, navigation }) {
    return (
        <View style={[{ flexDirection: 'column', height: 50 }]}>
            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: REGISTRATION_SUB_TITLE_COLOR }} />
            <View style={[{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 10 }]}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];
                    const label =
                        options.tabBarLabel !== undefined
                            ? options.tabBarLabel
                            : options.title !== undefined
                                ? options.title
                                : route.name;

                    const isFocused = state.index === index;

                    var iconName = ''
                    if (label === "Home") {
                        iconName = 'home'
                    } else if (label === "Games") {
                        iconName = 'television'
                    } else if (label === "Contests") {
                        iconName = 'trophy'
                    } else if (label === "Account") {
                        iconName = 'wallet'
                    }

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };

                    const onLongPress = () => {
                        navigation.emit({
                            type: 'tabLongPress',
                            target: route.key,
                        });
                    };

                    return (
                        <TouchableOpacity
                            key={index}
                            accessibilityRole="button"
                            accessibilityStates={isFocused ? ['selected'] : []}
                            accessibilityLabel={options.tabBarAccessibilityLabel}
                            testID={options.tabBarTestID}
                            activeOpacity={1}
                            onPress={onPress}
                            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
                            onLongPress={onLongPress}>
                            <FontAwesome5 name={iconName}
                                size={24}
                                color={isFocused ? PRIMARY_COLOR : REGISTRATION_SUB_TITLE_COLOR}
                                opacity={1}
                            />
                            <Text style={{ color: isFocused ? PRIMARY_COLOR : REGISTRATION_SUB_TITLE_COLOR, fontSize: 10, fontFamily: FRANKLIN_BOOK, textAlign:'center' }}>
                                {label}
                            </Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        </View>
    );
}